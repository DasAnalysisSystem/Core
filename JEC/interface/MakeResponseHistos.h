#include <algorithm>
#include <cstdlib>
#include <functional>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TString.h>
#include <TH3.h>

using namespace std;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Create TH3 histograms for the Jet response and HLT response
unique_ptr<TH3> makeRespHist (TString name, //!< abitrary name
                              TString study) //!< expecting JER or HLT
{
    using namespace DAS::JetEnergy;

    static int nResBins = 200;
    static auto resBins = getBinning(nResBins, 0, 2);
    unique_ptr<TH3D> h;
    if      (study == "HLT"){
        // Filling in bins of ptHLT and eta
        static const char * axistitles = ";p_{T}^{HLT};|#eta^{rec}|;#frac{p_{T}^{rec}}{p_{T}^{HLT bin low edge}}";
        static const vector<double> HLT_binning = {40., 60., 80., 140., 200., 260., 320., 400., 450., 500.};
        static int nHLTbins = HLT_binning.size()-1;
        h = make_unique<TH3D>(name, axistitles,
                nHLTbins, HLT_binning.data(),
                nYbins, y_edges.data(),
                nResBins, resBins.data());
    }
    else if (study == "JER"){
        // Filling in bins of ptgen results in better conditioned response distributions
        // Pure rec level weight is used
        static const char * axistitles = ";p_{T}^{gen};|#eta^{rec}|;#frac{p_{T}^{rec}}{p_{T}^{gen}}";
        h = make_unique<TH3D>(name, axistitles,
                nPtJERCbins, pt_JERC_edges.data(),
                nAbsEtaBins, abseta_edges.data(),
                nResBins, resBins.data());
    }

    h->SetDirectory(nullptr);
    return h;
}

} // end of DAS::Normalisation namespace
