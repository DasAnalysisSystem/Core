#pragma once

#include <iostream>
#include <queue>
#include <utility>
#include <vector>

#include "Core/Objects/interface/Jet.h"

#include "Math/VectorUtil.h"

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Function that performs matching between an input list of reconstructed level
/// jets and a list of generator level jets, based on their angular distance.
/// The JetMET matching algorithm is used for the procedure.
///
/// JetMET matching algorithm (to what we understand):
///  1. Allow all possible pairs between gen and rec jets
///  2. Discard pairs with DR larger than R/2
///  3. Sort by ascending DR (smaller to larger)
///  4. Keep pairs that are uniquely defined
///
/// According to a page on the [TWiki](https://twiki.cern.ch/twiki/bin/viewauth/CMS/JetResolution),
/// the matching radius should be taken as half of the cone size radius.
template<typename RecContainer = std::vector<RecJet>,
         typename GenContainer = std::vector<GenJet>> struct JMEmatching {

    using RecIt = RecContainer::iterator;
    using GenIt = GenContainer::iterator;
    using Couple = std::pair<RecIt, GenIt>;

    static float maxDR;

    std::vector<RecIt> fake_its;
    std::vector<GenIt> miss_its;
    std::vector<Couple> match_its;

    JMEmatching (RecContainer& recJets, //!< full container of rec jet
                 GenContainer& genJets) //!< full container of gen jet
    {
        using namespace std;

        // 1) get the size of the collections

        size_t nRec = distance(recJets.begin(), recJets.end()),
               nGen = distance(genJets.begin(), genJets.end());

        // 2) declare a few utilities

        typedef pair<size_t,size_t> Indices;

        auto DeltaR2 = [&recJets,&genJets](const Indices& indices) {
            const FourVector& recJet = recJets[indices.first ].p4,
                              genJet = genJets[indices.second].p4;
            return ROOT::Math::VectorUtil::DeltaR2(recJet, genJet);
        };

        auto ascending_DR2 = [&DeltaR2](const Indices& l, const Indices& r) {
            return DeltaR2(l) > DeltaR2(r);
        };

        // 3) make pair candidates

        // https://en.cppreference.com/w/cpp/container/priority_queue
        priority_queue<      Indices,
                      vector<Indices>, decltype(ascending_DR2)>
                                     candidates(ascending_DR2);
        for (size_t iRec = 0; iRec < nRec; ++iRec)
        for (size_t iGen = 0; iGen < nGen; ++iGen) {
            Indices indices {iRec, iGen};
            auto DR2 = DeltaR2(indices);
            if (DR2 >= pow(maxDR,2)) continue;
            candidates.emplace( std::move(indices) );
        }

        // 4) keep only the closest pairs (keep track of matched jets to avoid double matching)

        vector<bool> matchedRec(nRec, false),
                     matchedGen(nGen, false);
        match_its.reserve(min(nRec,nGen));
        while (!candidates.empty()) {
            auto [iRec, iGen] = candidates.top();
            candidates.pop();

            // skip already matched objects
            if (matchedRec.at(iRec)) continue;
            if (matchedGen.at(iGen)) continue;
            matchedRec.at(iRec) = true;
            matchedGen.at(iGen) = true;

            RecIt rec_it = recJets.begin();
            GenIt gen_it = genJets.begin();
            advance(rec_it, iRec);
            advance(gen_it, iGen);

            match_its.push_back( make_pair(rec_it, gen_it) );
        }

        // 5) fill the unmatched entries

        fake_its.reserve(nRec);
        for (int iRec = 0; iRec < nRec; ++iRec) {
            if (matchedRec.at(iRec)) continue;
            RecIt fake_it = recJets.begin();
            advance(fake_it, iRec);
            fake_its.push_back(fake_it);
        }

        miss_its.reserve(nGen);
        for (int iGen = 0; iGen < nGen; ++iGen) {
            if (matchedGen.at(iGen)) continue;
            GenIt miss_it = genJets.begin();
            advance(miss_it, iGen);
            miss_its.push_back(miss_it);
        }
    }
};
template<typename RecContainer, typename GenContainer>
    float JMEmatching<RecContainer, GenContainer>::maxDR = 0.2;

} // end of DAS namespace
