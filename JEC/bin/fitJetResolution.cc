#include <cstdlib>
#include <cmath>
#include <cassert>
#include <iostream>
#include <map>
#include <limits>
#include <functional>
#include <filesystem>

#include <TROOT.h>
#include <TString.h>
#include <TRegexp.h>
#include <TFile.h>
#include <TKey.h>
#include <TH1.h>
#include <TF1.h>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/JEC/interface/resolution.h"

#include "common.h"
#include "fit.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Performs a fit of the resolution with the NSC function, including ad-hoc
/// modification $d$ for possibly non-linearities in the jet resolution:
/// \f[
///     \frac{\sigma}{p_\mathrm{T}} = \sqrt{ \frac{N^2}{p_\mathrm{T}^2}
///                                        + \frac{S^2}{p_\mathrm{T}^d}
///                                        + C^2 }
/// \f]
/// where N stands for noise, S for stochastic, and C for constant.
struct ResolutionFit : public AbstractFit {

    enum {
        N = 0,
        S,
        C,
        d,
        NPARS
    }; //!< parameter indices

    enum Status {
        failed     = 0b0000,
        base       = 0b0001,
        modified   = 0b0010,
    };

    ////////////////////////////////////////////////////////////////////////////
    /// Preparing for fit, setting ranges.
    ResolutionFit (const unique_ptr<TH1>& h, ostream& cout) :
        AbstractFit(h, cout, NPARS)
    {
        interval.first = 32;
        for (int N = h->GetNbinsX()+1; h->GetBinContent(N) == 0 && N > 0; --N)
            interval.second = h->GetBinLowEdge(N-3);

        f = make_unique<TF1>("f", ::mNSC, interval.first, interval.second, NPARS);
        p[N] = 2.;      e[N] = 1;
        p[S] = 1.;      e[S] = 0.5;
        p[C] = 0.05;    e[C] = 0.03; /// \todo take average of last three points?
        p[d] = 1;       e[d] = 0.1;
        f->SetParameters(p);
        f->SetParErrors (e);

        f->SetParName(N, "N");
        f->SetParName(S, "S");
        f->SetParName(C, "C");
        f->SetParName(d, "d");

        f->SetNpx(2000); // Increase points used to draw TF1 object when saving
    }

    void NSC ();
    void NSCd ();

    ~ResolutionFit ()
    {
        for (int i = 0; i < NPARS; ++i)
            if (!isfinite(p[i]))
                BOOST_THROW_EXCEPTION( runtime_error(Form("Fit parameter %d is not finite", i)) );
    }

private:
    void Write (const char *) override;
};

void ResolutionFit::NSC ()
{
    f->SetParLimits(N, 0, 10);
    f->SetParLimits(S, 0, 3);
    f->SetParLimits(C, 0, 0.1);
    f->FixParameter(d, 1);

    fit(interval, __func__);

    if (!chi2ndf) return;
    status |= Status::base;
    Write(__func__);
}

void ResolutionFit::NSCd ()
{
    f->SetParLimits(N, -10, 10);
    f->SetParLimits(S, p[S]/2, p[S]*2);
    f->SetParLimits(C, p[C]/2, p[C]*2);
    f->SetParLimits(d, 0.5, 1.5);

    auto lastChi2ndf = chi2ndf;
    float L = interval.first;
    const int N = h->GetNbinsX();
    for (int im = h->FindBin(L); im > 0; --im) {
        if (h->GetBinContent(im) == 0) continue;
        L = h->GetBinLowEdge(im);
        float R = interval.second;
        for (int iM = h->FindBin(R); iM <= N; ++iM) {
            if (h->GetBinContent(iM) == 0) continue;
            R = h->GetBinLowEdge(iM+1);
            fit({L,R}, __func__);
        }
    }

    if (!chi2ndf || *chi2ndf >= *lastChi2ndf) return;
    status |= Status::modified;
    Write(__func__);
}

////////////////////////////////////////////////////////////////////////////////
/// Write best current estimate in current directory.
///
/// Three color codes are used to categorize fits on their final state
/// (after all 3 fits are performed):
/// - Red for a failed fit
/// - Orange a successful NSC fit
/// - Green a successful modified NSC fit
void ResolutionFit::Write (const char * name)
{
    bool goodNSC  = (status & Status::base) == Status::base;
    bool goodNSCd = (status & Status::modified) == Status::modified;
    if      (goodNSCd) f->SetLineColor(kGreen+1);
    else if (goodNSC ) f->SetLineColor(kOrange+1);
    else               f->SetLineColor(kRed+1);

    AbstractFit::Write(name);
}

} // end of DAS::JetEnergy namespace

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Fit the resolution obtained from `fitJetResponse` in a given eta/rho bin.
void FitResolution (TDirectory * dir,
                    unique_ptr<TH1> h,
                    const int steering)
{
    dir->cd();
    TString stdPrint = h->GetTitle();
    stdPrint.ReplaceAll(TString( h->GetName() ), "");
    cout << h->GetName() << '\t' << stdPrint << endl;
    auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

    ResolutionFit res(h, cout);

    res.NSC();
    res.NSCd();

    /// \todo parameters in bins of eta (and rho/mu?)
    ///
    /// --> one should probably give the directory with all eta bins as input
}

////////////////////////////////////////////////////////////////////////////////
/// Copy directory structure.
/// Given an input directory `dIn` recursively loop over all directory
/// hierarchy/contents and reproduce it in the directory `dOut` of the output file.
/// For each `TH1` instance found containing the resolution,
/// perform a NSC fit by calling `FitResolution()` function.
void loopDirsFromFitResponse (TDirectory * dIn,
                              TDirectory * dOut,
                              const int steering)
{
    auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;
    for (const auto&& obj: *(dIn->GetListOfKeys())) {
        auto const key = dynamic_cast<TKey*>(obj);
        if ( key->IsFolder() ) {
            auto ddIn = dynamic_cast<TDirectory*>( key->ReadObj() );
            if ( TString( ddIn->GetName() ).Contains("ptbin") ) continue;
            TDirectory * ddOut = dOut->mkdir(ddIn->GetName(), ddIn->GetTitle());
            ddOut->cd();
            cout << bold << ddIn->GetPath() << def << endl;

            loopDirsFromFitResponse(ddIn, ddOut, steering);
        }
        else if ( TString( key->ReadObj()->GetName() ).Contains("sigmaCore") ) {
            auto res = unique_ptr<TH1>( dynamic_cast<TH1*>(key->ReadObj()) );
            res->SetDirectory(dOut);
            dOut->cd();
            res->Write();

            if ( TString(res->GetName()).Contains("Fail") ) continue;

            // Fit resolution
            FitResolution(dOut, std::move(res), steering);
        }
        else cout << orange << "Ignoring " << key->ReadObj()->GetName() << " of `"
                  << key->ReadObj()->ClassName() << "` type" << def << endl;
    } // End of for (list of keys) loop
    cout << def << flush;
}

////////////////////////////////////////////////////////////////////////////////
/// Fit resolution curves from `fitJetResponse`
void fitJetResolution
            (const fs::path& input, //!< input ROOT file (histograms)
             const fs::path& output, //!< name of output ROOT (histograms)
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    cout << __func__ << " start" << endl;

    auto fIn  = make_unique<TFile>(input .c_str(), "READ"),
         fOut = make_unique<TFile>(output.c_str(), "RECREATE"); // TODO
    JetEnergy::loopDirsFromFitResponse(fIn.get(), fOut.get(), steering);

    cout << __func__ << " stop" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options("Fit resolution in bins of pt and eta.");
        options.input ("input" , &input , "input ROOT file (from `fitJetResponse`)")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::fitJetResolution(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
