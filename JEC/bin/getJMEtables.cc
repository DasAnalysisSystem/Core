#include <cstdlib>
#include <filesystem>
#include <iomanip>
#include <iostream>
#include <string>

#include <TH3D.h>
#include <TF1.h>
#include <TFile.h>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "common.h"

#include <darwin.h>
#include <correction.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Obtain corrections and uncertainties in ROOT format for plotting and
/// comparison.
void getJMEtable
            (const fs::path input, //!< JetMET JES tables in JSON format
             const fs::path output, //!< name of output root file
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering //!< steering parameters from `DT::Options`
            )
{
    cout << __func__ << " start" << endl;

    auto fOut = DT::GetOutputFile(output);

    auto R = config.get<int>("flags.R");
    auto year = config.get<int>("flags.year");
    string algo = "chs"; /// \todo identify algo
    cout << R << ' ' << year << ' ' << algo << endl;

    const auto tag = config.get<string>("corrections.JES.tag"), // correction tag
               level = config.get<string>("corrections.JES.level"); // correction level
    string key = tag + "_" + level + "_AK" + to_string(R) + "PF" + algo;
    cout << key << endl;

    auto cset = correction::CorrectionSet::from_file(input.string());
    correction::Correction::Ref sf;
    try {
        sf = cset->at(key);
    }
    catch (std::out_of_range& e) {
        BOOST_THROW_EXCEPTION( std::invalid_argument("Nothing corresponding to that key") );
    }

    cout << "Declaring histogram" << endl;
    unique_ptr<TH1> h;
    map<string, correction::Variable::Type> named_inputs;
    if (level == "L1FastJet" || level == "PtResolution") {
        h = make_unique<TH3D>(level.c_str(), level.c_str(),
                                   nAbsEtaBins, abseta_edges .data(),
                                   nPtJERCbins, pt_JERC_edges.data(),
                                   rho_edges.at(year).size()-1,
                                   rho_edges.at(year).data());
        named_inputs["JetA"] = M_PI * pow(R*0.1, 2); // Jet area estimation
    }
    else if (level == "L2Relative" || level == "L2L3Residual")
        h = make_unique<TH2D>(level.c_str(), level.c_str(),
                                   nAbsEtaBins, abseta_edges .data(),
                                   nPtJERCbins, pt_JERC_edges.data());
    else if (level == "ScaleFactor") {
        h = make_unique<TH1D>(level.c_str(), level.c_str(),
                                   nAbsEtaBins, abseta_edges .data()); /// \todo check binning
        named_inputs["systematic"] = "nom";
    }
    else // assuming uncertainties
        h = make_unique<TH2D>(level.c_str(), level.c_str(),
                                   nEtaUncBins, eta_unc_edges.data(),
                                   nPtJERCbins, pt_JERC_edges.data());

    cout << "dim = " << h->GetDimension() << endl;
    for (int etabin = 1; etabin <= h->GetNbinsX(); ++etabin)
    for (int  ptbin = 1;  ptbin <= h->GetNbinsY(); ++ ptbin)
    for (int rhobin = 1; rhobin <= h->GetNbinsZ(); ++rhobin) {
        // set values by name
        named_inputs["JetEta"] = h->GetXaxis()->GetBinCenter(etabin);
        named_inputs["JetPt" ] = h->GetYaxis()->GetBinCenter( ptbin);
        named_inputs["Rho"   ] = h->GetZaxis()->GetBinCenter(rhobin);

        // order the input value in the necessary order
        vector<correction::Variable::Type> inputs;
        for (const correction::Variable& input: sf->inputs())
            inputs.push_back(named_inputs.at(input.name()));

        // get and fill the value
        double value = sf->evaluate(inputs);

        cout << setw(5) << ptbin << setw(5) << etabin << setw(5) << rhobin << setw(15) << value << endl;

        if (level == "L1FastJet")
            h->SetBinContent(etabin, ptbin, rhobin, value);
        else if (level == "ScaleFactor")
            h->SetBinContent(etabin, value);
        else
            h->SetBinContent(etabin, ptbin, value);
    }
    h->Write();

    cout << __func__ << " end" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options("Obtain JES corrections JetMET tables in ROOT histograms");
        options.input ("input" , &input , "JSON file containing the corrections")
               .output("output", &output, "output ROOT file")
               .arg<string>("tag", "corrections.JES.tag", "tag (e.g. Summer19UL18_V5_MC)")
               .arg<string>("level", "corrections.JES.level", "level (e.g. L2Relative)")
               .arg<int>("R", "flags.R", "R (x10) parameter in jet clustering algorithm")
               .arg<int>("year", "flags.year", "year (4 digits)");

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::JetEnergy::getJMEtable(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
