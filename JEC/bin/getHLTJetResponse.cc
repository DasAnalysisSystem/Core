#include <algorithm>
#include <cstdlib>
#include <fstream>
#include <functional>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/toolbox.h"
#include "Core/CommonTools/interface/DASOptions.h"

#include <TFile.h>
#include <TH3.h>
#include <TString.h>
#include <TTree.h>

#include "common.h"
#include "Core/JEC/interface/MakeResponseHistos.h"
#include "Core/Trigger/interface/match.h"

#include <darwin.h>

using namespace std;
using namespace DAS::JetEnergy;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Get HLT single-jet response in bins of kinematics in real data.
/// The output can be used as input to fitJetResponse to determine and
/// understand the deviations from the Gaussian expectations,
/// which prevent from fitting the trigger efficiency with the error function.
void getHLTJetResponse
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
         const fs::path& output, //!< name of output ROOT file (histograms)
         const pt::ptree& config, //!< config file from `DT::Options`
         const int steering, //!< steering parameters from `DT::Options`
         const DT::Slice slice = {1,0} //!< slices for running
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (isMC)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only DATA may be used as input.", metainfo) );

    auto rEv = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto recjets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");
    auto hltjets = flow.GetBranchReadOnly<vector<FourVector>>("hltJets");

    unique_ptr<TH3> inclResp = makeRespHist("inclusive", "HLT"); // Inclusive response

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        auto recWgt = rEv->weights.front();
        // Matching & Fill response
        for (const RecJet& recjet: *recjets) {
            FourVector hltjet = match(recjet.p4, hltjets);
            auto ptrec = recjet.CorrPt();
            auto pthlt = hltjet.Pt();

            int pthltbin = inclResp->GetXaxis()->FindBin(pthlt);
            double hlt_lowedge = inclResp->GetXaxis()->GetBinLowEdge(pthltbin);

            // Fill response
            auto response = ptrec/hlt_lowedge;
            auto yrec = recjet.AbsRap();

            inclResp->Fill(pthlt, yrec, response, recWgt);

        } // End of recjets' loop
    } // End of event loop

    // Creating directory hierarchy and saving histograms
    inclResp->SetDirectory(fOut);
    inclResp->SetTitle("Response");
    inclResp->Write("Response");

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        
        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options("Get HLT response in bins of pt and eta.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory after applyJEScorrections")
               .output("output", &output, "output ROOT file to be used in fitJetResponse");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::getHLTJetResponse(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
