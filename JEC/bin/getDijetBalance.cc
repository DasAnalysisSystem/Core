#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <iostream>
#include <fstream>
#include <functional>
#include <type_traits>
#include <vector>

#include <TFile.h>
#include <TRandom.h>
#include <TH3.h>
#include <TString.h>
#include <TDirectory.h>

#include "Core/Objects/interface/Di.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Variation.h"

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Balance.h"
#include "common.h"

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;
namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

using DAS::Uncertainties::Variation;

namespace DAS::JetEnergy {

////////////////////////////////////////////////////////////////////////////////
/// Apply dijet topology selection independently from the gen or rec level.
/// The balance is part of the standard jet calibration procedure at CMS.
bool passDijetSelection (const auto& dijet, //!< dijet system
                         const auto& jets, //!< full jet vector
                         const Variation& v, //!< systematic variation
                         const double alpha = 0.3) //!< reject significant extra jets
{
    if (!dijet) return false;
    if (jets.size() < 2) return false;

    // back-to-back
    if (dijet.DeltaPhi() < 2.7 ) return false;

    // at least one jet in barrel acceptance
    if (std::abs(dijet.first ->p4.Eta()) >= 1.3 &&
        std::abs(dijet.second->p4.Eta()) >= 1.3) return false;

    // impact of extra jets
    if (jets.size() > 2) {
        const auto pt2 = jets.at(2).CorrPt(v);
        if (pt2 > alpha * dijet.HT()) return false;
    }

    return true;
}

////////////////////////////////////////////////////////////////////////////////
/// Get Pt balance from dijet events
void getDijetBalance
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    const auto alpha = config.get<float>("skim.dijet.alpha");
    metainfo.Set<float>("skim", "dijet", "alpha", alpha);

    auto gEv = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent"): nullptr;
    auto rEv = flow.GetBranchReadOnly<RecEvent>("recEvent");
    
    auto genJets = isMC ? flow.GetBranchReadOnly<vector<GenJet>>("genJets") : nullptr;
    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");

    auto genDijet = isMC ? flow.GetBranchReadOnly<GenDijet>("genDijet") : nullptr;
    auto recDijet = flow.GetBranchReadOnly<RecDijet>("recDijet");

    const bool applySyst = (steering & DT::syst) && !isMC;
    const auto variations = GetScaleVariations(metainfo, applySyst);

    // detector level
    vector<Balance> recBalances;
    TDirectory * rec = fOut->mkdir("rec");
    for (const Variation& v: variations)
        recBalances.emplace_back(rec, v, pt_JERC_edges, abseta_edges);

    // particle level
    vector<Balance> genBalances;
    TDirectory * gen = nullptr;
    if (isMC) {
        gen = fOut->mkdir("gen");
        genBalances.emplace_back(gen, variations.front(), pt_JERC_edges, abseta_edges);
    }

    TRandom3 r3(metainfo.Seed<39856>(slice));
    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        // we take the probe and the tag randomly
        auto r = r3.Uniform();
        int itag =   (r <  0.5),
            iprobe = (r >= 0.5);

        for (Balance& bal: recBalances) {
            auto gw = isMC ? gEv->Weight(bal.v) : Weight{1.,0},
                 rw =        rEv->Weight(bal.v);

            if (passDijetSelection(*recDijet, *recJets, bal.v, alpha) && 
                (abs((itag ? recDijet->first : recDijet->second)->p4.Eta()) < 1.3))
                bal((itag   ? *recDijet->first : *recDijet->second),
                    (iprobe ? *recDijet->first : *recDijet->second), gw*rw);
        }

        if (!isMC) continue;

        for (Balance& bal: genBalances) {
            auto gw = gEv->Weight(bal.v);
            if (passDijetSelection(*genDijet, *genJets, bal.v, alpha) && 
                (abs((itag ? genDijet->first : genDijet->second)->p4.Eta()) < 1.3))
                bal((itag   ? *genDijet->first : *genDijet->second),
                    (iprobe ? *genDijet->first : *genDijet->second), gw);
        }

    } // end of event loop

    for (auto& p: recBalances) p.Write();
    for (auto& p: genBalances) p.Write();

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::JetEnergy namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        DT::Options options("Get dijet pt balance in bins of pt and eta.",
                            DT::split | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("alpha", "skim.dijet.alpha", "tolerance for extra radiations");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetEnergy::getDijetBalance(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
