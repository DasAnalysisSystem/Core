#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/JEC/interface/JMEmatching.h"

#define BOOST_TEST_MODULE Matching
#include <boost/test/included/unit_test.hpp>

#include <iostream>
#include <array>

#include <TTree.h>
#include <TFile.h>

#include "colours.h"

using namespace DAS;
using namespace std;

BOOST_AUTO_TEST_CASE( matching )
{
    array<GenJet,6> genjets;
    genjets[0].p4.SetCoordinates(2540.7522, -0.939323, 2.9671194, 42.679748);
    genjets[1].p4.SetCoordinates(2524.7915, 0.3277488, -0.195445, 58.079277);
    genjets[2].p4.SetCoordinates(34.406520, -2.007476, 1.9084951, 6.9956078);
    genjets[3].p4.SetCoordinates(33.516635, -0.122238, 0.1614812, 7.0759511);
    genjets[4].p4.SetCoordinates(376.69122, 0.6268847, 0.1621258, 54.715171); // miss
    genjets[5].p4.SetCoordinates(286.42022, 0.1454636, -2.711239, 30.607936); // miss

    array<RecJet,14> recjets;
    recjets[ 0].p4.SetCoordinates(2483.1442, 0.3214379, -0.196805, 106.61298);
    recjets[ 1].p4.SetCoordinates(2134.2094, -0.942423, 2.9649009, 89.293060);
    recjets[ 2].p4.SetCoordinates(40.095314, -2.047485, 1.9051857, 8.5427942);
    recjets[ 3].p4.SetCoordinates(39.125808, -0.116546, 0.1461072, 8.0769491);
    recjets[ 4].p4.SetCoordinates(26.883150, -3.200822, 0.1855384, 4.9055452); // fake
    recjets[ 5].p4.SetCoordinates(17.303262, -4.666824, 0.1659940, 4.1057658); // fake
    recjets[ 6].p4.SetCoordinates(16.604623, 0.3660668, -0.827474, 4.8734064); // fake
    recjets[ 7].p4.SetCoordinates(16.549018, -2.687304, 2.4509530, 3.4501276); // fake
    recjets[ 8].p4.SetCoordinates(12.394617, -1.593085, 1.7367856, 2.5770857); // fake
    recjets[ 9].p4.SetCoordinates(12.211031, 1.5677306, 2.3947563, 3.7380209); // fake
    recjets[10].p4.SetCoordinates(11.802206, 3.9794914, 3.1383521, 3.0563554); // fake
    recjets[11].p4.SetCoordinates(11.423704, -4.258290, 2.9604301, 2.9349415); // fake
    recjets[12].p4.SetCoordinates(11.079508, -1.123926, -1.405094, 2.6803200); // fake
    recjets[13].p4.SetCoordinates(10.026986, -1.896397, -1.734840, 2.4190814); // fake

    auto genjets_copy = genjets;
    auto recjets_copy = recjets;

    JMEmatching<decltype(recjets),decltype(genjets)>::maxDR = 0.2;
    JMEmatching matching(recjets, genjets);

    cout << bold << "matched:" << def;
    for (auto& [rec_it,gen_it]: matching.match_its)
        cout << '\n' << rec_it->p4 << '\t' << gen_it->p4;
    cout << bold << "\nfake jets:" << def;
    for (auto& rec_it: matching.fake_its)
        cout << '\n' << rec_it->p4;
    cout << bold << "\nmiss jets:" << def;
    for (auto& gen_it: matching.miss_its)
        cout << '\n' << gen_it->p4;
    cout << endl;

    BOOST_TEST( matching.match_its.size() == 4 );
    BOOST_TEST( matching.fake_its.size() == 10 );
    BOOST_TEST( matching.miss_its.size() == 2 );

    BOOST_TEST( genjets == genjets_copy );
    BOOST_TEST( recjets == recjets_copy );
}

#endif
