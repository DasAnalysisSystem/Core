#include "Core/CommonTools/interface/binnings.h"
#include "Core/Unfolding/interface/BF.h"

#include "Math/VectorUtil.h"

#include <exceptions.h>

using namespace std;
using namespace DAS;
using namespace DAS::Unfolding::ZmmY;
using ROOT::Math::VectorUtil::DeltaR;
namespace DE = Darwin::Exceptions;

BFFiller::BFFiller (const BF& obs, Darwin::Tools::Flow& flow)
    : obs(obs)
    , genMuons(obs.isMC ? flow.GetBranchReadOnly<vector<GenMuon>>("genMuons") : nullptr)
    , recMuons(flow.GetBranchReadOnly<vector<RecMuon>>("recMuons"))
    , genPhotons(obs.isMC ? flow.GetBranchReadOnly<vector<GenPhoton>>("genPhotons") : nullptr)
    , recPhotons(flow.GetBranchReadOnly<vector<RecPhoton>>("recPhotons"))
    , gEv(obs.isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr)
    , rEv(flow.GetBranchReadOnly<RecEvent>("recEvent"))
{
}

list<int> BFFiller::fillRec (DistVariation& v)
{
    irecbin.reset();
    rInfo = EventInfo(gEv, rEv, *recMuons, *recPhotons, v);

    switch (rInfo.category) {
    case EventCategory::NotSelected:
        break;

    case EventCategory::MuMu:
        irecbin = obs.recBinning->GetGlobalBinNumber(rInfo.mMuMu, 1.);
        if (*irecbin == 0) cerr << red << rInfo.mMuMu << '\n' << def;
        v.tmp->Fill(*irecbin, rInfo.weight);
        v.rec->Fill(*irecbin, rInfo.weight);
        return {*irecbin};

    case EventCategory::MuMuGamma:
        irecbin = obs.recBinning->GetGlobalBinNumber(rInfo.mMuMuGamma, 2.);
        if (*irecbin == 0) cerr << red << rInfo.mMuMuGamma << '\n' << def;
        v.tmp->Fill(*irecbin, rInfo.weight);
        v.rec->Fill(*irecbin, rInfo.weight);
        return {*irecbin};
    }

    return {};
}

void BFFiller::fillMC (DistVariation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    EventInfo gInfo(gEv, nullptr, *genMuons, *genPhotons, v);
    optional<int> igenbin;

    switch (gInfo.category) {
    case EventCategory::NotSelected:
        break;

    case EventCategory::MuMu:
        igenbin = obs.genBinning->GetGlobalBinNumber(*obs.process, 1.);
        if (*igenbin == 0) cerr << red << gInfo.mMuMu << '\n' << def;
        break;

    case EventCategory::MuMuGamma:
        igenbin = obs.genBinning->GetGlobalBinNumber(*obs.process, 2.);
        if (*igenbin == 0) cerr << red << gInfo.mMuMuGamma << '\n' << def;
        break;
    }

    if (igenbin) v.gen->Fill(*igenbin, gInfo.weight);

    if (igenbin && irecbin) {
        // Good events
        v.RM     ->Fill(*igenbin, *irecbin,                rInfo.weight);
        v.missOut->Fill(*igenbin,           gInfo.weight - rInfo.weight);
    } else if (igenbin && !irecbin)
        // Miss
        v.missOut->Fill(*igenbin,           gInfo.weight               );
    else if (!igenbin && irecbin)
        // Fake
        v.fakeOut->Fill(          *irecbin,                rInfo.weight);
}

////////////////////////////////////////////////////////////////////////////////

BF::BF () :
    Observable(__FUNCTION__, "Branching fraction of Z decaying to two muons and a photon")
{
    int nCh = channels.size(),
        nPr = processes.size();
    recBinning->AddAxis("invMass"   ,  30,  76,     106,false,false);
    recBinning->AddAxis("finalState", nCh, 0.5, 0.5+nCh,false,false);
    genBinning->AddAxis("processes" , nPr, 0.5, 0.5+nPr,false,false);
    genBinning->AddAxis("finalState", nCh, 0.5, 0.5+nCh,false,false);
}

unique_ptr<DAS::Unfolding::Filler> BF::getFiller (Darwin::Tools::Flow& flow) const
{
    return make_unique<BFFiller>(*this, flow);
}
