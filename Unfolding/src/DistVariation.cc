#include <cassert>
#include <iostream>
#include <list>
#include <numeric>

#include <TH1.h>
#include <TH2.h>
#include <TDirectory.h>
#include <TUnfoldBinning.h>

#include "Core/Unfolding/interface/DistVariation.h"

#include <colours.h>

using namespace std;

static const auto feps = numeric_limits<float>::epsilon();

namespace DAS::Unfolding {

DistVariation::DistVariation (const TString& group, const TString& name, size_t index, int bit) :
        DAS::Uncertainties::Variation(group, name, index, bit)
{
    cout << "|- Setting up " << __func__ << " `" << name << "`." << endl;

    if (!genBinning)
        BOOST_THROW_EXCEPTION( runtime_error("Particle-level binning has not been defined") );
    if (!recBinning)
        BOOST_THROW_EXCEPTION( runtime_error("Detector-level binning has not been defined") );

    // TH1 * TUnfoldBinning::CreateHistogram
    //      (const char * histogramName,
    //       Bool_t       originalAxisBinning = kFALSE, // no physical axis
    //       Int_t **     binMap = nullptr, // mapping from global bins to hist bins (unused here)
    //       const char * histogramTitle = nullptr,
    //       const char * axisSteering = nullptr  // needed for underflow and overflow (not done here)
    //      )
    auto makeRec = [this,name](TString subname, TString title) {
        auto h = unique_ptr<TH1>(recBinning->CreateHistogram(name + subname,
                                        false, nullptr, title + " (detector level)"));
        h->GetXaxis()->SetTitle("rec");
        return h;
    };
    rec = makeRec("rec", "all");
    tmp = makeRec("tmp", "temporary object to calculate covariance");

    // TH2D * TUnfoldBinning::CreateErrorMatrixHistogram
    //      (const char * histogramName,
    //       Bool_t       originalAxisBinning,
    //       Int_t **     binMap = nullptr,
    //       const char * histogramTitle = nullptr,
    //       const char * axisSteering = nullptr 
    //      )
    cov = unique_ptr<TH2>(recBinning->CreateErrorMatrixHistogram(name + "cov",
                                        false, nullptr, "covariance matrix (detector level)"));
    cov->GetXaxis()->SetTitle("rec");
    cov->GetYaxis()->SetTitle("rec");

    if (!isMC) return;
    auto makeGen = [this,name](TString subname, TString title) {
        auto h = unique_ptr<TH1>(genBinning->CreateHistogram(name + subname,
                                        false, nullptr, title + " (particle level)"));
        h->GetXaxis()->SetTitle("gen");
        return h;
    };
    gen         = makeGen("gen"        , "all");
    missNoMatch = makeGen("missNoMatch", "truly unmatched");
    missOut     = makeGen("missOut"    , "matched out of the phase space");
    fakeNoMatch = makeRec("fakeNoMatch", "truly unmatched");
    fakeOut     = makeRec("fakeOut"    , "matched out of the phase space");

    // TH2D * TUnfoldBinning::CreateHistogramOfMigrations (TUnfoldBinning const * xAxis,
    //                                                     TUnfoldBinning const * yAxis,
    //                                                     char const *           histogramName,
    //                                                     Bool_t                 originalXAxisBinning = kFALSE,
    //                                                     Bool_t                 originalYAxisBinning = kFALSE,
    //                                                     char const *           histogramTitle = nullptr 
    //                                                    )
    RM = unique_ptr<TH2>(TUnfoldBinning::CreateHistogramOfMigrations(genBinning, recBinning,
                                        name + "RM", false, false, "response matrix"));
    RM->GetXaxis()->SetTitle("gen");
    RM->GetYaxis()->SetTitle("rec");
}

void DistVariation::Write (TDirectory * d)
{
    cout << "Writing " << name << endl;
    d->cd();
    TDirectory * dd = d->mkdir(name);
    dd->cd();
    auto write = [this,dd](auto&& h) {
        if (!h) return;
        TString n = h->GetName();
        n.ReplaceAll(name,"");
        h->SetDirectory(dd);
        h->Write(n);
    };
    auto apply_to_all = [](auto&& f, auto&& ... x) { (f(x),...); };
    apply_to_all(write,
            rec, gen, missNoMatch, missOut, fakeNoMatch, fakeOut, RM ,cov);
}

bool DistVariation::isMC = false; // just fort initialisation
TUnfoldBinning * DistVariation::genBinning = nullptr;
TUnfoldBinning * DistVariation::recBinning = nullptr;

DistVariation::~DistVariation ()
{
    auto apply_to_all = [](auto&& f, auto&& ... x) { (f(x),...); };

    // 1D histograms
    auto check_bin_contents_1D = [](auto&& h) {
        if (!h) return;
        if (h->GetEntries() == 0) // TODO: fix move constructor (otherwise this is prompted when copying, which is misleading)
            cerr << orange << h->GetName() << " is empty" << def << '\n';
        if (h->GetBinContent(0) > 0)
            cerr << orange << h->GetName() << "'s underflow is not empty\n" << def;
        if (h->GetBinContent(h->GetNbinsX()+1) > 0)
            cerr << orange << h->GetName() << "'s overflow is not empty\n" << def;
    };
    apply_to_all(check_bin_contents_1D,
            rec, gen, missNoMatch, missOut, fakeNoMatch, fakeOut);

    // 2D histograms
    auto check_bin_contents_2D = [](auto&& h) {
        if (!h) return;

        int nBinsX = h->GetNbinsX();
        int nBinsY = h->GetNbinsY();

        bool underflowFilled = false,
             overflowFilled = false;
        for (int i = 1; i <= nBinsX; ++i) {
            underflowFilled |= h->GetBinContent(i,0) > 0;
            overflowFilled |= h->GetBinContent(i,nBinsY+1) > 0;
        }
        if (underflowFilled)
            cerr << orange << h->GetName() << "'s underflow on horizontal axis is not empty\n" << def;
        if (overflowFilled)
            cerr << orange << h->GetName() << "'s overflow on horizontal axis is not empty\n" << def;

        underflowFilled = false;
        overflowFilled = false;
        for (int i = 1; i <= nBinsX; ++i) {
            underflowFilled |= h->GetBinContent(0,i) > 0;
            overflowFilled |= h->GetBinContent(nBinsX+1,0) > 0;
        }
        if (underflowFilled)
            cerr << orange << h->GetName() << "'s underflow on vertical axis is not empty\n" << def;
        if (overflowFilled)
            cerr << orange << h->GetName() << "'s overflow on vertical axis is not empty\n" << def;
    };
    apply_to_all(check_bin_contents_2D, RM, cov);

    if (!RM) return;

    bool genClosure = true, recClosure = true;
    auto checkbin = [](const unique_ptr<TH1>& h, int i) {
        auto content = h->GetBinContent(i);
        bool ok = std::abs(content - 1) < feps || std::abs(content) < feps;
        if (!ok) cerr << orange << "| " << i << " " << content << "\n" << def;
        return ok;
    };

    auto RMx = unique_ptr<TH1>(RM->ProjectionX());
    RMx->Add(missNoMatch.get());
    RMx->Add(missOut.get());
    RMx->Divide(gen.get());
    for (int i = 1; i <= RMx->GetNbinsX(); ++i)
        genClosure &= checkbin(RMx, i);

    auto RMy = unique_ptr<TH1>(RM->ProjectionY());
    RMy->Add(fakeNoMatch.get());
    RMy->Add(fakeOut.get());
    RMy->Divide(rec.get());
    for (int i = 1; i <= RMy->GetNbinsX(); ++i)
        recClosure &= checkbin(RMy, i);

    if (!recClosure)
        cerr << red << name << " is not closing at rec level" << '\n' << def;
    if (!genClosure)
        cerr << red << name << " is not closing at gen level" << '\n' << def;
}

void fillCov (const DistVariation& v, const std::list<int>& binIDs)
{
    for (auto x: binIDs) for (auto y: binIDs) {
        double cCov = v.cov->GetBinContent(x,y),
               cTmp = v.tmp->GetBinContent(x)
                    * v.tmp->GetBinContent(y);
        v.cov->SetBinContent(x,y,cCov+cTmp);
    }
    for (auto x: binIDs) {
        v.tmp->SetBinContent(x, 0);
        v.tmp->SetBinError  (x, 0);
    }
}

vector<DistVariation> GetVariations (Darwin::Tools::MetaInfo& metainfo,
                                     bool applySyst, ostream& cout)
{
    using namespace DAS::Unfolding;

    vector<DistVariation> variations;
    variations.reserve(100);
    variations.emplace_back("nominal","nominal");
    if (!applySyst) return variations;

    const TList * groupList = metainfo.List("variations");
    for (const TObject * group: *groupList) {
        const auto groupContents = dynamic_cast<const TList*>(group);
        if (!groupContents)
            BOOST_THROW_EXCEPTION( invalid_argument(group->GetName()) );
        size_t i = 0;
        for (const TObject * obj: *groupContents) {
            const auto string = dynamic_cast<const TObjString*>(obj);
            if (!string)
                BOOST_THROW_EXCEPTION( invalid_argument(obj->GetName()) );
            cout << group->GetName() << " " << string->GetString() << " " << (i+1) << endl;
            variations.emplace_back(group->GetName(), string->GetString(), ++i);
        }
    }

    return variations;
}

} // end of DAS::Unfolding namespace
