#include "Core/CommonTools/interface/binnings.h"
#include "Core/Unfolding/interface/MjjYmax.h"

#include "Math/VectorUtil.h"

#include <colours.h>
#include <exceptions.h>

#include <TVectorT.h>

using namespace std;
using namespace DAS;
using namespace DAS::Unfolding;
using namespace DAS::Unfolding::DijetMass;
namespace DE = Darwin::Exceptions;


MjjYmaxFiller::MjjYmaxFiller (const MjjYmax& obs, Darwin::Tools::Flow& flow)
    : obs(obs)
    , genJets(obs.isMC ? flow.GetBranchReadOnly<vector<GenJet>>("genJets") : nullptr)
    , recJets(flow.GetBranchReadOnly<vector<RecJet>>("recJets"))
    , gEv(obs.isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr)
    , rEv(flow.GetBranchReadOnly<RecEvent>("recEvent"))
{
}

void MjjYmaxFiller::match ()
{
    matched.reset();

    auto match = [this](size_t i, size_t j) {
        const FourVector& g = genJets->at(i).p4,
                          r = recJets->at(j).p4;
        using ROOT::Math::VectorUtil::DeltaR;
        auto DR = DeltaR(g, r);
        //cout << g << '\t' << r << '\t' << DR << '\t' << result << '\n';
        return DR < obs.maxDR;
    };

    // matching (swapping leading and subleading is allowed)
    matched = genJets->size() > 1 && recJets->size() > 1
              && (   (match(0,0) && match(1,1))
                  || (match(0,1) && match(1,0)) );
}

namespace {

////////////////////////////////////////////////////////////////////////////////
/// Dijet selection
template<typename Jet>
DAS::Di<const Jet, const Jet> selection (const vector<Jet>& jets,
                                         const Uncertainties::Variation& v)
{
    Di<const Jet, const Jet> dijet;
    if (jets.size() < 2) return dijet;

    dijet = jets[0] + jets[1];

    if (dijet.first->CorrPt(v) < 100 || dijet.second->CorrPt(v) < 50 ||
        dijet.first->AbsRap() >= maxy || dijet.second->AbsRap() >= maxy)
        dijet.clear();

    return dijet;
}

////////////////////////////////////////////////////////////////////////////////
/// Find the bin number.
///
/// \return bin index (0 means out of PS, like `TUnfoldBinning::GetGlobalBinNumber`)
template<typename Jet>
double getBinNumber (const DAS::Di<const Jet, const Jet>& dijet,
                     const Uncertainties::Variation& v,
                     TUnfoldBinning * bng)
{
    if (!dijet) return 0;

    float mass = dijet.CorrP4(v).M(),
          ymax = dijet.Ymax();

    return bng->GetGlobalBinNumber(mass, ymax);
}

} // end of anonymous namespace

list<int> MjjYmaxFiller::fillRec (DistVariation& v)
{
    auto dijet = selection(*recJets, v);
    if (!dijet) return {};

    int i = getBinNumber(dijet, v, obs.recBinning);
    if (i == 0) return {};

    double w = rEv->Weight(v);
    if (obs.isMC) w *= gEv->Weight(v);
    w *= dijet.Weight(v);

    v.tmp->Fill(i, w);
    v.rec->Fill(i, w);

    return list<int>{i};
}

void MjjYmaxFiller::fillMC (DistVariation& v)
{
    if (!obs.isMC)
        BOOST_THROW_EXCEPTION( runtime_error(__func__ + " should only be called for MC"s) );

    auto recdijet = selection(*recJets, v);
    auto irec = getBinNumber(recdijet, v, obs.recBinning);
    double recW = rEv->Weight(v);
    if (irec > 0) recW *= recdijet.Weight(v);

    auto gendijet = selection(*genJets, v);
    auto igen = getBinNumber(gendijet, v, obs.genBinning);
    double genW = gEv->Weight(v);
    if (igen > 0) genW *= gendijet.Weight(v);

    if (igen > 0) v.gen->Fill(igen, genW);

    if (*matched) {
        if      (irec > 0  && igen >  0) {    v.RM->Fill(igen, irec, genW *    recW );
                                     v.missNoMatch->Fill(igen,       genW * (1-recW)); }
        else if (irec == 0 && igen >  0) v.missOut->Fill(igen,       genW           );
        else if (irec > 0  && igen == 0) v.fakeOut->Fill(      irec, genW *    recW );
    }
    else {
        if (igen > 0) v.missNoMatch->Fill(igen, genW       );
        if (irec > 0) v.fakeNoMatch->Fill(irec, genW * recW);
    }
}

////////////////////////////////////////////////////////////////////////////////

MjjYmax::MjjYmax () :
    Observable(__FUNCTION__, "Dijet mass double differential cross section")
{
    vector<double> recMjj_edges;
    recMjj_edges.reserve(Mjj_edges.size()*2);

    // rec binning with twice more bins
    for (size_t j = 0; j < Mjj_edges.size()-1; ++j) {
        double m = Mjj_edges.at(j),
               M = Mjj_edges.at(j+1);
        recMjj_edges.push_back(m);
        recMjj_edges.push_back((m+M)/2);
    }
    recMjj_edges.push_back(Mjj_edges.back());

    int nRecMjjBins = recMjj_edges.size()-1;
    int nGenMjjBins =    Mjj_edges.size()-1;

    recBinning->AddAxis("Mjj", nRecMjjBins, recMjj_edges.data(),false,false);
    recBinning->AddAxis("y"  ,   nYmaxBins,   ymax_edges.data(),false,false);
    genBinning->AddAxis("Mjj", nGenMjjBins,    Mjj_edges.data(),false,false);
    genBinning->AddAxis("y"  ,   nYmaxBins,   ymax_edges.data(),false,false);
}

unique_ptr<DAS::Unfolding::Filler> MjjYmax::getFiller (Darwin::Tools::Flow& flow) const
{
    return make_unique<MjjYmaxFiller>(*this, flow);
}

void MjjYmax::setLmatrix (const unique_ptr<TH1>& bias, unique_ptr<TH2>& L)
{
    for (size_t iy = 1; iy <= nYmaxBins; ++iy) {
        const TVectorD * Mjj_edges = genBinning->GetDistributionBinning(0);
        int nMjjBins = Mjj_edges->GetNrows()-1;
        for (int iM = 1; iM <= nMjjBins; ++iM) {

            double Ymax = (  y_edges.at(iy-1) +   y_edges.at(iy)) / 2,
                   Mjj  = ((*Mjj_edges)[iM-1] + (*Mjj_edges)[iM]) / 2;
            int i = genBinning->GetGlobalBinNumber(Mjj, Ymax);
            if (i == 0)
                BOOST_THROW_EXCEPTION( runtime_error("Mjj = "s + Mjj + " and Ymax = "s + Ymax
                                        + " do not correspond to any bin index"s) );

            // b(in)
            int                 bUp     = i-nMjjBins,
                 bLeft   = i-1, bCenter = i         , bRight  = i+1,
                                bDown   = i+nMjjBins;

            // values (curvature regularisation)
            auto get = [&bias](int i) {
                double content = bias->GetBinContent(i);
                if (content < 0)
                    BOOST_THROW_EXCEPTION( DE::BadInput("Expecting only positive entries", bias) );
                return content > 0 ? 1./content : 0;
            };
            double cUp     = get(bUp   ),
                   cLeft   = get(bLeft ),
                   cRight  = get(bRight),
                   cDown   = get(bDown );

            cout << setw(3) << iy << setw(3) << iM
                 << setw(5) << bCenter << setw(15) << (cUp+cLeft+cRight+cDown)
                 << setw(5) << bUp     << setw(15) << -cUp
                 << setw(5) << bLeft   << setw(15) <<     -cLeft
                 << setw(5) << bRight  << setw(15) <<           -cRight
                 << setw(5) << bDown   << setw(15) <<                  -cDown << '\n';

            // filling L-matrix
                            L->SetBinContent(i, bCenter,  cUp+cLeft+cRight+cDown );
            if (cUp    > 0) L->SetBinContent(i, bUp    , -cUp                    );
            if (cLeft  > 0) L->SetBinContent(i, bLeft  ,     -cLeft              );
            if (cRight > 0) L->SetBinContent(i, bRight ,           -cRight       );
            if (cDown  > 0) L->SetBinContent(i, bDown  ,                  -cDown );
        }
    }
    cout << flush;
}
