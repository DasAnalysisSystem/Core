#pragma once

#include <iostream>
#include <list>
#include <memory>

#include "Core/Objects/interface/Variation.h"

#include <TString.h>

#include <MetaInfo.h>

class TH1;
class TH2;
class TDirectory;
class TUnfoldBinning;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Generic structure to hold the histograms and indices corresponding to one
/// variation, for any observable using `TUnfoldBinning`.
///
/// This class is intended to be used in `getUnfHist` only.
struct DistVariation : public DAS::Uncertainties::Variation {

    static bool isMC; //!< flag from metainfo

    static TUnfoldBinning * genBinning, //!< full binning at particle level
                          * recBinning; //!< full binning at detector level

    std::unique_ptr<TH1> rec, //!< reconstructed-level distribution
                         tmp, //!< temporary histogram help fill the covariance matrix
                         gen, //!< generated-level distribution
                         missNoMatch, //!< losses (unmatched entries)
                         missOut, //!< losses (migration out of phase space)
                         fakeNoMatch, //!< background (unmatched entries)
                         fakeOut; //!< background (migration out of phase space)
    /// \todo include miss entries in RM
    /// \todo use TUnfoldBinning to decorrelate categories of miss/fake entries
    std::unique_ptr<TH2> cov, //!< covariance matrix
                         RM; //!< response matrix

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor
    ~DistVariation ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor
    DistVariation (const TString& group, const TString& name, size_t index = 0, int bit = 0);

    ////////////////////////////////////////////////////////////////////////////////
    /// copy constructor
    DistVariation (const DistVariation& v) :
        DAS::Uncertainties::Variation(v),
        rec        (std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.rec        ->Clone()))),
        tmp        (std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.tmp        ->Clone()))),
        gen        (std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.gen        ->Clone()))),
        missNoMatch(std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.missNoMatch->Clone()))),
        missOut    (std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.missOut    ->Clone()))),
        fakeNoMatch(std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.fakeNoMatch->Clone()))),
        fakeOut    (std::unique_ptr<TH1>(dynamic_cast<TH1*>(v.fakeOut    ->Clone()))),
        cov        (std::unique_ptr<TH2>(dynamic_cast<TH2*>(v.cov        ->Clone()))),
        RM         (std::unique_ptr<TH2>(dynamic_cast<TH2*>(v.RM         ->Clone())))
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// move constructor
    DistVariation (DistVariation&& v) :
        DAS::Uncertainties::Variation(v),
        rec(std::move(v.rec)), tmp(std::move(v.tmp)), gen(std::move(v.gen)),
        missNoMatch(std::move(v.missNoMatch)), missOut(std::move(v.missOut)),
        fakeNoMatch(std::move(v.fakeNoMatch)), fakeOut(std::move(v.fakeOut)),
        cov(std::move(v.cov)), RM(std::move(v.RM))
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Create a subdirectory to the directory given in argument where all
    /// histograms are written.
    void Write (TDirectory * d);
};

////////////////////////////////////////////////////////////////////////////////
/// Fill covariance matrix for a given variation (and its `tmp` histogram).
/// The list allows to directly access the non-trivial elements of the histogram.
///
/// \todo make this function a static member of Observable
void fillCov (const DistVariation&, const std::list<int>&);

////////////////////////////////////////////////////////////////////////////////
/// Get all variations availables according to metainfo.
std::vector<DistVariation> GetVariations
            (Darwin::Tools::MetaInfo&, //!< full meta info including variation block
             bool = false, //!< apply systematics
             std::ostream& = std::cout);

} // end of namespace DAS::Unfolding
