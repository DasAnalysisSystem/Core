#pragma once

#include <list>
#include <optional>
#include <vector>

#include <TUnfoldBinning.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Unfolding/interface/Observable.h"

#include "Core/JEC/interface/JMEmatching.h"
#endif

namespace DAS::Unfolding::InclusiveJet {

static const std::vector<double> recBins{/*74,84,*/97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832},
                                 genBins{/*74   ,*/97    ,133    ,174    ,220    ,272    ,330    ,395    ,468    ,548    ,638    ,737    ,846    ,967     ,1101     ,1248     ,1410     ,1588     ,1784     ,2000     ,2238     ,2500     ,2787     ,3103     ,3450,     3832},
                                 y_edges{0., 0.5, 1.0, 1.5, 2.0, 2.5, 3.0};

static const int nRecBins = recBins.size()-1,
                 nGenBins = genBins.size()-1,
                 nYbins   = y_edges.size()-1;

static const double minpt = recBins.front(), maxpt = recBins.back(), maxy = y_edges.back();

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct PtY final : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    PtY ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (Darwin::Tools::Flow&) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::setLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct PtYFiller final : public Filler {
    PtY obs; ///< Backreference to the observable

    std::vector<GenJet> * genJets;
    std::vector<RecJet> * recJets;
    GenEvent * gEv;
    RecEvent * rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    PtYFiller (const PtY&, Darwin::Tools::Flow&);

    ////////////////////////////////////////////////////////////////////////////////
    /// Jet selection
    inline bool selection (double pt, double absy)
    {
        return pt >= minpt && pt < maxpt && absy < maxy;
    }

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (DistVariation&) override;

    std::optional<DAS::JMEmatching<std::vector<DAS::RecJet>,
                                   std::vector<DAS::GenJet>>> matching;

    ////////////////////////////////////////////////////////////////////////////////
    /// Using JME matching
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (DistVariation&) override;
};
#endif

} // end of DAS::Unfolding::InclusiveJet namespace
