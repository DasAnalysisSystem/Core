#pragma once

#include <vector>
#include <list>
#include <optional>

#include <TUnfoldBinning.h>
#include <TH1.h>
#include <TH2.h>

#if !defined(__CLING__) || defined(__ROOTCLING__)
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Di.h"
#include "Core/Unfolding/interface/Observable.h"
#endif

namespace DAS::Unfolding::MNjets {

static const int nRecDEtaBins = 16,
                 nGenDEtaBins =  8,
                 nRecDPhiBins = 20,
                 nGenDPhiBins = 10;

static const double minpt = 35, maxy = 4.7;

#if !defined(__CLING__) || defined(__ROOTCLING__)
struct DEtaDPhi final : public Observable {
    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    DEtaDPhi ();

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructs a filler for the observable
    std::unique_ptr<Filler> getFiller (Darwin::Tools::Flow&) const override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Observable::setLmatrix`
    void setLmatrix (const std::unique_ptr<TH1>&, std::unique_ptr<TH2>&) override;
};

struct DEtaDPhiFiller final : public Filler {
    DEtaDPhi obs; ///< Backreference to the observable

    std::vector<GenJet> * genJets;
    std::vector<RecJet> * recJets;
    GenEvent * gEv;
    RecEvent * rEv;

    ////////////////////////////////////////////////////////////////////////////////
    /// Constructor
    DEtaDPhiFiller (const DEtaDPhi&, Darwin::Tools::Flow&);

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillRec`
    std::list<int> fillRec (DistVariation&) override;

    Di<const GenJet, const GenJet> genMNjets;
    Di<const RecJet, const RecJet> recMNjets;
    std::optional<bool> matched;

    ////////////////////////////////////////////////////////////////////////////////
    /// Check if the MN jets can be matched at both levels.
    void match () override;

    ////////////////////////////////////////////////////////////////////////////////
    /// See `Filler::fillMC`
    void fillMC (DistVariation&) override;
};
#endif

} // end of DAS::Unfolding::MNjets namespace
