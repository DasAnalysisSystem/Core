#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TH2.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/Unfolding/interface/PtY.h"
#include "Core/Unfolding/interface/MjjYmax.h"
#include "Core/Unfolding/interface/HTn.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Make the L-matrix for a given set of observables.
///
/// NOTE:
///  - the x-axis *must* correspond to the axis of the TUnfoldBinning binning
///  - the y-axis only happens to correspond to it as well in *our* approach,
///    but primarily corresponds to an a-priori arbitrary number of conditions
void getLmatrix
           (const fs::path& input, //!< input ROOT file (histograms)
            const fs::path& output, //!< output ROOT file (histograms)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering //!< parameters obtained from explicit options
            )
{
    cout << __func__ << " start" << endl;

    // The matrix inversion only corrects for migrations within the phase space,
    // therefore, we take the projection of the RM on the gen-level axis.
    // For all systematic variations that only affect the detector level, this
    // leads the same bias. If one wants to vary the generator level, then one
    // has to loop over variations as well, since the bias will slightly change
    // (although this is likely to be a negligible effect).
    //
    /// \todo implement L-matrix and bias variations?
    ///       - the weights will change slightly at gen level
    ///       - the rec level spectrum may slightly change
    auto RM = DT::Flow(steering, {input}).GetInputHist<TH2>("nominal/RM");
    auto bias = unique_ptr<TH1>(RM->ProjectionX("bias"));

    auto fOut = DT::GetOutputFile(output);

    cout << "Setting observables" << endl;
    Observable::isMC = true;
    auto pt_obs = config.get_child("unfolding.observables");
    vector<Observable *> observables = GetObservables(pt_obs);

    auto genBinning = new TUnfoldBinning("gen"); /// \todo smart pointer?
    for (Observable * obs: observables)
        genBinning->AddBinning(obs->genBinning);

    /// \todo `(steering & DT::verbose)`

    // L-matrix
    auto L = unique_ptr<TH2>(TUnfoldBinning::CreateHistogramOfMigrations
                                (genBinning, genBinning, "L"));
    for (Observable * obs: observables) {
        cout << obs->genBinning->GetName() << endl;
        obs->setLmatrix(bias, L);
    }

    fOut->cd();
    TDirectory * nom = fOut->mkdir("nominal"); /// \todo the empty bins may change from variation to variation
    nom->cd();
    L->SetDirectory(nom);
    L->Write();
    bias->SetDirectory(nom);
    bias->Write();

    cout << __func__ << " stop" << endl;
}

} // end of DAS::Unfolding namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["TUnfold"] = TUnfold::GetTUnfoldVersion();

        fs::path input, output;

        auto options = DAS::Options(
                            "Make the L-matrix for a given set of observables.",
                            DT::config);
        options.input("input"  , &input, "input MC ROOT file (from `getUnfHist*`)")
               .output("output", &output, "output ROOT file")
               .args("observables", "unfolding.observables", "list of observables (use DAS::Unfolding subnamespaces)");
        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::Unfolding::getLmatrix(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
