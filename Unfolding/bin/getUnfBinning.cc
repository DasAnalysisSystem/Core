#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TUnfoldBinningXML.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/Unfolding/interface/Observable.h"

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Get binning in XML format
void getUnfBinning
           (const fs::path& outputGen, //!< output XML file
            const fs::path& outputRec, //!< output XML file
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering //!< parameters obtained from explicit options
            )
{
    cout << __func__ << " start" << endl;

    cout << "Setting observables" << endl;
    auto pt_obs = config.get_child("unfolding.observables");
    vector<Observable *> observables = GetObservables(pt_obs);

    /// \todo get post-transformation unfolding

    cout << "Adding binnings" << endl;
    TUnfoldBinningXML * genBinning = new TUnfoldBinningXML("gen"),
                      * recBinning = new TUnfoldBinningXML("rec");
    for (Observable * obs: observables) {
        genBinning->AddBinning(obs->genBinning);
        recBinning->AddBinning(obs->recBinning);
    }

    cout << "Exporting" << endl;
    if (outputGen != "/dev/null")
        genBinning->ExportXML(outputGen.c_str());
    if (outputRec != "/dev/null")
        genBinning->ExportXML(outputRec.c_str());

    cout << __func__ << " stop" << endl;
}

} // end of DAS::Unfolding namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["TUnfold"] = TUnfold::GetTUnfoldVersion();

        fs::path outputGen, outputRec;

        auto options = DAS::Options(
                            "Get binning used in unfolding in XML format.",
                            DT::config);
        options.output("outputGen", &outputGen, "output XML file with binning at hadron level (`/dev/null` to deactivate)")
               .output("outputRec", &outputRec, "output XML file with binning at detector level (`/dev/null` to deactivate)")
               .args("observables", "unfolding.observables", "list of observables (use DAS::Unfolding subnamespaces)");
        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::Unfolding::getUnfBinning(outputGen, outputRec, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
