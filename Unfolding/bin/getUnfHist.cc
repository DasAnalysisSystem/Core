#include <cstdlib>

#include <iostream>
#include <vector>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/Unfolding/interface/HTn.h"
#include "Core/Unfolding/interface/DistVariation.h"

#include "Math/VectorUtil.h"
#include "Math/GenVector/GenVector_exception.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding {

////////////////////////////////////////////////////////////////////////////////
/// Get histograms for unfolding only for pt (in d=1),
/// without using `TUnfoldBinning`.
///
/// Then the output can be unfolded using `unfold`.
void getUnfHist
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file
         const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    bool isMC = metainfo.Get<bool>("flags", "isMC");
    int R = metainfo.Get<int>("flags", "R");

    cout << "Setting observables" << endl;
    Observable::isMC = isMC;
    Observable::maxDR = R / 20.;
    auto pt_obs = config.get_child("unfolding.observables");
    vector<Observable *> observables = GetObservables(pt_obs);
    vector<unique_ptr<Filler>> fillers;
    for (const auto obs: observables)
        fillers.push_back(obs->getFiller(flow));
    if (observables.empty())
        BOOST_THROW_EXCEPTION( invalid_argument("You should provide at least one observable "
                                                "(e.g. `InclusiveJet::PtY`)") );

    cout << "Setting variations" << endl;
    DistVariation::isMC = isMC;
    DistVariation::genBinning = new TUnfoldBinning("gen");
    DistVariation::recBinning = new TUnfoldBinning("rec");
    for (Observable * obs: observables) {
        DistVariation::genBinning->AddBinning(obs->genBinning);
        DistVariation::recBinning->AddBinning(obs->recBinning);
    }

    [[ maybe_unused ]]
    static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

    vector<DistVariation> variations = GetVariations(metainfo,
                                                steering & DT::syst, cout);

    for (DT::Looper looper(tIn); looper(); ++looper) {
        for (DistVariation& v: variations) {

            list<int> binIDs; // collect filled bins to calculate covariance
            for (auto& filler: fillers)
                binIDs.merge(filler->fillRec(v));
            fillCov(v, binIDs);

            if (!isMC) continue;

            // run matching (possibly different for each observable)
            for (auto& filler: fillers)
                filler->match();

            // fill gen, RM, fake, miss
            for (auto& filler: fillers)
                filler->fillMC(v);
        }
    }

    metainfo.Set<bool>("git", "complete", true);

    fOut->cd();
    for (DistVariation& v: variations)
        v.Write(fOut);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Unfolding

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["TUnfold"] = TUnfold::GetTUnfoldVersion();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Fill histograms to run global unfolding of "
                            "multiple observables.", DT::syst | DT::split | DT::config);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               )
               .args("observables", "unfolding.observables", "list of observables (use DAS::Unfolding subnamespaces)");

        const auto& config = options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Unfolding::getUnfHist(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
