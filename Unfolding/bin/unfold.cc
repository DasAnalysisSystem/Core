#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <limits>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TLegend.h>
#include <TKey.h>
#include <TMatrixD.h>
#include <TVectorD.h>
#include <TDecompSVD.h>

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/Objects/interface/Format.h"
#include "Core/Unfolding/interface/MyTUnfoldDensity.h"
#include "Core/Unfolding/interface/toolbox.h"

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

static const auto deps = numeric_limits<double>::epsilon();

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Unfolding {

namespace {

template<typename T> inline unique_ptr<T> ReadObj (TKey * key)
{
    return unique_ptr<T>(dynamic_cast<T*>(key->ReadObj()));
}

template<typename T> inline unique_ptr<T> Clone (const unique_ptr<T>& obj, const char * newname)
{
    return unique_ptr<T>(dynamic_cast<T*>(obj->Clone(newname)));
}

} // end of anonymous namespace

////////////////////////////////////////////////////////////////////////////////
/// CorrectInefficiencies
///
/// Since miss entries are not accounted for in the migrations of the RM, they
/// must be corrected after the unfolding
void CorrectInefficiencies (unique_ptr<TH1>& unf, const map<TString, unique_ptr<TH1>>& miss)
{
    cout << __LINE__ << '\t' << __func__ << endl;
    for (int i = 1; i <= unf->GetNbinsX(); ++i) {
        double content = unf->GetBinContent(i),
               factor = 1;
        for (auto& m: miss) factor += m.second->GetBinContent(i);
        content *= factor;
        unf->SetBinContent(i, content);
        // Note: we avoid explicitely the use of TH1::Multiply()
        //       in order to have full control on the propagation
        //       of the uncertainties (done later with cov matrix)
    }
    cout << flush;
}

namespace Migrations {

////////////////////////////////////////////////////////////////////////////////
/// Condition of the Probability Matrix
///
/// Calculates the condition of the probability matrix with the gen binning.
///
/// Note: we do *not* include the miss entries in the normalisation since they
///       are *not* included in inverted probability matrix
double Condition (const unique_ptr<TH2>& RM)
{
    cout << __LINE__ << "\tCalculating condition" << endl;
    const int Nx = RM->GetNbinsX(),
              Ny = RM->GetNbinsY();

    auto RMx = unique_ptr<TH1>(RM->ProjectionX("RMx", 0, -1));

    // normalisation & condition
    TMatrixD m(Ny,Nx); // unfortunately, we have to swap the axes...
    for (int i = 1; i <= Nx; ++i) {
        double normalisation = RMx->GetBinContent(i);
        if (normalisation > 0)
        for (int j = 1; j <= Ny; ++j)
            m(j-1,i-1) = RM->GetBinContent(i,j) / normalisation;
    }
    TDecompSVD svd(m);
    TVectorD v = svd.GetSig();

    double Max = v[0], Min = v[0];
    for(int k = 0; k < Nx; ++k) {
        if (abs(v[k]) < 1e-5) break;
        Min = v[k];
        cout << setw(5) << k << setw(15) << v[k] << setw(15) << Max/Min << '\n';
    }
    if (Min > 0) return Max/Min;
    else         return -numeric_limits<double>::infinity();
}

} // end of RM namespace

namespace Checks {

////////////////////////////////////////////////////////////////////////////////
/// Check negative entries
///
/// Note: you should not necessarily worry if you get a value compatible with zero
///       BUT you should not trust the result if the value is not at all
///       compatible with zero
void NegativeBins (const unique_ptr<TH1>& unf)
{
    cout << __LINE__ << '\t' << __func__ << endl;
    for (int i = 1; i <= unf->GetNbinsX(); ++i) {
        double content = unf->GetBinContent(i);
        if (content >= 0) continue;
        double error = unf->GetBinError(i);
        cerr << __LINE__ << orange << "\tWarning: bin " << i
             << " has negative content: " << content << " +/- " << error << '\n' << def;
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Calculate probability matrix
unique_ptr<TH2> GetPM (const unique_ptr<TH2>& RM)
{
    unique_ptr<TH2> PM = Clone<TH2>(RM, "PM");

    const int Nx = RM->GetNbinsX(),
              Ny = RM->GetNbinsY();

    // normalisation
    for (int i = 1; i <= Nx; ++i) {
        double normalisation = RM->Integral(i, i, 1, Ny);
        if (normalisation > 0)
        for (int j = 1; j <= Ny; ++j) {
            double content = RM->GetBinContent(i,j);
            content /= normalisation;
            PM->SetBinContent(i,j, content);
        }
    }

    return PM;
}

////////////////////////////////////////////////////////////////////////////////
/// Folding test
///
/// Check that the MC closes: (gen - sum of misses) * PM = (rec - sum of fakes)
void folding (const unique_ptr<TH1>& genIn, //!< input gen-level spectrum
              const unique_ptr<TH1>& recIn, //!< input rec-level spectrum
              const unique_ptr<TH2>& PMin, //!< input probability matrix
              const map<TString, unique_ptr<TH1>>& miss, //!< miss counts
              const map<TString, unique_ptr<TH1>>& fake) //!< fake counts
{
    cout << __LINE__ << '\t' << __func__ << endl;

    unique_ptr<TH1> gen = Clone<TH1>(genIn, "gen_folding");
    unique_ptr<TH1> rec = Clone<TH1>(recIn, "rec_folding");
    unique_ptr<TH2> PM  = Clone<TH2>(PMin ,  "PM_folding");

    for (auto& m: miss) gen->Add(m.second.get(), -1);
    for (auto& f: fake) rec->Add(f.second.get(), -1);

    const int Nx = PM->GetNbinsX(),
              Ny = PM->GetNbinsY();

    // check folding
    bool Fail = false;
    for (int j = 1; j <= Ny; ++j) {
        double error = rec->GetBinError(j);
        if (::abs(error) < deps) continue;

        double content = rec->GetBinContent(j);
        Fail |= isnan(content);

        for (int i = 1; i <= Nx; ++i)
            content -= gen->GetBinContent(i) * PM->GetBinContent(i,j);

        if (::abs(content) < error / 100) continue;
        Fail = true;
        cerr << __LINE__ << orange << "\tWarning: Folding test is failing in bin "
             << j << " with (gen - miss) x PM - (rec - fake) = " << content
             << " > error / 100 = " << (error / 100) << def << '\n';
    }

    if (Fail) /*BOOST_THROW_EXCEPTION( std::runtime_error(*/ cerr << red << "Failure: folding test has failed\n" << def /*) )*/;
}

////////////////////////////////////////////////////////////////////////////////
/// Bottom Line Test
///
/// Checks the agreement of data and MC before and after unfolding
///
/// \todo current implementation is insufficient for complex binning schemes
void BLT (const unique_ptr<TH1>& dataDist,
          const unique_ptr<TH2>& dataCov,
          const unique_ptr<TH1>& MC,
          int rebin = 1)
{
    cout << __LINE__ << '\t' << __func__ << endl;
    cerr << __LINE__ << '\t' << orange << "Warning: the BLT only gives reasonable "
                                          "results for simples rebinning schemes "
                                          "(e.g. 2 rec bins for 1 gen bins)\n" << def;
    auto reb_dataDist = Clone<TH1>(dataDist, "reb_dataDist");
    auto reb_dataCov  = Clone<TH2>(dataCov,  "reb_dataCov" );
    auto reb_MC       = Clone<TH1>(MC,       "reb_MC"      );

    reb_dataDist->Rebin(rebin);
    reb_dataCov->Rebin2D(rebin, rebin);
    reb_MC->Rebin(rebin);
    vector<int> indices;
    for (int i = 1; i <= dataCov->GetNbinsX(); ++i)
        if (dataCov->GetBinContent(i,i) > 0) indices.push_back(i);
    int ndf = indices.size();

    TVectorD v(ndf);
    TMatrixD M(ndf,ndf);
    M.SetTol(1e-100);
    for (int i = 0; i < ndf; ++i) {
        int index = indices.at(i);
        double iData = dataDist->GetBinContent(index),
               iMC   = MC      ->GetBinContent(index);
        v(i) = iData-iMC;
        for (int j = 0; j < ndf; ++j) {
            int jndex = indices.at(j);
            M(i,j) = dataCov->GetBinContent(index,jndex);
        }
    }
    M.Invert();

    double chi2 = v*(M*v);

    cout << "chi2/ndf = " << chi2 << " / " << ndf << " = " << (chi2/ndf) << endl;
}

////////////////////////////////////////////////////////////////////////////////
/// Set underflow/overflow to 0
void CovMargin (unique_ptr<TH1>& h, unique_ptr<TH2>& cov, TString name, int ibin)
{
    if (   h->GetBinContent(ibin) == 0
        && h->GetBinError(ibin) == 0
        && cov->GetBinContent(ibin,ibin) == 0) return;

    cerr << __LINE__ << orange << "\tWarning: " << name << " is not empty! "
            "It will be set to zero, but no guarantee it works...\n" << def;

    h->SetBinContent(ibin,0);
    h->SetBinError  (ibin,0);
    for (int i = 0; i <= h->GetNbinsX(); ++i) {
        cov->SetBinContent(ibin,i,0);
        cov->SetBinContent(i,ibin,0);
    }
}

} // end of Checks namespace

namespace Tikhonov {

////////////////////////////////////////////////////////////////////////////////
/// Prepare regularisation
///
/// Load bias and L-matrix from the output file of one of the `getLmatrix*` commands
void PrepareRegularisation (TString MCvar, const char * fname,
                            unique_ptr<MyTUnfoldDensity>& density)
{
    cout << __LINE__ << '\t' << __func__ << endl;
    cerr << __LINE__ << '\t' << "Warning: the implementation of the regularisation "
                                "is not yet stable. For instance, it is not expected "
                                "to work correctly in case of empty bins. Use at your "
                                "own risk.\n" << def;

    auto fReg = make_unique<TFile>(fname, "READ");

    cout << __LINE__ << "\tgetting bias" << endl;
    auto bias = unique_ptr<TH1>(fReg->Get<TH1>(MCvar + "/bias"));
    if (!bias) BOOST_THROW_EXCEPTION( DE::BadInput("Could not find the bias", fReg) );
    density->SetBias(bias.get());

    // getting L matrix
    cout << __LINE__ << "\tgetting L-matrix" << endl;
    auto L = unique_ptr<TH2>(fReg->Get<TH2>(MCvar + "/L"));
    if (!L) BOOST_THROW_EXCEPTION( DE::BadInput("Could not find the L-matrix", fReg) );

    int Nconditions = L->GetNbinsY(), // arbitrarily large number
        NtrueBins   = L->GetNbinsX(); // must be exactly the number of bins at particle level

    /// \todo calculate \f$ A^T_{yy} V^{-1} A \f$ & adapt the L matrix to have the same rank

    cout << __LINE__ << "\tlooping over " << Nconditions << " conditions" << endl;
    for (int j = 1; j <= Nconditions; ++j) { // y-axis

        vector<int> indices;
        vector<double> conditions;
        indices.reserve(Nconditions);
        conditions.reserve(Nconditions);

        for (int i = 1; i <= NtrueBins; ++i) { // x-axis
            double content = L->GetBinContent(i, j);
            if (std::abs(content) < deps) continue;
            indices.push_back(i);
            conditions.push_back(content);
        }

        // cheating with call to protected method
        if (indices.size() != conditions.size())
            BOOST_THROW_EXCEPTION( DE::BadInput("inconsistent number of indices and conditions",
                                                fReg) );

        density->MyAddRegularisationCondition(indices.size(), indices.data(), conditions.data());
    }
    cout << __LINE__ << "\tregularisation is ready" << endl;
}

////////////////////////////////////////////////////////////////////////////////
/// RedoUnfold
///
/// Repeat unfolding for alternative values of tau parameter
void RedoUnfold (unique_ptr<MyTUnfoldDensity>& density, double newtau,
                 TString name, TString subtitle,
                 shared_ptr<TFile> fOut, const map<TString, unique_ptr<TH1>>& miss)
{
    auto dOut = fOut->mkdir(name, subtitle);
    density->DoUnfold(newtau);
    auto unf = unique_ptr<TH1>(density->GetOutput("unfold", "particle level (" + subtitle + ")"));
    CorrectInefficiencies(unf, miss);
    Checks::NegativeBins(unf);
    unf->SetDirectory(dOut);
    dOut->cd();
    unf->GetXaxis()->SetTitle("gen");
    unf->Write();
    /// \todo cov (needed for smoothing)
}

////////////////////////////////////////////////////////////////////////////////
/// Save L-curve and other curves, as well as solutions
double ScanLcurve (unique_ptr<MyTUnfoldDensity>& density,
                   TDirectory * dOut, const pt::ptree& config
                  )
{
    cout << __LINE__ << "\tScanning L-curve" << endl;

    TGraph * lCurve = nullptr;
    TSpline * logTauX = nullptr,
            * logTauY = nullptr,
            * logTauCurvature = nullptr;

    auto nPoints = config.get_optional<int>  ("unfolding.TUnfold.regularisation.Lcurve.nPoints").value_or(100);
    auto mintau = config.get_optional<double>("unfolding.TUnfold.regularisation.Lcurve.mintau").value_or(1e-6),
         maxtau = config.get_optional<double>("unfolding.TUnfold.regularisation.Lcurve.maxtau").value_or(1e+2);
    int iBest = density->ScanLcurve(nPoints, mintau, maxtau,
                                    &lCurve, &logTauX, &logTauY, &logTauCurvature);

    cout << __LINE__ << "\tSaving control plots for L-curve scan" << endl;
    Double_t t[1],x[1],y[1], c[1];
    logTauX->GetKnot(iBest,t[0],x[0]);
    logTauY->GetKnot(iBest,t[0],y[0]);
    logTauCurvature->GetKnot(iBest,t[0],c[0]);
    auto bestLcurve          = make_unique<TGraph>(1,x,y);
    auto bestLogTauX         = make_unique<TGraph>(1,t,x);
    auto bestLogTauY         = make_unique<TGraph>(1,t,y);
    auto bestLogTauCurvature = make_unique<TGraph>(1,t,c);

    double chi2a = density->GetChi2A(),
           chi2l = density->GetChi2L();
    int ndf = density->GetNdf();
    if (ndf == 0) BOOST_THROW_EXCEPTION( runtime_error("Can't regularise if ndf = 0") );
    cout << __LINE__ << "\tchi^2/ndf = " << chi2a/ndf << " + " << chi2l/ndf << endl;

    double tau = density->GetTau();
    cout << __LINE__ << "\ttau = " << tau << endl;

    lCurve    ->SetNameTitle("lCurve"    , Form("#tau = %f", tau));
    bestLcurve->SetNameTitle("bestLcurve", Form(       "%f", tau));

    logTauX    ->SetNameTitle("logTauX", "log(#chi_{A}^{2})");
    bestLogTauX->SetNameTitle("bestLogTauX", Form("#chi_{A}^{2}/ndf = %f", chi2a/ndf));

    logTauY    ->SetNameTitle("logTauY", "log(#chi_{L}^{2}/#tau)");
    bestLogTauY->SetNameTitle("bestLogTauY", Form("#chi_{L}^{2}/ndf = %f", chi2l/ndf));

    logTauCurvature    ->SetNameTitle("logTauCurvature", Form("%f", tau));
    bestLogTauCurvature->SetNameTitle("bestLogTauCurvature", Form("%f", tau));

    dOut->cd();
    lCurve->Write();           bestLcurve->Write();
    logTauX->Write();          bestLogTauX->Write();
    logTauY->Write();          bestLogTauY->Write();
    logTauCurvature->Write();  bestLogTauCurvature->Write();

    auto L  = unique_ptr<TH2>(density->GetL          ("L"          , "L-matrix"         ));
    auto Lx = unique_ptr<TH1>(density->GetLxMinusBias("LxMinusBias", "L*(x-f_{b}*x_{0})"));
    L ->SetDirectory(dOut);
    Lx->SetDirectory(dOut);
    L ->Write();
    Lx->Write();

    delete lCurve;
    delete logTauX;
    delete logTauY;
    delete logTauCurvature;

    return tau;
}

} // end of Tikhonov namespace

////////////////////////////////////////////////////////////////////////////////
/// Unfolding output from one of the `getUnfHistograms*.C` executables (indifferently)
///
/// Note that this code does *not* require `TUnfoldBinning`. Indeed,
/// `TUnfoldBinning` is used to make the mapping from ND to 1D in other
/// executables, but it is not necessary to make the unfolding itself.
/// In fact, on purpose, we do *not* want to know the meaning of the distribution
/// in this executable, to keep it as generic as possible.
///
/// Comment on the structure of the code:
/// - nested loops, one over the variations in MC, the other over the variations in data
/// - we vary either MC or Data, but never both at the same time
/// - we keep the same structure of directories as in the inputs, and add additional
///   directories corresponding to unfolding variations, and additional control
///   distributions about the unfolding
///
/// \todo DAgostini (at least for nominal value)
void unfold
        (const fs::path& inputData, //!< input ROOT files (histograms)
         const fs::path& inputMC, //!< input ROOT files (histograms)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    auto sysUncorr = config.get_optional<fs::path>("unfolding.sysUncorr"),
         Lmatrix   = config.get_optional<fs::path>("unfolding.TUnfold.regularisation.Lmatrix");
    const bool applySyst = steering & DT::syst;

    cout << __LINE__ << "\tOpening input files" << endl;
    auto fData = make_unique<TFile>(inputData.c_str(), "READ"),
         fMC   = make_unique<TFile>(inputMC  .c_str(), "READ");

    /// \todo merge the MetaInfo from the two (or more??) inputs
    /// - compare flags, git, corrections
    ///   - merge
    ///   - send warning if relevant but proceed anyway
    /// - merge histories & variations
    /// - find a solution for the preseed (Q: is it necessary for the data?)
    /// - then add block on unfolding but preserve the cycle

    cout << __LINE__ << "\tPreparing output file" << endl;
    auto fOut = DT::GetOutputFile(output);

    fMC->cd();
    // loop over the entries of a TFile / TDirectory
    for (const auto&& objMC: *(fMC->GetListOfKeys())) {

        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        auto keyMC = dynamic_cast<TKey*>(objMC);
        const TString classname = keyMC->ReadObj()->ClassName();
        //if (classname == "TTree") {
        //    unique_ptr<TTree> tree = ReadObj<TTree>(keyMC);
        //    auto List = tree->GetUserInfo();
        //    fOut->cd();
        //    List->Write("MC");
        //    fMC->cd();
        //}
        if (!classname.Contains("TDirectory")) continue;

        const TString MCvar = objMC->GetName();
        cout << __LINE__ << '\t' << MCvar << endl;
        if (MCvar != "nominal" && !applySyst) continue;

        auto dirMC = dynamic_cast<TDirectory*>(keyMC->ReadObj());
        dirMC->cd();

        cout << __LINE__ << "\tExtracting miss and fake counts" << endl;
        map<TString, unique_ptr<TH1>> miss, // only to be added by hand at the end
                                      fake; // seen as backgrounds from the point of view of TUnfold
        // reminder: in principle, one source of miss / fake is enough for the unfolding,
        // but it's good to be able to disentangle the different sources

        // loop over the entries of a TFile / TDirectory
        // here, we want to retrieve only miss and fake
        for (const auto&& objMC2: *(dirMC->GetListOfKeys())) {
            auto keyMC = dynamic_cast<TKey*>(objMC2);
            if (!TString(keyMC->ReadObj()->ClassName()).Contains("TH") ) continue;

            unique_ptr<TH1> h = ReadObj<TH1>(keyMC);
            TString name = h->GetName();
            if (!name.Contains("miss") && !name.Contains("fake")) continue;
            if (h->GetEntries() == 0)
                cerr << orange << "`" << name << "` is empty\n" << def;
            name.ReplaceAll(MCvar, "");
            h->SetDirectory(nullptr);
            if (name.Contains("fake")) {
                name.ReplaceAll("fake", "");
                cout << __LINE__ << "\tfake\t" << name << endl;
                fake[name] = std::move(h);
            }
            else {
                name.ReplaceAll("miss", "");
                cout << __LINE__ << "\tmiss\t" << name << endl;
                miss[name] = std::move(h);
            }
        } // end of retrieving of miss/fake histograms from MC

        cout << __LINE__ << "\tExtracting RM from MC" << endl;
        auto RM = unique_ptr<TH2>(dirMC->Get<TH2>("RM"));
        if (!RM)
            BOOST_THROW_EXCEPTION( DE::BadInput("Missing response "
                                                "matrix", *dirMC) );
        RM->SetDirectory(nullptr);

        auto genMC = unique_ptr<TH1>(dirMC->Get<TH1>("gen")),
             recMC = unique_ptr<TH1>(dirMC->Get<TH1>("rec"));
        if (!genMC)
            BOOST_THROW_EXCEPTION( DE::BadInput("Missing gen "
                                                "histogram", *dirMC) );
        genMC->SetName("genMC");
        recMC->SetName("recMC");
        genMC->SetDirectory(nullptr);
        recMC->SetDirectory(nullptr);

        unique_ptr<TH2> PM = Checks::GetPM(RM);
        PM->SetDirectory(nullptr);
        Checks::folding(genMC, recMC, PM, miss, fake);

        cout << __LINE__ << "\tCalculating miss and fake rates" << endl;

        // we want a fake/miss >> RATE << (i.e. a value between 0 and 1)
        // -> we will use it later to estimate the fake/miss contribution
        //    in the data using a bin-by-bin method.
        // currently: the elements of `fake`/`miss` are currently fake/miss *counts*
        //            (including model dependence)
        unique_ptr<TH1> genMC_noMiss = Clone(genMC, "genMC_noMiss");
        for (auto& f: fake) f.second    ->Divide(f.second.get(), recMC.get(), 1, 1, "b");
        for (auto& m: miss) genMC_noMiss->Add   (m.second.get(), -1);
        for (auto& m: miss) m.second    ->Divide(m.second.get(), genMC_noMiss.get(), 1, 1, "b");
        // now: the elements of `fake`/`miss` are currently fake/miss *rates*

        // now we are done with retrieving objects from fMC, and we move to fData

        fData->cd();

        // now we estimate the fake contribution in data with bin-by-bin method
        {
            auto rec = unique_ptr<TH1>(fData->Get<TH1>("nominal/rec"));
            // reminder: the elements of `fake` are currently fake *rates*
            for (auto& f: fake) f.second->Multiply(rec.get());
            // now: the elements of `fake` are currently fake *counts* (corrected to data)
        }
        // (similar thing will be done with the miss contribution *after* the unfolding)

        // reminder: now we have absolute fake count BUT miss rate

        cout << __LINE__ << "\tExtracting data variations and running unfolding" << endl;

        // here we loop again on the entries of a file
        for (const auto&& objData: *(fData->GetListOfKeys())) {

            auto keyData = dynamic_cast<TKey*>(objData);
            const TString classname = keyData->ReadObj()->ClassName();
	    /// \todo Propagate metainfo
            //if (classname == "TTree") {
            //    unique_ptr<TTree> tree = ReadObj<TTree>(keyData);
            //    auto List = tree->GetUserInfo();
            //    fOut->cd();
            //    List->Write("Data");
            //    fMC->cd();
            //}
            if (!classname.Contains("TDirectory")) continue;

            const TString DATAvar = objData->GetName();
            if (DATAvar != "nominal") {
                if (!applySyst) continue;
                if (MCvar != "nominal") continue;
            }
            cout << __LINE__ << "\tvariation: " << DATAvar << endl;
            TString var = MCvar == "nominal" ? DATAvar : MCvar;

            if (var != "nominal" && MCvar == DATAvar)
                cerr << orange << "The same variation has been found "
                                  "in both data and MC.\n" << def;

            {
                static int ivar = 0; // used for parallelisation
                ++ivar;
                if (slice.second != ivar % slice.first) continue;
            }

            auto dirData = dynamic_cast<TDirectory*>(keyData->ReadObj());
            dirData->cd();

            auto recData = unique_ptr<TH1>(dirData->Get<TH1>("rec"));
            recData->SetName("recData");
            recData->SetTitle("detector level (" + var + ")");
            recData->GetXaxis()->SetTitle("rec");
            recData->SetDirectory(nullptr);
            auto covInData = unique_ptr<TH2>(dirData->Get<TH2>("cov"));
            covInData->SetName("covInData");
            covInData->SetTitle("covariance matrix at detector level (" + var + ")");
            covInData->GetXaxis()->SetTitle("rec");
            covInData->GetYaxis()->SetTitle("rec");
            covInData->SetDirectory(nullptr);
            auto covInMC = unique_ptr<TH2>(dirMC->Get<TH2>("cov"));
            covInMC->SetName("covInMC");
            covInMC->SetTitle("covariance matrix at detector level (" + var + ")");
            covInMC->GetXaxis()->SetTitle("rec");
            covInMC->GetYaxis()->SetTitle("rec");
            covInMC->SetDirectory(nullptr);

            cout << __LINE__ << '\t' << "\e[1m" << DATAvar << "\e[0m" << endl;

            if (DATAvar.Contains("nominal")) { // if data variation is nominal,
                                               // but the condition number will
                                               // be calculated for each variation of MC
                auto condition = Migrations::Condition(RM);
                RM->SetTitle(Form("%.1f",condition)); // keep track of the condition number
            }

            // underflow and overflow are expected to be empty (else, set to 0 with a warning)
            Checks::CovMargin(recData, covInData, "underflow", 0);
            Checks::CovMargin(recData, covInData, "overflow", recData->GetNbinsX()+1);

            // so far, still no unfolding done

            cout << __LINE__ << "\tSetting up density & input (checking "
                                "and comparing the bin contents of data and MC)" << endl;

            auto density = make_unique<MyTUnfoldDensity>(
                    RM.get(), // reminder: this is varied by the first loop
                    TUnfold::kHistMapOutputHoriz, // truth level on x-axis of the response matrix
                    TUnfold::kRegModeNone, // do not use *predefined* regularisation schemes
                    TUnfold::kEConstraintNone, // no constraint on area
                    TUnfoldDensity::kDensityModeBinWidthAndUser); // no scale factors

            // reminder: the epsilon is the smallest possible number
            density->SetEpsMatrix(1e-100); /// \todo Eps matrix of MyTUnfoldDensity != 1e-100?

            int status = density->SetInput(
                    recData.get(), // reconstructed level
                    0, // bias factor (only relevant for Tikhonov regularisation)
                    0, // for bins with zero error, this number defines 1/error
                    covInData.get()); // covariance matrix

            if (status > 0)
                cerr << __LINE__ << '\t' << orange << "unfold status = " << status << '\n' << def;

            // now, we should have (nearly) everything settled for the unfolding...

            cout << __LINE__ << "\tSubtract background" << endl;
            auto fakeNormVar = config.get_optional<float>("unfolding.fakeNormVar").value_or(0.05);
            cout << __LINE__ << "\tfakeNormVar = " << fakeNormVar << endl;
            for (auto& f: fake) { // fake now contains esimations of the fake contributions in data
                cout << __LINE__ << '\t' << f.first << endl;
                density->SubtractBackground(
                        f.second.get(), // 1D histogram in rec-level binning (with its own stat use
                                        // --> we will use later `GetEmatrixSysBackgroundUncorr()`
                        "fake" + f.first, // name
                        1.0, // scale factor
                        fakeNormVar); // factor to vary the normalisation
                                      // --> we will use later `GetDeltaSysBackgroundScale()`
                // the lines just above are equivalent to three things:
                //  - `h->Add(f.second, -1);`, i.e. subtract the fake contribution
                //    from the rec level
                //  - it propagates the contribution from the limited statistics
                //    to the covariance matrix used in the unfolding
                //  - TUnfold provides systematic variations
            }

            if (sysUncorr) {
                cout << __LINE__ << "\tAdding bin-to-bin uncertainty" << endl;

                if (!fs::exists(*sysUncorr))
                    BOOST_THROW_EXCEPTION(fs::filesystem_error("File with systematic "
                                "bin-to-bin uncorrelated uncertainties was not found!",
                                make_error_code(errc::no_such_file_or_directory)));

                auto fSys = make_unique<TFile>(sysUncorr->c_str(), "READ");
                auto hSysUncorr = unique_ptr<TH1>( fSys->GetDirectory(DATAvar)
                                                    ->Get<TH1>("sysUncorr1D") );
                density->SubtractBackground(hSysUncorr.get(), "sysUncorr", 1.0, 0.);
            }

            fOut->cd();
            if (fOut->GetDirectory(var)) continue;
            auto dOut = fOut->mkdir(var);
            dOut->cd();

            cout << __LINE__ << "\tUnfolding" << endl;
            static auto tau = config.get_optional<float>("unfolding.TUnfold.regularisation.tau");
            if (Lmatrix) {
                cout << __LINE__ << "\tFetching L-matrix" << endl;

                if (!fs::exists(*Lmatrix))
                    BOOST_THROW_EXCEPTION(fs::filesystem_error("File with L-matrix not found!",
                                            make_error_code(errc::no_such_file_or_directory)));

                Tikhonov::PrepareRegularisation("nominal", Lmatrix->c_str(), density); /// \todo syst?
                if (!tau && var == "nominal") {

                    tau = Tikhonov::ScanLcurve(density, dOut, config);

                    if (applySyst) {
                        cout << __LINE__ << "\tSaving shifts for variations of tau parameter" << endl;
                        Tikhonov::RedoUnfold(density, *tau*2, "Regularisation" + SysUp,
                                   "upper variation of regularisation", fOut, miss);
                        Tikhonov::RedoUnfold(density, *tau/2, "Regularisation" + SysDown,
                                   "lower variation of regularisation", fOut, miss);
                    }
                }
            } // end of regularise
            cout << __LINE__ << "\ttau = " << tau << endl;
            density->DoUnfold(tau.value_or(0));

            // now that the unfolding is done, retrieve the result (use TUnfold)
            // and apply the correction for miss entries (by hand)

            cout << __LINE__ << "\tGetting output" << endl;
            auto unf = unique_ptr<TH1>(density->GetOutput("unfold", "particle level (" + var + ")"));
            Checks::NegativeBins(unf);
            CorrectInefficiencies(unf, miss);
            dOut->cd();
            unf->SetDirectory(dOut);
            unf->GetXaxis()->SetTitle("gen");
            unf->Write();

            /// \todo run CT if the gen level is available (= pseudodata)
            ///       - if inputData = inputMC -> expect perfect agreement
            ///       - otherwise, interpretation may depend...

            cout << __LINE__ << "\tCovariance matrix from measurement" << endl;
            auto covOutData = unique_ptr<TH2>(density->GetEmatrixInput("covOutData", // name
                "covariance matrix coming from input data distribution (" + var + ")")); // title
            covOutData->SetDirectory(dOut);
            covOutData->GetXaxis()->SetTitle("gen");
            covOutData->GetYaxis()->SetTitle("gen");
            covOutData->Write();

            cout << __LINE__ << "\tSaving correlation matrix from measurement" << endl;
            SaveCorr(covInData , "corrInData" , dOut); // transform cov matrix into corr matrix
            SaveCorr(covOutData, "corrOutData", dOut); // transform cov matrix into corr matrix

            // main reason to do it is to get the condition number (saved in the title)
            // -> otherwise, it should be identical to the input `fMC` file
            cout << __LINE__ << "\tSaving response and probability matrices" << endl;
            RM->SetDirectory(dOut);
            RM->Write("RM"); // title was already set earlier (corresponding to condition number)
            RM->SetDirectory(nullptr);
            PM->SetDirectory(dOut);
            PM->Write();

            cout << __LINE__ << "\tGetting backfolding" << endl;
            auto backfolding = unique_ptr<TH1>(density->GetFoldedOutput(
                        "backfold", // histogram name
                        "backfolded (detector level)", // histogram title
                        nullptr /* dist name */, nullptr /* axis steering */,
                        true /* axis binning */, true /* add bg */));
            backfolding->SetDirectory(dOut);
            backfolding->GetXaxis()->SetTitle("rec");
            backfolding->Write();
            recData->Write("recData");
            covInData->Write();

            genMC->SetDirectory(dOut);
            genMC->Write();
            genMC->SetDirectory(nullptr);

            recMC->SetDirectory(dOut);
            recMC->Write();
            recMC->SetDirectory(nullptr);
            covInMC->SetDirectory(dOut);
            covInMC->Write();

            cout << __LINE__ << "\tGetting systematic uncertainties from MC and from unfolding"
                 << endl;

            // just initialising histogram to contain contribution from the limited statistics
            // of the MC (not only from RM, but also from fake and miss histograms)
            auto covOutUncor = Clone<TH2>(covOutData, "covOutUncor");
            covOutUncor->SetTitle("covariance matrix at particle level coming from the MC "
                                  "(RM + miss + fake)");
            covOutUncor->Reset();

            auto dUncorr = dOut->mkdir("Uncorr");
            dUncorr->cd();
            cout << __LINE__ << "\tCovariance matrix from RM" << endl;
            auto SysUncorr = unique_ptr<TH2>(density->GetEmatrixSysUncorr("RM", // name
                    "covariance matrix at particle level coming from the RM")); // title
            SysUncorr->SetDirectory(dUncorr);
            SysUncorr->GetXaxis()->SetTitle("gen");
            SysUncorr->Write();
            covOutUncor->Add(SysUncorr.get());
            cout << __LINE__ << "\tCovariance matrices from backgrounds" << endl;
            for (auto& f: fake) {
                auto SysBackgroundUncorr = unique_ptr<TH2>(density->GetEmatrixSysBackgroundUncorr(
                        "fake" + f.first, // source
                        "fake" + f.first, // histogram name
                        Form("covariance matrix coming from %s", f.second->GetTitle())
                        ));
                SysBackgroundUncorr->SetDirectory(dUncorr);
                SysBackgroundUncorr->Write();
                covOutUncor->Add(SysBackgroundUncorr.get());
            }

            if (sysUncorr) { /// \todo remove?
                cout << __LINE__ << "\tCovariance matrices from input bin-to-bin "
                                    "uncorrelated systematic uncertainties." << endl;
                auto SysBackgroundUncorr = unique_ptr<TH2>(
                        density->GetEmatrixSysBackgroundUncorr(
                            "sysUncorr", "sysUncorr",
                            "covariance matrix from trigger and prefiring uncertainties"));
                SysBackgroundUncorr->SetDirectory(dUncorr);
                SysBackgroundUncorr->Write();
                covOutUncor->Add(SysBackgroundUncorr.get());
            }
            cout << __LINE__ << "\tCovariance matrices from inefficiencies" << endl;
            for (const auto& m: miss) { // only using miss for the names
                auto SysIneffUncorr = Clone<TH2>(SysUncorr, "miss" + m.first);
                SysIneffUncorr->Reset();
                for (int i = 1; i <= m.second->GetNbinsX(); ++i) {
                    double error = m.second->GetBinError(i) * ::abs(unf->GetBinContent(i));
                    SysIneffUncorr->SetBinContent(i, i, pow(error, 2));
                }
                SysIneffUncorr->SetDirectory(dUncorr);
                SysIneffUncorr->Write();
                covOutUncor->Add(SysIneffUncorr.get());
            }
            dOut->cd();
            covOutUncor->SetDirectory(dOut);
            covOutUncor->Write();

            auto covOutTotal = Clone<TH2>(covOutData, "covOutTotal");
            covOutTotal->Add(covOutUncor.get());
            covOutTotal->SetTitle("covariance matrix at particle level including "
                                  "contributions from data and MC");
            covOutTotal->SetDirectory(dOut);
            covOutTotal->Write();
            SaveCorr(covOutTotal, "corrOutTotal", dOut);

            cout << __LINE__ << "\tSaving estimates of fake counts" << endl;
            for (auto& f: fake) {
                TString name = "fake" + f.first,
                        title = f.second->GetTitle();
                cout << __LINE__ << '\t' << name << endl;

                // count
                f.second->SetDirectory(dOut);
                f.second->SetTitle(title + " (corrected)");
                f.second->Write(name);
                f.second->SetDirectory(nullptr);
            }

            if (var == "nominal" && applySyst) {
                cout << __LINE__ << "\tSaving shifts from background variations "
                                    "in parallel directories" << endl;
                fOut->cd();
                for (auto& f: fake) {
                    TString name = "fake" + f.first,
                            title = f.second->GetTitle();
                    cout << __LINE__ << '\t' << name << endl;

                    // upper shift
                    {
                        auto shift = unique_ptr<TH1>(density->GetDeltaSysBackgroundScale(
                                    name, // name of the source given in `SubtractBackground()`
                                    name + SysUp, // name of new histogram
                                    "")); /// \todo title of new histogram
                        shift->Add(unf.get());
                        auto dOut = fOut->mkdir(name + SysUp, "upper shift of " + title);
                        shift->SetDirectory(dOut);
                        dOut->cd();
                        shift->GetXaxis()->SetTitle("gen");
                        shift->Write("unfold");
                        /// \todo cov? (needed for smoothing)
                    }

                    // lower shift
                    {
                        auto shift = unique_ptr<TH1>(density->GetDeltaSysBackgroundScale(
                                    name, // name of the source given in `SubtractBackground()`
                                    name + SysDown, // name of new histogram
                                    "")); /// \todo title of new histogram
                        shift->Scale(-1);
                        shift->Add(unf.get());
                        auto dOut = fOut->mkdir(name + SysDown, "lower shift of " + title);
                        shift->SetDirectory(dOut);
                        dOut->cd();
                        shift->GetXaxis()->SetTitle("gen");
                        shift->Write("unfold");
                        /// \todo cov? (needed for smoothing)
                    }
                }
            }

            cout << __LINE__ << "\tSaving estimates of miss counts" << endl;
            for (auto& m: miss) {
                TString name = "miss" + m.first,
                        title = m.second->GetTitle();
                cout << __LINE__ << '\t' << name << endl;

                auto M = Clone<TH1>(m.second, name);

                // count
                dOut->cd();
                for (int i = 1; i <= unf->GetNbinsX(); ++i) {
                    double u = unf->GetBinContent(i),
                           content = m.second->GetBinContent(i) *       u ,
                           error   = m.second->GetBinError  (i) * ::abs(u);
                    M->SetBinContent(i, content);
                    M->SetBinError  (i, error  );
                }
                M->SetDirectory(dOut);
                M->Write();
                M->SetDirectory(nullptr);

                if (var == "nominal" && applySyst) {
                    cout << __LINE__ << "\tSaving shifts from inefficiency variations "
                                        "in parallel directories" << endl;
                    fOut->cd();

                    auto missNormVar = config.get_optional<float>("unfolding.missNormVar").value_or(0.05);
                    cout << __LINE__ << "\tmissNormVar = " << missNormVar << endl;

                    // upper shifts
                    {
                        auto shift = Clone<TH1>(M, name + SysUp);
                        shift->Scale(missNormVar);
                        shift->Add(unf.get());
                        auto dOut = fOut->mkdir(name + SysUp, "lower shift of " + title);
                        shift->SetDirectory(dOut);
                        dOut->cd();
                        shift->GetXaxis()->SetTitle("gen");
                        shift->Write("unfold");
                        /// \todo cov? (needed for smoothing)
                    }

                    // lower shifts
                    {
                        auto shift = Clone<TH1>(M, name + SysDown);
                        shift->Scale(-1);
                        shift->Scale(missNormVar);
                        shift->Add(unf.get());
                        auto dOut = fOut->mkdir(name + SysDown, "lower shift of " + title);
                        shift->SetDirectory(dOut);
                        dOut->cd();
                        shift->GetXaxis()->SetTitle("gen");
                        shift->Write("unfold");
                        /// \todo cov? (needed for smoothing)
                    }
                }
            }

            // basically, everything has now been retrieved
            // -> now we just perform some last test

            cout << __LINE__ << "\tBottom line test" << endl;
            {
                int rebin = recMC->GetNbinsX()/genMC->GetNbinsX();

                cout << __LINE__ << "\tDetector level (`rebin` = " << rebin << "): ";
                Checks::BLT(recData, covInData, recMC, rebin);

                cout << __LINE__ << "\tHadron level (with data unc. only): ";
                Checks::BLT(unf, covOutData, genMC);

                cout << __LINE__ << "\tHadron level (with data + MC unc.): ";
                Checks::BLT(unf, covOutTotal, genMC);
            }
        } // end of data variations
    } // end of MC variations

    cout << __func__ << ' ' << slice << " stop" << endl;
} // end of `unfold`

} // end of DAS::Unfolding namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();
        DT::MetaInfo::versions["TUnfold"] = TUnfold::GetTUnfoldVersion();

        fs::path inputData, inputMC, output;

        auto options = DAS::Options(
                            "Run the unfolding with the TUnfold package. The default approach "
                            "is to run least-square minimisation or pure matrix inversion. "
                            "Unlike all other executables, the options (including those for the "
                            "Tikhonov regularisation, if one wishes to try it) may only be "
                            "provided via a configuration file (note: unlike all other commands, "
                            "the `-e` option does not work here). In case no config file "
                            "is provided, the code will run with the default settings. "
                            "Finally, the parallelisation works slightly differently: instead "
                            "of splitting by ranges of events (since we have no events), "
                            "the running is split by the variations, which may be necessary for "
                            "the unfolding of distributions with a large number of bins (> 100).",
                            DT::config | DT::syst | DT::split);
        options.input("inputData", &inputData, "input Data ROOT file (from `getUnfHist*`)")
               .input("inputMC"  , &inputMC  , "input MC ROOT file (from `getUnfHist*`)")
               .output("output", &output, "output ROOT file with unfolded cross section ");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Unfolding::unfold(inputData, inputMC, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
