# Das Analysis System

[![pipeline status](https://gitlab.cern.ch/paconnor/software/badges/master/pipeline.svg)](https://gitlab.cern.ch/paconnor/software/-/commits/main)
[![coverage report](https://gitlab.cern.ch/paconnor/software/badges/master/coverage.svg)](https://gitlab.cern.ch/paconnor/software/-/commits/main)

The software is developed on top of Darwin.
It allows to conduct analyses from official CMS data sets to the results ready for publication.

The software is made of two parts:
* CMSSW-based tools for the production of *n*-tuples.
* Modules to apply physics selections and corrections specific to CMS, modifying *n*-tuples step by step until distributions can be obtained and unfolded.

It encompasses the following steps:
 1. *n*-tuplisation
 2. Basic treatement of data and MC (the list of correction is not exhaustive):
   1. for data, trigger, and ormalisation to the luminosity,
   2. for MC, normalisation to the cross section, corrections of pile-up simulation, corrections to the jet energy
 3. Unfolding with systematics

## Description of the subpackages

In alphabetical order:
 - [CommonTools](CommonTools/README.md): some common tools for development and running (no physics in this directory)
 - [JEC](JEC/README.md): apply JES corrections and JER smearing corrections
 - [Muons](Muons/README.md): select muons and apply scale factors
 - [Normalisation](Normalisation/README.md): normalisation of data and simulation after merging
 - [Ntupliser](Ntupliser/README.md): code for *n*-tuplisation with CRAB jobs
 - [Objects](Objects/README.md): code for the objects in the *n*-tuples
 - [Photons](Photons/README.md): select photons and apply scale factors
 - [Prefiring](Prefiring/README.md): code to apply the prefiring correction in data
 - [PUprofile](PUprofile/README.md): tools to perform the pile-up profile reweighting in simulation
 - [PUstaubSauger](PUstaubSauger/README.md): tool to clean up the simulation from the badly sampled simulation
 - [Trigger](Trigger/README.md): trigger to determine the trigger turn-on points for 99% of efficiency

## Generic source of information

 - [JetMET](https://twiki.cern.ch/twiki/bin/view/CMS/JetMET)
 - [BTV](https://twiki.cern.ch/twiki/bin/viewauth/CMS/BtagRecommendation)
