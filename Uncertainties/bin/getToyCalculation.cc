#include <cstdlib>
#include <iostream>
#include <vector>
#include <numeric>
#include <limits>
#include <utility>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/Uncertainties/interface/Teddy.h"

#include "Core/Unfolding/interface/Observable.h"
#include "Core/Unfolding/interface/toolbox.h"

#include <TString.h>
#include <TFile.h>
#include <TH2D.h>
#include <TKey.h>

#include <darwin.h>

using namespace std;
using namespace Eigen;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

static const auto deps = numeric_limits<double>::epsilon();

namespace DAS::Unfolding {

template<typename TH1X, typename TH2X>
void CheckSymmetry (const unique_ptr<TH1X>& h, const unique_ptr<TH2X>& cov)
{
    cout << __LINE__ << "\tChecking that cov matrix is symmetric" << endl;
    for (int i = 1; i <= h->GetNbinsX(); ++i)
    for (int j = 1; j <= h->GetNbinsX(); ++j) {
        double normalisation = h->GetBinContent(i) * h->GetBinContent(j);
        if (std::abs(normalisation) < deps) continue;
        double a_ij = cov->GetBinContent(i,j) / normalisation,
               a_ji = cov->GetBinContent(j,i) / normalisation;

        if (a_ji > 0 && std::abs(a_ij / a_ji - 1) > 1e-6)
            cerr << __LINE__ << orange << "\tWarning: asymmetry found for i = "
                 << i << ", j = " << j << ", getting a_ij = " << a_ij
                 << " and a_ji = " << a_ji << '\n' << def;
    }
}

template<typename TH1X, typename TH2X>
void CheckEntries (const unique_ptr<TH1X>& h, const unique_ptr<TH2X>& cov)
{
    cout << __LINE__ << "\tLooking for abnormal entries" << endl;
    for (int i = 1; i <= h->GetNbinsX(); ++i) {
        double content = h->GetBinContent(i);
        if (isfinite(content)) continue;
        cerr << __LINE__ << orange << "\tWarning: bin " << i
             << " has abnormal content: " << content << '\n' << def;
        h->SetBinContent(i, 0);
        h->SetBinError  (i, 0);
        for (int j = 0; j <= h->GetNbinsX(); ++j) {
            cov->SetBinContent(i,j,0);
            cov->SetBinContent(j,i,0);
        }
    }
}


////////////////////////////////////////////////////////////////////////////////
/// Use a toy calculation to apply some operations to the input observables.
///
/// The output file keeps (as much as possible) the same structure as the input.
void getToyCalculation
        (const fs::path& input, //!< input ROOT file
         const fs::path& output, //!< output ROOT file
         const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    Teddy::verbose = (steering & DT::verbose) == DT::verbose;

    auto fIn = make_unique<TFile>(input.c_str(), "READ");

    auto fOut = DT::GetOutputFile(output);

    auto pt_obs = config.get_child("unfolding.observables");
    cout << "Setting observables" << endl;
    vector<Observable *> observables = GetObservables(pt_obs);

    const TString hName = config.get<string>("uncertainties.toy.distribution"),
                covName = config.get<string>("uncertainties.toy.covariance"),
                  level = config.get<string>("uncertainties.toy.level");

    const bool genLevel = level.Contains("gen");
    if (!genLevel && !level.Contains("rec"))
        BOOST_THROW_EXCEPTION( invalid_argument("Level was unrecognized") );

    TUnfoldBinning * pre = new TUnfoldBinning("pre");
    TUnfoldBinning * post = new TUnfoldBinning("post");
    vector<unique_ptr<Transformer>> transformers;
    for (const Observable * obs: observables) {
        // pre
        auto preBinning = genLevel ? obs->genBinning : obs->recBinning;
        pre->AddBinning(preBinning);
        // post
        unique_ptr<Transformer> t = obs->getTransformer(preBinning);
        TUnfoldBinning * postSubBinning = t->postBinning;
        post->AddBinning(postSubBinning);
        transformers.push_back(std::move(t));
    }

    auto N = config.get<int>("uncertainties.toy.N");

    fIn->cd();
    // loop over the entries of a TFile / TDirectory
    for (const auto&& obj: *(fIn->GetListOfKeys())) {

        auto key = dynamic_cast<TKey*>(obj);
        const TString classname = key->ReadObj()->ClassName();
        cout << __LINE__ << '\t' << key->GetName() << endl;
        if (!classname.Contains("TDirectory")) continue;
        auto dirIn = dynamic_cast<TDirectory*>(key->ReadObj());
        dirIn->cd();

        auto   hIn = unique_ptr<TH1D>(dirIn->Get<TH1D>(  hName));
        auto covIn = unique_ptr<TH2D>(dirIn->Get<TH2D>(covName));
        if (!hIn)
            BOOST_THROW_EXCEPTION( DE::BadInput("Can't find" + hName, *dirIn) );
        if (!covIn)
            BOOST_THROW_EXCEPTION( DE::BadInput("Can't find" + covName, *dirIn) );

        cout << __LINE__ << "\t`hIn` has " << hIn->GetNbinsX() << " bins\n"
             << __LINE__ << "\t`covIn` has " << covIn->GetNbinsX()
                                      << 'x' << covIn->GetNbinsY() << " bins" << endl;

        {
            static int ivar = 0; // used for parallelisation
            ++ivar;
            if (slice.second != ivar % slice.first) continue;
        }

        cout << __LINE__ << "\tRemove bad input bins" << endl;
        for (auto& t: transformers)
            t->RemoveBadInputBins(hIn.get(), covIn.get());

        cout << __LINE__ << "\tChecking input" << endl;
        CheckSymmetry(hIn, covIn);
        CheckEntries(hIn, covIn);

        int nBinsOut = 0;
        for (auto node = post->GetChildNode(); node != nullptr;
                  node = node->GetNextNode()) {
            int nBinsHere = node->GetDistributionNumberOfBins();
            cout << __LINE__ << '\t' << node->GetName() << " has " << nBinsHere << " bins\n";
            nBinsOut += nBinsHere;
        }
        cout << __LINE__ << "\tnBinsOut = " << nBinsOut << endl;
        function<VectorXd(const VectorXd &)> transformation =
            [nBinsOut,&transformers](const VectorXd& x) {
                Transformer::y = VectorXd::Zero(nBinsOut);
                for (const auto& t: transformers)
                    t->Transform(x);
                return Transformer::y;
            };

        cout << __LINE__ << "\tDeclaring toy" << endl;
        auto seed = 42; /// \todo once the meta info is available, use `metainfo.Seed<67325879>(slice)`
        Teddy toy(hIn, covIn, transformation, seed);

        cout << __LINE__ << "\tPlaying" << endl;
        for (long i = 0; i < N; ++i)
            toy.play();

        cout << __LINE__ << "\tGetting output" << endl;
        auto [hOut, covOut] = toy.buy("trans", nBinsOut, 0.5, nBinsOut + 0.5);
        hOut->SetTitle(hIn->GetTitle());
        covOut->SetTitle(covIn->GetTitle());

        cout << __LINE__ << "\tChecking output" << endl;
        CheckSymmetry(hOut, covOut);
        CheckEntries(hOut, covOut);

        fOut->cd();

        cout << __LINE__ << "\tWriting output" << endl;
        auto dirOut = fOut->mkdir(dirIn->GetName());
        dirOut->cd();
        hOut->SetDirectory(dirOut);
        hOut->Write(hName);
        covOut->SetDirectory(dirOut);
        covOut->Write(covName);
        TString corrName = covName;
        corrName.ReplaceAll("cov", "corr");
        corrName.ReplaceAll("trans_", "");
        SaveCorr<TH2D>(covOut, corrName, dirOut);
    }

    cout << __LINE__ << "\tDone" << endl;
}

} // end of DAS::Uncertainties namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options(
                            "Fill histograms to run global unfolding of "
                            "multiple observables.", DT::syst | DT::split | DT::config);

        options.input("input", &input, "input ROOT file")
               .output("output", &output, "output ROOT file")
               .arg<string>("level", "uncertainties.toy.level", "gen or rec")
               .arg<string>("distribution", "uncertainties.toy.distribution", "distribution (e.g. unfold)")
               .arg<string>("covariance", "uncertainties.toy.covariance", "covariance matrix (e.g. covOutData)")
               .arg<long>("N", "uncertainties.toy.N", "number of events")
               .args("observables", "unfolding.observables", "list of observables (use DAS::Unfolding subnamespaces)");

        const auto& config = options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Unfolding::getToyCalculation(input, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }
}
#endif
