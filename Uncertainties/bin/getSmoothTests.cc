#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <TROOT.h>
#include <TString.h>
#include <TFile.h>
#include <TH1.h>
#include <TKey.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/Step.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Find range for non-empty bins in an input histogram.
pair<int,int> GetBinRange (TH1 * h)
{
    int N = h->GetNbinsX();
    int im, iM;
    for (im =  1; h->GetBinContent(im) <= 0 && im <= N; ++im);
    for (iM = im; h->GetBinContent(iM) >  0 && iM <= N; ++iM);
    --iM;
    return {im,iM};
}

////////////////////////////////////////////////////////////////////////////////
/// Divide the histogram by bin width and adapt the covariance matrix
/// accordingly.
void ScaleWidth (TH1 * h, TH2 * cov)
{
    for (int i = 1; i <= h->GetNbinsX(); ++i) {
        double width = h->GetBinWidth(i);
        double content = h->GetBinContent(i);
        h->SetBinContent(i, content/width);
        for (int j = 1; j <= h->GetNbinsX(); ++j) {
            double width2 = h->GetBinWidth(j);
            double content = cov->GetBinContent(i,j);
            cov->SetBinContent(i, j, content/width/width2);
        }
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Smooth relative uncertainties on the output of `getUnfPhysDist`.
void getSmoothFits
           (const fs::path& input, //!< input ROOT files (histograms)
            const fs::path& output, //!< output ROOT file (histograms)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << " start" << endl;

    auto fIn = make_unique<TFile>(input.c_str(), "READ");

    auto fOut = DT::GetOutputFile(output);

    const TString nDist = config.get<string>("uncertainties.smoothness.distribution"),
                  nCov  = config.get<string>("uncertainties.smoothness.covariance");
    cout << nDist << ' ' << nCov << endl;

    Step::verbose = steering & DT::verbose;
    for (const auto& var: GetObjects<TDirectory>(fIn.get())) {
        // parallelisation (option `-j`)
        {
            static int ivar = 0; // used for parallelisation
            ++ivar;
            if (slice.second != ivar % slice.first) continue;
        }

        TString varname = var->GetName();
        if (varname.Contains("fake")) continue; // TODO: calculate cov matrix
        if (varname.Contains("miss")) continue; // TODO: calculate cov matrix

        // looping over all available observables
        for (const auto& obs: GetObjects<TDirectory>(var->GetDirectory(nDist))) {
            TString obsname = obs->GetName();
            cout << varname << ' ' << obsname << endl;

            [[ maybe_unused ]] // trick to only activate `cout` in the loop if option `-v` has been given
            static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

            var->cd();
            TDirectory * dInDist = GetDirectory(var, {nDist, obsname}),
                       * dInCov  = GetDirectory(var, {nCov, obsname, obsname}),
                       * dOutDist = fOut->mkdir(var->GetName(), "", true)
                                        ->mkdir(nDist, "", true)
                                        ->mkdir(obsname);
            for (const auto& h: GetObjects<TH1>(dInDist)) {
                if (h->Integral() <= 0) {
                    delete h;
                    continue;
                }

                auto [im, iM] = GetBinRange(h);
                if (iM - im <= 2) {
                    delete h;
                    continue;
                }

                TString n = h->GetName(),
                        covName = n + "__" + n;

                auto cov = dInCov->Get<TH2>(covName);
                if (!cov) {
                    h->Print("all");
                    BOOST_THROW_EXCEPTION( DE::BadInput("Covariance matrix "s + covName
                                    + " could not be found for "s + varname, *dInCov) );
                }

                ScaleWidth(h, cov);

                using namespace Step;
                auto f = GetSmoothFit<Basis::Chebyshev,log,exp>(h, cov, im, iM, 10,
                                                EarlyStopping::Chi2ndf, 2, 0, cout);
                // NOTE: degree 10 and 2 sigmas are inspired from personal experience with inclusive jet
                //       --> we may want to make options from the command line in the future
                dOutDist->cd();
                for (int i = im; i <= iM; ++i) {
                    double err2 = cov->GetBinContent(i,i);
                    h->SetBinError(i, sqrt(err2));
                }
                h->SetDirectory(dOutDist);
                h->Write();
                f->Write(Form("fit_%s", h->GetName()));

                delete h;
                delete cov;
                delete f;
            }
        }
    }

    cout << __func__ << " stop" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        DT::Options options("Smooth relative uncertainties in the output of `getUnfPhysDist`",
                            DT::split | DT::config);
        options.input ("input" , &input , "input ROOT file")
               .output("output", &output, "output ROOT file")
               .arg<string>("distribution", "uncertainties.smoothness.distribution", "distribution (e.g. `unfold`)")
               .arg<string>("covariance", "uncertainties.smoothness.covariance", "covariance matrix (e.g. `covOutTotal`)")
               (argc, argv);
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::getSmoothFits(input, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
