#include <cstdlib>
#include <iostream>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TChain.h>
#include <TDirectory.h>
#include <TH2.h>
#include <TProfile.h>

#include "Math/VectorUtil.h"

#include "Core/MET/interface/Filters.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::MissingET {

struct FractionCut {

    const TString name;

    // from MET
    std::unique_ptr<TH1> Et, SumEt, Fraction, Pt, Phi;

    std::unique_ptr<TH2> pt_y;

    FractionCut (TString Name) : //!< Constructor, initialises all histograms
        name    (Name),
        Et      (std::make_unique<TH1F>(Name + "Et", "Et", 100, 1, 1000)),
        SumEt   (std::make_unique<TH1F>(Name + "SumEt", "SumEt", 650, 1, 6500)),
        Fraction(std::make_unique<TH1F>(Name + "Fraction", "Fraction", 100, 0, 1)),
        Pt      (std::make_unique<TH1F>(Name + "Pt", "Pt", 100, 1, 1000)),
        Phi     (std::make_unique<TH1F>(Name + "Phi", "Phi", 314, -M_PI, M_PI)),
        pt_y    (std::make_unique<TH2F>(Name + "pt_y", "pt_y", nPtBins, pt_edges.data(), nYbins, y_edges.data()))
    {
        cout << __func__ << ' ' << name << endl;
    }

    void Fill //!< Fill all histograms directly
        (MET * met,  //!< MET info
         std::vector<RecJet> * recJets, //!< rec jets
         double w)
    {
        // from MET
        Et->Fill(met->Et, w);
        SumEt->Fill(met->SumEt, w);
        Fraction->Fill(met->Et/met->SumEt, w);
        Pt->Fill(met->Pt, w);
        Phi->Fill(met->Phi, w);

        // jet
        for (const RecJet& jet: *recJets)  {
            double jW = jet.weights.front();
            pt_y->Fill(jet.CorrPt(), jet.AbsRap(), w*jW);
        }
    }

    void Write (TDirectory * d)
    {
        cout << "Writing MET plots for " << name << endl;
        d->cd();
        TDirectory * dd = d->mkdir(name);
        dd->cd();
        for (TH1 * h: {Et.get(), SumEt.get(), Fraction.get(), Pt.get(), Phi.get()}) {
            h->SetDirectory(dd);
            TString n = h->GetName();
            n.ReplaceAll(name,"");
            h->Write(n);
        }
        pt_y->SetDirectory(dd);
        TString n = pt_y->GetName();
        n.ReplaceAll(name,"");
        pt_y->Write(n);
        d->cd();
    }
};

////////////////////////////////////////////////////////////////////////////////
/// Apply MET filters, both for MC and Data
/// following the official recommendations
void applyMETfilters
              (const vector<fs::path>& inputs, //!< input ROOT file (n-tuple)
               const fs::path& output, //!< output ROOT file (n-tuple)
               const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
               const int steering, //!< parameters obtained from explicit options
               const DT::Slice slice = {1,0} //!< number and index of slice
               )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto year = metainfo.Get<int>("flags", "year");

    auto gEv = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr;
    auto rEv = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto met = flow.GetBranchReadOnly<MET     >("met"     );
    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");

    if (!metainfo.Find("corrections", "METfilters"))
        BOOST_THROW_EXCEPTION( DE::BadInput("No MET filters in this tree", tOut) );

    fs::path p_METfilters = config.get<fs::path>("corrections.METfilters");
    if (metainfo.Get<fs::path>("corrections", "METfilters") != p_METfilters) {
        cerr << orange << "You are now using a different list of MET filters w.r.t. n-tuplisation.\n" << def;
        metainfo.Set<bool>("git", "reproducible", false);
    }
    MissingET::Filters metfilters(p_METfilters);

    /**** declaring a few histograms to control effect of filters ****/

    MissingET::FractionCut beforeCut("beforeMETcut"),
                           afterCut("afterMETcut");

    ControlPlots METbefore("METbefore"),
                 METafterAllMETfilters("METafterAllMETfilters"),
                 METafterMETfraction("METafterMETfraction"),
                 METafterAllMETfiltersAndMETfraction("METafterAllMETfiltersAndMETfraction");

    vector<ControlPlots> METfilters;
    for (const string& METname: metfilters.names)
        METfilters.emplace_back(METname);

    auto totRecWgt = [&](size_t i) {
        return (isMC ? gEv->weights.front().v : 1) * rEv->weights.at(i).v;
    };

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        beforeCut.Fill(met, recJets, totRecWgt(0));
        METbefore(*recJets, totRecWgt(0));

        if (met->Et < 0.3 * met->SumEt){
            afterCut.Fill(met, recJets, totRecWgt(0));
            METafterMETfraction(*recJets, totRecWgt(0));
        }

        /// \note In case the MET fraction based cut would be used,
        ///       the description of the MET class should be changed!!

        metfilters(*met, *rEv);

        METafterAllMETfilters(*recJets, totRecWgt(0));

        if (met->Et < 0.3 * met->SumEt)
            METafterAllMETfiltersAndMETfraction(*recJets, totRecWgt(0));

        if (steering & DT::fill) tOut->Fill();
    }

    cout << "Saving MET variables before and after the MET fraction cut" << endl;
    auto dMETcut = fOut->mkdir("METfraction");
    beforeCut.Write(dMETcut);
    afterCut.Write(dMETcut);

    cout << "Saving control observables before and after the MET fraction cut and/or MET filters" << endl;
    auto dCP = fOut->mkdir("controlplots");
    METbefore.Write(dCP);
    METafterMETfraction.Write(dCP);
    METafterAllMETfilters.Write(dCP);
    METafterAllMETfiltersAndMETfraction.Write(dCP);

    cout << "Saving control observables for each MET filter individually" << endl;
    auto dMETfilters = fOut->mkdir("METfilters");
    for (auto& filt: METfilters)
        filt.Write(dMETfilters);

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::MissingET namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Applies the MET filters by removing the rec-level jets.",
                            DT::split | DT::Friend);

        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file"               )
               .arg<fs::path>("METfilters", "corrections.METfilters", "2-column file containing the MET filters");

        const auto& config = options(argc,argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::MissingET::applyMETfilters(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
