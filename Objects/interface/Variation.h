#pragma once

#include <iostream>
#include <memory>

#include <TString.h>

#include "Math/Vector4D.h"

#include "Core/Objects/interface/Weight.h"

namespace DAS {

typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<float>> FourVector;

namespace Uncertainties {

////////////////////////////////////////////////////////////////////////////////
/// Generic structure to hold the relevant information to identify the variation
/// indices. This structure may be passed over to physics (simple or composite)
/// objects to vary (or not) the kinematics or the weight respectively.
struct Variation {

    const TString group; //!< e.g. event, recjet
    const TString name; //!< variation name (including "Up" or "Down")

    const std::size_t index; //!< index in the vector where the variation factor is stored
    const int bit; //!< correlation bit

    ////////////////////////////////////////////////////////////////////////////////
    /// Destructor
    ~Variation () = default;

    ////////////////////////////////////////////////////////////////////////////////
    /// Normal constructor
    Variation (const TString& group, const TString& name, size_t index = 0, int bit = 0) :
        group(group), name(name), index(index), bit(bit)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Copy constructor
    Variation (const Variation& v) :
        group(v.group), name(v.name), index(v.index), bit(v.bit)
    { }

    ////////////////////////////////////////////////////////////////////////////////
    /// Move constructor
    Variation (Variation&& v) :
        group(std::move(v.group)), name(std::move(v.name)),
        index(std::move(v.index)), bit(std::move(v.bit))
    { }

};

const Variation nominal {"", "nominal"};

inline std::ostream& operator<< (std::ostream& s, const DAS::Uncertainties::Variation& v)
{
    return s << v.group << ' ' << v.name << ' ' << v.index << ' ' << v.bit;
}

} // end of namespace Uncertainties

} // end of namespace DAS
