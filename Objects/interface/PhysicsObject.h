#pragma once

#include <cmath>
#include <vector>
#include <iostream>
#include <string_view>

#include "Math/Vector4D.h"

#include "Core/Objects/interface/Variation.h"
#include "Core/Objects/interface/Weight.h"

namespace DAS {

typedef ROOT::Math::LorentzVector<ROOT::Math::PtEtaPhiM4D<float>> FourVector;

////////////////////////////////////////////////////////////////////////////////
/// Generic physics entity, either an object directly reconstructed in the
/// detector, or a composite system made of such objects. The existence of such
/// a class is motivated by the need to define a way to access variations in a
/// common way for simple and composite objects with `DAS::Uncertainties::Variation`.
struct AbstractPhysicsObject {

    AbstractPhysicsObject () = default;
    virtual ~AbstractPhysicsObject () = default;

    virtual FourVector CorrP4 (const Uncertainties::Variation&) const = 0;
    virtual float      CorrPt (const Uncertainties::Variation&) const = 0;
    virtual double     Weight (const Uncertainties::Variation&) const = 0;

    virtual float      Rapidity (const Uncertainties::Variation&) const = 0;
    virtual float      AbsRap   (const Uncertainties::Variation&) const = 0;
};

////////////////////////////////////////////////////////////////////////////////
/// Generic physics object, such as a jet or a stable particle.
///
/// The four-momentum p4 does not include the energy calibration.
/// The `scales` vector typically contains the JES or Rochester corrections.
/// The `weights` include a value and a (de)correlation bit.
class PhysicsObject : public AbstractPhysicsObject {
protected:
    virtual std::string_view scale_group  () const = 0;
    virtual std::string_view weight_group () const = 0;

    PhysicsObject () = default;
    virtual ~PhysicsObject () = default;

public:
    FourVector p4; //!< raw four-momentum directly after reconstruction
    std::vector<float> scales = {1.}; //!< energy scale corrections and variations
    Weights weights = {{1.,0}}; //!< object weights

    inline FourVector CorrP4 (size_t i = 0) const { return p4      * scales.at(i); } //!< corrected 4-vector
    inline float      CorrPt (size_t i = 0) const { return p4.Pt() * scales.at(i); } //!< corrected transverse momentum
    inline float Rapidity (const Uncertainties::Variation& = Uncertainties::nominal) const final
                { return       p4.Rapidity() ; } //!< rapidity
    inline float AbsRap   (const Uncertainties::Variation& = Uncertainties::nominal) const final
                { return std::abs(Rapidity()); } //!< absolute rapidity

    inline FourVector CorrP4 (const Uncertainties::Variation& v) const final //!< corrected 4-vector
    { return PhysicsObject::CorrP4(v.group == scale_group() ? v.index : 0); }

    inline float CorrPt (const Uncertainties::Variation& v) const final //!< corrected transverse momentum
    { return PhysicsObject::CorrPt(v.group == scale_group() ? v.index : 0); }

    inline double Weight (const Uncertainties::Variation& v) const final //!< weight
    {
        if (v.group == weight_group()) {
            const DAS::Weight& w = weights.at(v.index);
            if (w.i == v.bit) return w;
        }
        return weights.front();
    }
};

inline bool operator== (const PhysicsObject& l, const PhysicsObject& r)
{
    return l.p4 == r.p4 && l.scales == r.scales && l.weights == r.weights;
}

inline bool operator< (const PhysicsObject& l, const PhysicsObject& r)
{
    return l.CorrPt() < r.CorrPt();
}

inline bool operator> (const PhysicsObject& l, const PhysicsObject& r)
{
    return l.CorrPt() > r.CorrPt();
}

} // end of DAS namespace

inline std::ostream& operator<< (std::ostream& s, const DAS::FourVector& p4)
{
    return s << '(' << p4.Pt() << ',' << p4.Eta() << ',' << p4.Phi() << ',' << p4.M() << ')';
}

inline std::ostream& operator<< (std::ostream& s, const DAS::PhysicsObject& obj)
{
    return s << obj.p4 << ' ' << obj.scales.size() << ' ' << obj.weights.size();
}

#if defined(__ROOTCLING__)
#pragma link C++ class DAS::AbstractPhysicsObject +;
#pragma link C++ class DAS::PhysicsObject +;
#pragma link C++ class std::vector<DAS::FourVector> +;
#endif
