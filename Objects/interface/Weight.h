#pragma once

#include <vector>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Mimics a `pair<double,int>`, where the double contains a weight (either gen
/// or rec level, for events, jets, photons, etc.), and the int an index.
/// For bin-to-bin correlated (uncorrelated) uncertainties, it should always be
/// 0 (correspond to the index of the bin containing the corrections). This
/// allows to handle the decorrelations coming from independent stat in different
/// bins.
///
/// A few operators are overloaded so that the weight may be used just as a double.
struct Weight {
    double v = 1; //!< value
    int i = 0; //!< correlation index
    operator double () const { return v; }
    Weight& operator= (const double v) { this->v = v; return *this; }
};

inline bool operator== (const Weight& w, const int v) { return w.v == v; }
inline bool operator== (const Weight& w, const float v) { return w.v == v; }
inline bool operator== (const Weight& w, const double v) { return w.v == v; }
inline bool operator== (const Weight& l, const Weight& r) { return l.v == r.v && l.i == r.i; }
inline double operator* (const Weight& w, const int v) { return w.v * v; }
inline double operator* (const int v, const Weight& w) { return w.v * v; }
inline double operator* (const Weight& w, const float v) { return w.v * v; }
inline double operator* (const float v, const Weight& w) { return w.v * v; }
inline double operator* (const Weight& w, const double v) { return w.v * v; }
inline double operator* (const double v, const Weight& w) { return w.v * v; }
inline Weight& operator*= (Weight& w, const int v) { w.v *= v; return w; }
inline Weight& operator/= (Weight& w, const int v) { w.v /= v; return w; }
inline Weight& operator*= (Weight& w, const float v) { w.v *= v; return w; }
inline Weight& operator/= (Weight& w, const float v) { w.v /= v; return w; }
inline Weight& operator*= (Weight& w, const double v) { w.v *= v; return w; }
inline Weight& operator/= (Weight& w, const double v) { w.v /= v; return w; }
inline double operator* (const Weight& w1, const Weight& w2) { return w1.v * w2.v; }

typedef std::vector<Weight> Weights;

inline Weights& operator*= (Weights& wgts, const int v) { for (auto& w: wgts) w *= v; return wgts; }
inline Weights& operator/= (Weights& wgts, const int v) { for (auto& w: wgts) w /= v; return wgts; }
inline Weights& operator*= (Weights& wgts, const float v) { for (auto& w: wgts) w *= v; return wgts; }
inline Weights& operator/= (Weights& wgts, const float v) { for (auto& w: wgts) w /= v; return wgts; }
inline Weights& operator*= (Weights& wgts, const double v) { for (auto& w: wgts) w *= v; return wgts; }
inline Weights& operator/= (Weights& wgts, const double v) { for (auto& w: wgts) w /= v; return wgts; }

} // end of DAS namespace

#ifdef __ROOTCLING__
#pragma link C++ class DAS::Weight +;
#endif // __ROOTCLING__

