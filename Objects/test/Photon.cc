#ifndef DOXYGEN_SHOULD_SKIP_THIS
#include "Core/Objects/interface/Photon.h"

#define BOOST_TEST_MODULE Photon
#include <boost/test/included/unit_test.hpp>

#include <iostream>

using namespace DAS;
using namespace std;

BOOST_AUTO_TEST_CASE( genlevel )
{
    GenPhoton photon;
    BOOST_TEST( photon.scales.size() == 1 );
    BOOST_TEST( photon.weights.size() == 1 );
    cout << photon << endl;
}

BOOST_AUTO_TEST_CASE( reclevel )
{
    RecPhoton photon;
    BOOST_TEST( photon.weights.size() == 1 );
    BOOST_TEST( photon.selectors == 0 );
    BOOST_TEST( photon.ecalEnergyErrPostCorr == -1 );
    cout << photon << endl;
}

#endif
