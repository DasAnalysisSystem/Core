#ifdef __ROOTCLING__
#include "Core/Objects/interface/Weight.h"
#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Di.h"
#endif // __ROOTCLING__
