#include <cstdlib>

#include <filesystem>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"

#include <TString.h>
#include <TFile.h>
#include <TH2.h>

#include "Core/CommonTools/interface/ControlPlots.h"

#include "PUcorrection.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::PUprofile {

////////////////////////////////////////////////////////////////////////////////
/// Apply the PU profile reweighting to the simulation.
void applyPUprofCorrection
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto pu     = flow.GetBranchReadOnly<PileUp  >("pileup"  );
    auto genJets = flow.GetBranchReadWrite<vector<GenJet>>("genJets");
    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    ControlPlots::isMC = isMC;
    ControlPlots raw("raw");
    vector<ControlPlots> calib { ControlPlots("nominal") };
    if (steering & DT::syst) {
        metainfo.Set<string>("variations", RecEvent::WeightVar, "PU" + SysUp);
        metainfo.Set<string>("variations", RecEvent::WeightVar, "PU" + SysDown);

        calib.push_back(ControlPlots("PU" + SysUp));
        calib.push_back(ControlPlots("PU" + SysDown));
    }

    TH2 * recptLogWgt = new TH2D("recptLogWgt", "recptLogWgt;Jet p_{T}^{rec} (GeV);log(w)", nPtBins, pt_edges.data(), 400, -30, 20);
    TH2 * recptPUwoWeights = new TH2D("recptPUwoWeights", "recptPUwoWeights;Jet p_{T}^{rec} (GeV);intPU", nPtBins, pt_edges.data(), 121, -0.5, 120.5);
    TH2 * recptPUwWeights = new TH2D("recptPUwWeights", "recptPUwWeights;Jet p_{T}^{rec} (GeV);intPU", nPtBins, pt_edges.data(), 121, -0.5, 120.5);

    auto DataProf = config.get<fs::path>("corrections.PUprofile.Data");
    auto MCprof = config.get<fs::path>("corrections.PUprofile.MC");
    for (auto& file: {DataProf, MCprof})
        if (!fs::exists(file))
            BOOST_THROW_EXCEPTION( fs::filesystem_error("Input file could not be found",
                                file, make_error_code(errc::no_such_file_or_directory)));
    auto fMCprof = TFile::Open(MCprof.c_str(), "READ"),
         fDataProf = TFile::Open(DataProf.c_str(), "READ");
    auto global = [](const char * variation, bool) { return Form("%s/intPU", variation); };
    auto maxWeight = config.get<float>("corrections.PUprofile.maxWeight");
    PUprofile::Correction correction(fMCprof, fDataProf, global, maxWeight);
    fDataProf->Close();
    fMCprof->Close();

    metainfo.Set<fs::path>("corrections", "PUprofile", "Data", DataProf);
    metainfo.Set<fs::path>("corrections", "PUprofile", "MC", MCprof);
    metainfo.Set<float>("corrections", "PUprofile", "maxWeight", maxWeight);

    PileUp::r3.SetSeed(metainfo.Seed<756347956>(slice));
    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        int intpu = pu->intpu;
        auto weight_raw = recEvt->weights.front(); // get current nominal weight before correction

        // loop over existing weights and correct them with the nominal value
        auto nominal_correction = correction(intpu, '0');
        recEvt->weights *= nominal_correction;

        for (auto& recjet: *recJets) {
            recptPUwoWeights->Fill(recjet.p4.Pt(), intpu); /// \todo Use raw pt??
            recptPUwWeights->Fill(recjet.p4.Pt(), intpu, nominal_correction);
        }

        for (const auto& recJet: *recJets)
            recptLogWgt->Fill(recJet.CorrPt(), log(nominal_correction));

        auto weight_nominal = recEvt->weights.front();

        // filling at gen level
        raw          (*genJets, genEvt->weights.front());
        calib.front()(*genJets, genEvt->weights.front());

        // filling at rec level
        raw          (*recJets, genEvt->weights.front() * weight_raw    );
        calib.front()(*recJets, genEvt->weights.front() * weight_nominal);

        // systematics
        if (steering & DT::syst) {
            auto weight_upper   = weight_raw*correction(intpu, '+'),
                 weight_lower   = weight_raw*correction(intpu, '-');
            recEvt->weights.push_back(Weight{weight_upper});
            recEvt->weights.push_back(Weight{weight_lower});
            calib.at(1)(*genJets, genEvt->weights.front());
            calib.at(2)(*genJets, genEvt->weights.front());
            calib.at(1)(*recJets, genEvt->weights.front() * weight_upper);
            calib.at(2)(*recJets, genEvt->weights.front() * weight_lower);
        }

        // filling tree
        if (steering & DT::fill) tOut->Fill();
    }

    TDirectory * dir = fOut->mkdir("corrections");
    bool reset = slice.second > 0;
    correction.Write(dir, reset);

    raw.Write(fOut);
    for (auto& c: calib)
        c.Write(fOut);

    fOut->cd();
    recptPUwoWeights->Write("recptPUwoWeights");
    recptPUwWeights->Write("recptPUwWeights");
    recptLogWgt->Write("recptLogWgt");

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::PUprofile namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Correct PU profile (independently to the trigger).",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("MC", "corrections.PUprofile.MC", "profile obtained with `getPUprofile`")
               .arg<fs::path>("Data", "corrections.PUprofile.Data", "profile obtained with `getPUprofile`")
               .arg<float>("maxWeight", "corrections.PUprofile.maxWeight", "highest allowed value for the weight");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::PUprofile::applyPUprofCorrection(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
