#!/bin/zsh
set -e

cmd=$(basename $0)

if [ "$#" -ne 3 ]
then
    echo $cmd input output
    echo "where\tinputLumiJSON = (copy of) JSON file obtained from \`crab report\`"
    echo "     \tinputPileupJSON = either one of the official \`pileup_latest.txt\` or the output of \`pileupRecalc_HLT.py\`"
    echo "     \tHLTPATH = HLT_PFJet or HLT_AK8PFJet (also used for the output)"
    echo "     \tyear = 2016 or 2017 or 2018"
    echo "Notes:"
    echo " - It runs only if the output does not exist yet."
    echo " - By default, the input (output) directory is expected to be \`\$DAS_WORKAREA/tables/luminosities/20xx/\` (\`\$DAS_WORKAREA/tables/DiffPUprofiles/20xx/\`)."
    exit 1
fi

inputLumi=$1
inputPileup=$2
HLTPATH=$2
year=$3

if [ -f $HLTPATH ]
then
    ls $HLTPATH
    echo "Output already exists. Exiting."
    exit
fi

if (( year == 2016 ))
then
    triggers=(40 60 80 140 200 260 320 400 450)
    eras=(B C D E F G H)
elif (( year == 2017))
then
    triggers=(40 60 80 140 200 260 320 400 450 500)
    eras=(B C D E F)
elif (( year == 2018 ))
then
    triggers=(40 60 80 140 200 260 320 400 450 500)
    eras=(A B C D)
else
    echo "$year is not recognised. You should edit the present script to make sure that it can be handled (see \`\$CMSSW_BASE/src/Core/PUprofile/scripts\`)."
    exit
fi

# values recommended by Lumi POG for Run 2
typeset -A xsect
xsect[nominal]=69200
unc="0.046"
xsect[lower]=`echo "${xsect[nominal]}*(1-$unc)" | bc`
xsect[upper]=`echo "${xsect[nominal]}*(1+$unc)" | bc`

for var in nominal upper lower
do
    for t in "${triggers[@]}"
    do
        dir=$HLTPATH/$var/$t
        mkdir -p $dir

        cmd="pileupCalc.py -i $inputLumi --inputLumiJSON $inputPileup --calcMode true --minBiasXsec ${xsect[$var]} --maxPileupBin 150 --numPileupBins 150 --pileupHistName=$var $HLTPATH/$var/$t.root"
        echo $cmd
        $(echo $cmd) > $HLTPATH/$var/$t.out &

    done
    wait

    hadd $HLTPATH.root $HLTPATH/*root 
done

echo Done
