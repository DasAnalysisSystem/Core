# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

# This module contains functions to factilitate creating libraries and
# executables from the code layout in Core. Functions in this module expect the
# following folder structure:
#
#   Module/
#       CMakeLists.txt
#       bin/
#       interface/
#       src/
#       test/
#
# The `bin` folder contains executables, `interface` the headers and `src` the
# source code of supporting code, and `test` contains files for testing.

enable_testing()

# Returns the name of the current module in the variable passed as argument.
function(core_current_module_name)
    cmake_path(GET CMAKE_CURRENT_SOURCE_DIR FILENAME modname)
    set(${ARGV0} ${modname} PARENT_SCOPE)
endfunction()

# Creates a test from a module in scram layout. The optional LIBRARIES
# parameter gives a list of targets to link against. The test will be called
# without arguments.
function(core_add_test)
    cmake_parse_arguments(PARSE_ARGV 2 ARG "" "" "LIBRARIES")
    set(test_name ${ARGV0}_${ARGV1})
    add_executable(${test_name} test/${ARGV1}.cc)
    # We always need Darwin
    target_link_libraries(${test_name} PRIVATE Darwin::Darwin ${ARG_LIBRARIES})
    if (TARGET ${ARGV0})
        # If there is a library, usually we also need it
        target_link_libraries(${test_name} PRIVATE ${ARGV0})
    endif()
    add_test(NAME ${test_name} COMMAND ${test_name})
endfunction()

# Creates a library from a module in scram layout. The SOURCES parameter gives
# the list of source files to include in the shared object, to be found in the
# `src` subdirectory. The optional DEPENDS parameter is a list of libraries to
# link against. The optional TESTS parameter gives a list of test file names
# (without extension) to be passed to core_add_test(). The library is named
# `lib<PROJECT_NAME><MODULE>.so`.
#
# This function always creates a target named after the current module. It is
# usually the library target created with add_library(). The name of the
# library target can optionally be customized by passing TARGET_NAME. In that
# case, an alias target is created with the module name. This is intended to
# work around ROOT bug #15792 for modules that need ROOT dictionaries.
function(core_add_library)
    cmake_parse_arguments(PARSE_ARGV 0 ARG "" "TARGET_NAME" "DEPENDS;SOURCES;TESTS")
    list(TRANSFORM ARG_SOURCES PREPEND src/)

    core_current_module_name(modname)

    # TARGET_NAME is needed to work around for ROOT bug 15792.
    if (DEFINED ARG_TARGET_NAME)
        set(target ${ARG_TARGET_NAME})
    else()
        set(target ${modname})
    endif()

    add_library(${target} SHARED ${ARG_SOURCES})
    # Set library names to libCoreABC.so without changing the target name
    set_target_properties(${target} PROPERTIES OUTPUT_NAME ${PROJECT_NAME}${modname})
    target_include_directories(${target} PUBLIC  "interface")
    target_include_directories(${target} PRIVATE "src")
    target_link_libraries(${target} PUBLIC Darwin::Darwin ${ARG_DEPENDS})

    install(TARGETS ${target}
            RUNTIME DESTINATION "${CMAKE_INSTALL_LIBDIR}")

    if (NOT "${target}" STREQUAL "${modname}")
        # Create an ALIAS target with the module name for convenience
        add_library(${modname} ALIAS ${target})
    endif()

    # Tests
    foreach(test IN LISTS ARG_TESTS)
        core_add_test(${modname} ${test})
    endforeach()
endfunction()

# Creates an executable from a module in scram layout. The only mandatory
# argument is the name of the executable. A source file with the same name is
# expected to exist in the `bin` subfolder. The optional LIBRARIES parameter
# gives a list of libraries to link against.
#
# If a library exists for the current module, it is automatically linked to the
# executable. Darwin is always linked in.
#
# A test is automatically added to check that the executable produces a helper
# when run with `-h` and an example when run with `-e`. If NO_EXAMPLE is given,
# the test passes even if the executable has no example.
function(core_add_executable)
    cmake_parse_arguments(PARSE_ARGV 1 ARG "NO_EXAMPLE" "" "LIBRARIES")

    core_current_module_name(modname)

    add_executable(${ARGV0} bin/${ARGV0}.cc)
    # We always need CommonTools for the version header (this brings Darwin)
    target_link_libraries(${ARGV0} PRIVATE CommonTools ${ARG_LIBRARIES})
    if (TARGET ${modname})
        # If there is a library, usually we also need it
        target_link_libraries(${ARGV0} PRIVATE ${modname})
    endif()
    install(TARGETS ${ARGV0}
            RUNTIME DESTINATION "${CMAKE_INSTALL_BINDIR}")

    # Check helper strings
    if (ARG_NO_EXAMPLE)
        set(grep_string "Help screen")
    else()
        set(grep_string "Print config example")
    endif()
    add_test(NAME ${modname}_${ARGV0}_helper COMMAND ${ARGV0} -h)
    set_property(TEST ${modname}_${ARGV0}_helper
                 PROPERTY PASS_REGULAR_EXPRESSION "${grep_string}")
    # Check that we can print the git commit hash
    add_test(NAME ${modname}_${ARGV0}_git COMMAND ${ARGV0} -g)
    # A commit hash is exactly 40 lowercase alphanumeric characters. CMake
    # doesn't support very advanced regular expressions, so we can't test for
    # the length. What we can do is test that no character is outside the
    # allowed set.
    set_property(TEST ${modname}_${ARGV0}_git
                 PROPERTY FAIL_REGULAR_EXPRESSION "^a-z0-9")
    # Check that it can print an example
    if (NOT ARG_NO_EXAMPLE)
        add_test(NAME ${modname}_${ARGV0}_example COMMAND ${ARGV0} -e)
    endif()
endfunction()
