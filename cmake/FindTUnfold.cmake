# SPDX-FileCopyrightText: 2014 Alex Merry <alex.merry@kde.org>
# SPDX-FileCopyrightText: 2014 Martin Gräßlin <mgraesslin@kde.org>
# SPDX-FileCopyrightText: 2014 Christoph Cullmann <cullmann@kde.org>
# SPDX-FileCopyrightText: 2024 Louis Moureaux <louis.moureaux@cern.ch>
#
# SPDX-License-Identifier: BSD-3-Clause

#[=======================================================================[.rst:
FindTUnfold
-----------

Try to find TUnfold on a Unix system.

This will define the following variables:

``TUNFOLD_FOUND``
    True if (the requested version of) TUnfold is available
``TUNFOLD_VERSION``
    The version of TUnfold
``TUNFOLD_LIBRARIES``
    This can be passed to target_link_libraries() instead of the ``TUnfold::TUnfold``
    target
``TUNFOLD_INCLUDE_DIRS``
    This should be passed to target_include_directories() if the target is not
    used for linking
``TUNFOLD_DEFINITIONS``
    This should be passed to target_compile_options() if the target is not
    used for linking

If ``TUNFOLD_FOUND`` is TRUE, it will also define the following imported target:

``TUnfold::TUnfold``
    The TUnfold library

In general we recommend using the imported target, as it is easier to use.
Bear in mind, however, that if the target is in the link interface of an
exported library, it must be made available by the package config file.
#]=======================================================================]

include(${CMAKE_CURRENT_LIST_DIR}/ECMFindModuleHelpers.cmake)

ecm_find_package_version_check(TUnfold)

find_path(TUNFOLD_INCLUDE_DIR
    NAMES
        TUnfold.h
    PATH_SUFFIXES
        TUnfold
)
find_library(TUNFOLD_LIBRARY NAMES TUnfold RooUnfold)

# get version from header, should work on windows, too
if(TUNFOLD_INCLUDE_DIR)
    file(STRINGS "${TUNFOLD_INCLUDE_DIR}/TUnfold.h" TUNFOLD_H REGEX "^#define TUnfold_VERSION +\"V[^\"]*\"$")

    string(REGEX REPLACE "^.*TUnfold_VERSION +\"V([0-9]+).*$" "\\1" TUNFOLD_VERSION_MAJOR "${TUNFOLD_H}")
    string(REGEX REPLACE "^.*TUnfold_VERSION +\"V[0-9]+\\.([0-9]+).*$" "\\1" TUNFOLD_VERSION_MINOR  "${TUNFOLD_H}")
    set(TUNFOLD_VERSION "${TUNFOLD_VERSION_MAJOR}.${TUNFOLD_VERSION_MINOR}")

    set(TUNFOLD_MAJOR_VERSION "${TUNFOLD_VERSION_MAJOR}")
    set(TUNFOLD_MINOR_VERSION "${TUNFOLD_VERSION_MINOR}")

    unset(TUNFOLD_H)
endif()

include(FindPackageHandleStandardArgs)
find_package_handle_standard_args(TUnfold
    FOUND_VAR
        TUNFOLD_FOUND
    REQUIRED_VARS
        TUNFOLD_LIBRARY
        TUNFOLD_INCLUDE_DIR
    VERSION_VAR
        TUNFOLD_VERSION
)

if(TUNFOLD_FOUND AND NOT TARGET TUnfold::TUnfold)
    add_library(TUnfold::TUnfold UNKNOWN IMPORTED)
    set_target_properties(TUnfold::TUnfold PROPERTIES
        IMPORTED_LOCATION "${TUNFOLD_LIBRARY}"
        INTERFACE_COMPILE_OPTIONS "${TUNFOLD_DEFINITIONS}"
        INTERFACE_INCLUDE_DIRECTORIES "${TUNFOLD_INCLUDE_DIR}"
    )
endif()

mark_as_advanced(TUNFOLD_LIBRARY TUNFOLD_INCLUDE_DIR)

set(TUNFOLD_LIBRARIES ${TUNFOLD_LIBRARY})
set(TUNFOLD_INCLUDE_DIRS ${TUNFOLD_INCLUDE_DIR})

include(FeatureSummary)
set_package_properties(TUnfold PROPERTIES
    URL "https://libgit2.github.com/"
    DESCRIPTION "A plain C library to interface with the git version control system."
)

