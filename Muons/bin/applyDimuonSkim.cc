#include <iostream>
#include <filesystem>
#include <utility>

#include <TChain.h>
#include <TFile.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Di.h"

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Muons {

////////////////////////////////////////////////////////////////////////////////
/// Checks whether an event should be kept
template<class Muon>
bool keepEvent(const vector<Muon>& muons, float pt1, float pt2)
{
    return muons.size() >= 2 && muons[0].p4.Pt() > pt1 && muons[1].p4.Pt() > pt2;
}

////////////////////////////////////////////////////////////////////////////////
/// Skims the input tree to keep only events with at least two muons, with
/// configurable pT thresholds.
void applyDimuonSkim
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    const bool isMC = metainfo.Get<bool>("flags", "isMC");

    const auto pt1 = config.get<float>("skims.dimuon.pt1");
    metainfo.Set<float>("skims", "dimuon", "pt1", pt1);

    const auto pt2 = config.get<float>("skims.dimuon.pt2");
    metainfo.Set<float>("skims", "dimuon", "pt2", pt2);

    cout << "Skimming events: pt(mu1) > " << pt1 << "\t pt(mu2) > " << pt2 << endl;

    // event information
    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent", DT::facultative);

    auto recMuons = flow.GetBranchReadOnly<vector<RecMuon>>("recMuons", DT::facultative);
    RecDimuon * recDimuon = nullptr;
    if (recMuons != nullptr)
        recDimuon = flow.GetBranchWriteOnly<RecDimuon>("recDimuon");

    auto genMuons = flow.GetBranchReadOnly<vector<GenMuon>>("genMuons", DT::facultative);
    GenDimuon * genDimuon = nullptr;
    if (genMuons != nullptr)
        genDimuon = flow.GetBranchWriteOnly<GenDimuon>("genDimuon");

    if (!recMuons && !genMuons)
        BOOST_THROW_EXCEPTION( DE::BadInput("No muons in input tree", tIn) );

    // histogram used in MC for normalisation
    TH1 * hSumWgt = nullptr;
    if (isMC) hSumWgt = new TH1F("hSumWgt",";;#sum_{i} w_{i}", 1, -0.5, 0.5);

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        bool passesRec = true, passesGen = true;

        if (recMuons) {
            passesRec = keepEvent(*recMuons, pt1, pt2);
            if (passesRec) *recDimuon = recMuons->at(0) + recMuons->at(1);
            else            recDimuon->clear();
        }
        if (genMuons) {
            passesGen = keepEvent(*genMuons, pt1, pt2);
            if (passesGen) *genDimuon = genMuons->at(0) + genMuons->at(1);
            else            genDimuon->clear();
        }

        if ((steering & DT::fill) && (passesRec || passesGen)) tOut->Fill();

        if (!isMC) continue;
        auto w = genEvt->weights.front();
        hSumWgt->Fill(0.0, w);
    }

    metainfo.Set<bool>("git", "complete", true);
    fOut->cd();
    if (hSumWgt) hSumWgt->Write();

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Muon namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Selects events with at least two muons passing pT cuts "
                            "and creates dimuon objects directly in the tree.",
                            DT::config | DT::split | DT::fill);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("pt1", "skims.dimuon.pt1", "Minimum pT of the first muon")
               .arg<float>("pt2", "skims.dimuon.pt2", "Minimum pT of the second muon");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Muons::applyDimuonSkim(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
