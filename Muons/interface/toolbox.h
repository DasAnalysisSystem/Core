#include <tuple>
#include <string>
#include <vector>
#include <filesystem>

#include <boost/algorithm/string.hpp>

#include <darwin.h>

namespace DAS::Muon {

std::tuple<std::filesystem::path, std::string> getLocation (const std::string& location)
{
    using namespace std;
    namespace al = boost::algorithm;
    namespace fs = filesystem;

    vector<string> words;
    al::split(words, location, al::is_any_of(":"), al::token_compress_on);
    if (words.size() != 2)
        BOOST_THROW_EXCEPTION( invalid_argument("Expecting `/path/to/file.ext:histname`") );
    fs::path filename = words.front();
    string histname = words.back();
    return {filename, histname};
}

} // end of DAS::Muon namespace
