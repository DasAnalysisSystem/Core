#include <cassert>
#include <cstdlib>

#include <algorithm>
#include <filesystem>
#include <iostream>
#include <vector>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Math/VectorUtil.h"

#include "Core/JetVetoMaps/interface/Conservative.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::JetVeto {

////////////////////////////////////////////////////////////////////////////////
/// Remove regions that are not properly simulated both in data and simulation
/// (fixed via unfolding at the end).
///
/// Maps are derived by Hannu (Helsinki), and should be downloaded and adapted.
void applyConservativeVetoMap
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    const auto& jetveto_file = config.get<fs::path>("corrections.jetvetomap.filename");
    JetVeto::Conservative jetveto(jetveto_file);
    metainfo.Set<fs::path>("corrections", "jetvetomap", "filename", jetveto_file);

    auto genEvt = isMC ? flow.GetBranchReadOnly<GenEvent>("genEvent") : nullptr;
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");

    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets");

    ControlPlots raw("raw");
    ControlPlots nominal("HotKillers");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        auto ev_w = (isMC ? genEvt->weights.front().v : 1) * recEvt->weights.front().v;

        raw(*recJets, ev_w);

        for (auto& recJet: *recJets)
            jetveto(recJet);

        nominal(*recJets, ev_w);

        if (steering & DT::fill) tOut->Fill();
    }

    fOut->cd();
    jetveto.Write(fOut);
    raw.Write(fOut);
    nominal.Write(fOut);

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::JetVeto namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Apply jet veto maps by setting the weight of reconstructed jet "
                            "to zero whenever that jet is found to be in a veto zone.",
                            DT::config | DT::split | DT::Friend);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("jetvetomap", "corrections.jetvetomap.filename", "ROOT file containing jet vet maps");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::JetVeto::applyConservativeVetoMap(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
