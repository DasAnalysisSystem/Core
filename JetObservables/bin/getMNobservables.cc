#include <cassert>
#include <cstdlib>

#include <iostream>
#include <filesystem>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include "Core/JetObservables/interface/MuellerNavelet.h"

#include <TFile.h>
#include <TH2.h>
#include <TProfile.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::MN {

static const vector<double> binsY = {0., 0.25, .5, 0.75, 1., 1.25, 1.5, 1.75, 2., 2.25, 2.5, 2.75, 3., 3.25, 3.5, 3.75, 4., 4.25, 4.5, 4.75, 5., 5.25, 5.5, 5.75, 6., 6.25, 6.5, 6.75, 7., 7.25, 7.5, 7.75, 8., 8.25, 8.5, 8.75, 9., 9.25, 9.5};

static const vector<double> binsPhi = {0.,.1*M_PI, .2*M_PI, .3*M_PI, .4*M_PI, .5*M_PI, .6*M_PI, .7*M_PI, .8*M_PI, .9*M_PI, M_PI};

static const double minY = binsY.front(), maxY = binsY.back(), maxy = 4.7;
static const double minpt = 32, maxpt = 1.e10;

////////////////////////////////////////////////////////////////////////////////
/// Contains the histograms that are filled in getMNobservables.
/// These histograms are filled through the different Fill() methods.
/// Beware that the three Fill() methods are different.
///
/// The attributes of this structure are the following:
/// - name        --> name of the instance to avoid memory leak
/// - njets       --> histogram of the number of jets in the event
/// - dsigmadphi  --> $(\pi - \Delta \phi)$ between the MN Jets
/// - ptleading   --> $p_T$ of the leading jet (in term of pt)
/// - ptratmn     --> ratio of pt of the mnjets (see the Obs2Jets)
/// - deta        --> Delta Eta distribution between the MN Jets
/// - pt          --> pt of all the jets in the event
/// - ptmnfwd     --> pt of the most fwd MN jet
/// - ptmnbwd     --> pt of the most bwd MN jet
/// - njetsptave  --> 2D histogram jets multiplicity and average pt of the MN jets
/// - njetsdeta   --> 2D histogram jets multiplicity and delat eta btw the MN jets
/// - ptavedeta   --> 2D histogram binned in $p_{T,MN,ave}$ and $ \Delta \eta$
/// - dphideta    --> 2D histogram binned in $\Delta\phi$ and $\Delta\eta$
/// - cos1,2,3    --> TProfile containing $ \left\langle \cos \left( n \left( \pi - \Delta \phi \right) \right) \right\rangle $ for $n=1,2,3$
/// - ptavemini   --> See Obs2Jets
/// - detaavemini --> See Obs2Jets
/// - retaavemini --> See Obs2Jets
/// - rptexpdeta  --> See Obs2Jets
struct Hist {
    TString name;

    TH1F njets, dsigmadphi, ptleading, ptratmn, pt, ptmnfwd, ptmnbwd, deta;
    TH2F njetsptave, njetsdeta, ptavedeta, dphideta, pteta;
    TProfile cos1, cos2, cos3;

    TH1F ptavemini, detaavemini, retaavemini, rptexpdeta;

    Hist (TString thename) :
        name(thename),
        njets(name+"njets",";N_{jets};N_{evts}",20,0,20),
        dsigmadphi(name+"dsigmadphi",";\\Delta \\phi;N_{jets}",binsPhi.size()-1, binsPhi.data()),
        ptleading(name+"ptlead","pt of the leading jet;p_{T} (GeV);N_{jets}", pt_edges.size()-1, pt_edges.data()),
        ptratmn(name+"ptratmn","Pt Balance;\\frac{p_{T,1}}{p_{T,2}};N_{jets}",30,0,1),
        pt(name+"pt","INclusive jets;p_{T} (GeV);N_{jets}",pt_edges.size()-1,pt_edges.data()),
        ptmnfwd(name+"ptmnfwd","Pt Backward MN jet;p_{T}^{fwd} (GeV);N_{jets}",pt_edges.size()-1,pt_edges.data()),
        ptmnbwd(name+"ptmnbwd","Pt Foward MN jet;p_{T}^{fwd} (GeV);N_{jets}",pt_edges.size()-1,pt_edges.data()),
        deta(name+"delta_eta",";\\Delta \\eta;\\frac{d \\sigma}{d p_{T} d \\eta};", binsY.size()-1, binsY.data()),
        njetsptave(name+"njetsptave","Jet multiplicity;p_{T}^{ave};N_{jets}", pt_edges.size()-1, pt_edges.data(),20,0,20),
        njetsdeta(name+"njetsdeta","Jet multiplicity;\\Delta \\eta;N_{jets}", binsY.size()-1, binsY.data(),10 ,0 ,10),
        ptavedeta(name+"ptavedeta","ptavedeta",pt_edges.size()-1, pt_edges.data(),binsY.size()-1, binsY.data()),
        dphideta(name+"dphideta","dphideta",30, 0, M_PI, binsY.size()-1, binsY.data()),
        pteta(name+"pteta",";p_{T};\\eta", pt_edges.size()-1, pt_edges.data(), y_edges.size()-1, y_edges.data()),
        cos1(name+"cos1","cos1",30,0,10,0,1),
        cos2(name+"cos2","cos2",30,0,10,0,1),
        cos3(name+"cos3","cos3",30,0,10,0,1),
        ptavemini(name+"ptavemini","ptavemini",50,0.,80.),
        detaavemini(name+"detaavemini","detaavemini",50,0.,5.),
        retaavemini(name+"retaavemini","retaavemini",50,0.,1.),
        rptexpdeta(name+"rptexpdeta","rptexpdeta",50,0.,2.)
    { }

    void Fill (vector<RecJet>& recJets, double evweight){
        double jetswgt = (recJets.front()).weights.front();
        for (auto it = next(recJets.begin()) ; it != recJets.end() ; ++it)
            jetswgt *= (it->weights).front();
        for (auto it = recJets.begin() ; it != recJets.end() ; ++it) {
            pt.Fill(it->CorrPt(), evweight * (it->weights).front());
            pteta.Fill(it->CorrPt(), it->p4.Eta(), evweight * (it->weights).front());
        }
        njets.Fill( recJets.size(), evweight*jetswgt );
        ptleading.Fill( (recJets.front()).CorrPt(), evweight * (recJets.front()).weights.front() );
    }

    void Fill (vector<GenJet>& genJets, double evweight){
        double jetswgt = (genJets.front()).weights.front();
        for (auto it = next(genJets.begin()) ; it != genJets.end() ; ++it)
            jetswgt *= (it->weights).front();
        for (auto it = genJets.begin() ; it != genJets.end() ; ++it) {
            pt.Fill( (it->p4).Pt(), evweight * (it->weights).front() );
            pteta.Fill( (it->p4).Pt(), (it->p4).Eta(), evweight * (it->weights).front());
        }
        njets.Fill(genJets.size(), evweight * jetswgt);
        ptleading.Fill( (genJets.front()).p4.Pt(), evweight * (genJets.front()).weights.front() );
    }

    void Fill (Obs2Jets& mn2Jets, ObsMiniJets& miniJets, double evweight){
        double w1 = evweight * mn2Jets.weight;
        double w2 = evweight * miniJets.weight;

        dsigmadphi.Fill( mn2Jets.DPhi() , w1 );
        deta.Fill( mn2Jets.DEta(), w1 );
        njetsptave.Fill( mn2Jets.PtAve(), miniJets.size()+2, w1 );
        njetsdeta.Fill( mn2Jets.DEta(), miniJets.size()+2, w1 );
        ptratmn.Fill( mn2Jets.PtRatMN(), w1 );
        ptavedeta.Fill( mn2Jets.PtAve(), mn2Jets.DEta(), w1 );
        dphideta.Fill( mn2Jets.DPhi(), mn2Jets.DEta(), w1 );

        ptmnfwd.Fill( mn2Jets.PtFWD(), evweight * mn2Jets.weightFWD );
        ptmnbwd.Fill( mn2Jets.PtBWD(), evweight * mn2Jets.weightBWD );

        cos1.Fill( mn2Jets.DEta(), mn2Jets.CosDPhi(1), w1 );
        cos2.Fill( mn2Jets.DEta(), mn2Jets.CosDPhi(2), w1 );
        cos3.Fill( mn2Jets.DEta(), mn2Jets.CosDPhi(3), w1 );

        if (miniJets.size()>0) ptavemini.Fill(miniJets.PtAveMini(), w2 );
        if (miniJets.size()>1) {
            detaavemini.Fill(miniJets.DEtaAveMini(), w2 );
            retaavemini.Fill(miniJets.REtaAveMini(), w2 );
            rptexpdeta.Fill(miniJets.RPtExpDEta(), w2 );
        }
    }

private:
    template<typename HIST> void Loop (vector<HIST *> hists, TDirectory * dir)
    {
        for (HIST * h: hists){
            h->SetDirectory(dir);
            TString histoname(h->GetName());
            histoname.ReplaceAll(name,"");
            h->Write(histoname);
        }
    }

public:
    void Write (TDirectory * dir)
    {
        dir->cd();
        auto subdir = dir->mkdir(name);
        subdir->cd();
        Loop<TH1F>({&njets, &dsigmadphi, &ptleading, &ptratmn, &ptmnfwd, &ptmnbwd, &pt,
                    &ptavemini, &detaavemini, &retaavemini, &rptexpdeta, &deta}, subdir);
        Loop<TH2F>({&njetsptave, &njetsdeta, &ptavedeta, &dphideta, &pteta}, subdir);
        Loop<TProfile>({&cos1, &cos2, &cos3}, subdir);
    }
};

////////////////////////////////////////////////////////////////////////////////
///  Calculate the observables that can be used to caracterise Mueller-Navelet Jets
void getMNobservables
            (const vector<fs::path> inputs, //!< input ROOT files (n-tuples)
             const fs::path output, //!< name of output root file containing the histograms
             const pt::ptree& config, //!< config file from `DT::Options`
             const int steering, //!< steering parameters from `DT::Options`
             const DT::Slice slice = {1,0} //!< slices for running
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");

    auto gEv = flow.GetBranchReadOnly<GenEvent>("genEvent");
    auto rEv = flow.GetBranchReadOnly<RecEvent>("recEvent");

    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");
    auto genJets = isMC ? flow.GetBranchReadOnly<vector<GenJet>>("genJets") : nullptr;

    Hist genHist("gen"), // NOTE: one should only declare the object if (isMC)
         recHist("rec");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = (steering & DT::verbose) == DT::verbose ? ::cout : DT::dev_null;

        auto MNJets = GetMNJet<RecJet>(*recJets, [](RecJet jet){return jet.CorrPt() < 35;});
        if (MNJets) {
            double recy = std::abs( MNJets->first.p4.Eta() - MNJets->second.p4.Eta() );
            bool LowRecY  = recy < minY,
                 HighRecY  =  recy >= maxY;
            bool goodRec = (!LowRecY) && (!HighRecY)
                && MNJets->first.p4.Eta() < maxy && MNJets->second.p4.Eta() < maxy
                && minpt < MNJets->first.CorrPt() && MNJets->first.CorrPt() < maxpt
                && minpt < MNJets->second.CorrPt() && MNJets->second.CorrPt() < maxpt;

            if (goodRec) {
                double evweight = rEv->weights.front();
                if (isMC) evweight *= gEv->weights.front();
                recHist.Fill(*recJets, evweight);
                Obs2Jets obs2jets(MNJets->first, MNJets->second);
                auto minijets = GetMiniJets(*recJets, *MNJets, function<bool(RecJet&)>([](RecJet& jet){return jet.CorrPt() < 20;}));
                ObsMiniJets obsminijets(minijets);
                recHist.Fill(obs2jets, obsminijets, evweight);
            }
        }

        if (!isMC) continue;

        auto genMNJets = GetMNJet<GenJet>(*genJets, [](GenJet jet){return jet.p4.Pt() < 35;});
        if (genMNJets) {
            double genY = std::abs( genMNJets->first.p4.Eta() - genMNJets->second.p4.Eta() );
            bool LowGenY  = genY < minY,
                 HighGenY = genY >= maxY,
                 goodGen = (!LowGenY) && (!HighGenY)
                     && genMNJets->first.p4.Eta() < maxy && genMNJets->second.p4.Eta() < maxy
                     && minpt < genMNJets->first.p4.Pt() && genMNJets->first.p4.Pt() < maxpt
                     && minpt < genMNJets->second.p4.Pt() && genMNJets->second.p4.Pt() < maxpt;

            if (goodGen) {
                double evweight = gEv->weights.front();
                genHist.Fill(*genJets, evweight);
                Obs2Jets obs2jets(genMNJets->first, genMNJets->second);
                auto minijets = GetMiniJets(*genJets, *genMNJets, function<bool(GenJet&)>([](GenJet& jet){return jet.p4.Pt() < 20;}));
                ObsMiniJets obsminijets(minijets);
                genHist.Fill(obs2jets, obsminijets, evweight);
            }
        }
    }

    recHist.Write(fOut);
    if (isMC)
        genHist.Write(fOut);

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of DAS::MN namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Project n-tuple on Mueller-Navelet jet observables. "
                            "These observables are designed for low-pileup data.",
                            DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::MN::getMNobservables(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
