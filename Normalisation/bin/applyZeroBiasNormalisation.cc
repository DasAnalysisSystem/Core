#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/Normalisation/interface/PhaseSelection.h"

#include <TString.h>
#include <TFile.h>
#include <TH1.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Normalise with effective luminosity or prescales.
/// The weight $\frac{ \text{Prescale} * \mathcal{L}^{-1} }{ \epsilon_\text{trigger} } $ is applied by event.
/// Apply phaseSelection and remove events already covered by jet triggers.
/// 
/// To combine ZeroBias and JetHT together, we take the first trigger's TurnOn
/// point(lowest pt threshold in `$DARWIN_TABLES/triggers/lumi`) as maxpt.
void applyZeroBiasNormalisation
           (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
            const fs::path& output, //!< output ROOT file (n-tuple)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering, //!< parameters obtained from explicit options
            const DT::Slice slice = {1,0} //!< number and index of slice
            )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (isMC)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only real data may be used as input.", metainfo) );

    auto total_lumi = config.get<float>("corrections.normalisation.total_lumi");
    auto maxpt = config.get<float>("corrections.normalisation.maxpt");
    auto method  = config.get<string>("corrections.normalisation.method"  );

    auto inv_total_lumi = 1. / total_lumi;

    auto recJets = flow.GetBranchReadWrite<vector<RecJet>>("recJets", DT::facultative);
    auto recEvt = flow.GetBranchReadWrite<RecEvent>("recEvent");
    auto trigger = flow.GetBranchReadOnly<Trigger>("zbTrigger");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        // we find the leading jet in tracker acceptance
        auto leadingInTk = DAS::Normalisation::phaseSel(*recJets);

        // removed events alreadu covered by jet triggers
        if (leadingInTk != recJets->end() && leadingInTk->CorrPt() >= maxpt) continue;

        auto preHLT   = trigger->PreHLT.front();
        auto preL1min = trigger->PreL1min.front();
        auto preL1max = trigger->PreL1max.front();
        if (preL1min != preL1max)
            BOOST_THROW_EXCEPTION( invalid_argument("Different preL1min and preL1max value.") );

        auto prescale = preHLT * preL1min;

        // sanity checks:
        // we assume that the same prescale is indeed constant for a given LS
        {
            static vector<map<pair<int,int>,int>> prescales(1); // this could be written in a simpler way...
            pair<int,int> runlum = {recEvt->runNo, recEvt->lumi};
            if (prescales.front().count(runlum)) 
                assert(prescales.front().at(runlum) == prescale);
            else 
                prescales.front()[runlum] = prescale;
        }

        if (method == "prescales")
            recEvt->weights *= prescale * inv_total_lumi * 1;
        else if (method == "lumi")
            recEvt->weights *= inv_total_lumi;
        else {
            BOOST_THROW_EXCEPTION( invalid_argument("method: " + method + " is not defined.") );
        }

        if (steering & DT::fill) tOut->Fill();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Normalise the ZeroBias samples to the luminosity.\n"
                            "Existing method:\n"
                            "- 'prescales': Reweight the events by its prescale per LS\n"
                            "Input lumi: Total lumi of whole Dataset\n"
                            "- 'lumi': Reweight the events by trigger luminosity\n"
                            "Input lumi: effictive lumi",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<float>("lumi", "corrections.normalisation.total_lumi", "lumi in /pb")
               .arg<int>("maxpt", "corrections.normalisation.maxpt", "max pt to be covered by ZeroBias trigger")
               .arg<string>("method"  , "corrections.normalisation.method"  , "'prescales' or 'lumi'");
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::applyZeroBiasNormalisation(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
