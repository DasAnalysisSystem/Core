#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <map>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TFile.h>
#include <TChain.h>
#include <TH2.h>

#include "Core/Normalisation/interface/LumiUnc.h"

#include "applyDataNormalisation.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Apply the the kinematic selection, ensuring that events in the dataset have fired a trigger and
/// correcting the weight of the events with the associated trigger prescales, trigger efficiency,
/// and total luminosity.
/// The weight $\frac{ \text{Prescale} * \mathcal{L}^{-1} }{ \epsilon_\text{trigger} } $ is applied by event.
/// The argument 'strategy' defines the selection criterion of the events and the calculation of the weight
/// (see functor in applyDataNormalisation.h).
void applyDataNormalisation
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (isMC)
        BOOST_THROW_EXCEPTION( DE::BadInput("Only real data may be used as input.", metainfo) );

    // event information
    auto recEvt = flow.GetBranchReadWrite<RecEvent>("recEvent");
    auto trigger = flow.GetBranchReadOnly<Trigger>("jetTrigger");

    // physics object information
    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");
    auto hltJets = flow.GetBranchReadOnly<vector<FourVector>>("hltJets");

    int year = metainfo.Get<int>("flags", "year");

    // defining functor to apply data prescales
    auto lumi_file    = config.get<fs::path>("corrections.normalisation.luminosities" ),
         unc_file     = config.get<fs::path>("corrections.normalisation.uncertainties"),
         turnon_file  = config.get<fs::path>("corrections.normalisation.turnons"      ),
         efficiencies = config.get<fs::path>("corrections.normalisation.efficiencies" );
    auto strategy = config.get<string>("corrections.normalisation.strategy"),
         method   = config.get<string>("corrections.normalisation.method"  );
    Functor normalisation(lumi_file, turnon_file, efficiencies, strategy, method, year);
    LumiUnc lumi_unc(unc_file, year);
    fOut->cd();

    bool applySyst = steering & DT::syst;
    if (applySyst)
        for (string source: lumi_unc.sources)
            metainfo.Set<string>("variations", RecEvent::WeightVar, source);

    // a few control plots
    ControlPlots::isMC = false;
    ControlPlots corrNoTrigEff("corrNoTrigEff");

    vector<ControlPlots> corr { ControlPlots("nominal") };
    TList * ev_wgts = metainfo.List("variations", RecEvent::WeightVar);
    for (TObject * obj: *ev_wgts) {
        auto name = dynamic_cast<TObjString*>(obj)->GetString();
        corr.push_back(ControlPlots(name));
    }
    //if (applySyst)
    /// \todo add lumi unc variations

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        if (applySyst)
            lumi_unc(recEvt);

        if (recJets->size() == 0) continue;

        RecJet leadingJet = normalisation(*recEvt, *recJets, *hltJets, *trigger);
        cout << "leadingJet: " << leadingJet << endl;
        // the normalisation itself is done in `Normalisation::Functor`
        /// \todo revisit the logic (having inputs modified + a return object may be confusing)

        /// \todo reimplement the logic to avoid a selection on the weight
        if (recEvt->weights.front() <= 0) continue;
        if (steering & DT::fill) tOut->Fill();

        float evtWgts = recEvt->weights.front();
        float efficiency = normalisation.eff(leadingJet);

        cout << "evtWgts = " << evtWgts << '\n'
             << "efficiency = " << efficiency << endl;
        if (efficiency <= 0) continue;

        corrNoTrigEff(*recJets, evtWgts*efficiency); // compensate
        for (size_t i = 0; i < corr.size(); ++i)
            corr.at(i)(*recJets, recEvt->weights.at(i));

        // Exclusive curves are filled (i.e. one event can populate only a trigger curve).
        // The ibit value is determined by
        // the leading jet but also the other jets of the event can populate the trigger curve.
        for (auto& jet: *recJets) {
            float y = jet.AbsRap();
            float w = recEvt->weights.front();
            normalisation.eff.contribs.at(normalisation.ibit)->Fill(jet.CorrPt(), y, w);
        }
    }

    fOut->cd();
    corrNoTrigEff.Write(fOut);
    for (size_t i = 0; i < corr.size(); ++i)
        corr.at(i).Write(fOut);
    TDirectory * controlplots = fOut->mkdir("controlplots");
    controlplots->cd();
    for (TH2 * h: normalisation.eff.contribs) {
        h->SetDirectory(controlplots);
        h->Write();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Normalise the data samples to the luminosity. Prescaled events may either "
                            "be normalised to the respective trigger luminosities or using trigger precales.\n"
                            "Existing strategies:\n"
                            " - 'pt': assume that the jets are sorted in pt and try to match the leading jet"
                            " - 'eta': sort the jets in eta and try to match the most fwd and second most fwd",
                            DT::config | DT::split | DT::Friend | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<fs::path>("luminosities" , "corrections.normalisation.luminosities" , "2-column file with luminosity per trigger"       )
               .arg<fs::path>("uncertainties", "corrections.normalisation.uncertainties", "luminosity uncertaintiy (including correlations)") /// \todo make optional with `/dev/null`
               .arg<fs::path>("turnons"      , "corrections.normalisation.turnons"      , "2-column file with turn-on per trigger"          )
               .arg<fs::path>("efficiencies" , "corrections.normalisation.efficiencies" , "output ROOT file of `getTriggerTurnons`"         )
               .arg<string>("strategy", "corrections.normalisation.strategy", "'eta' or 'pt'"        )
               .arg<string>("method"  , "corrections.normalisation.method"  , "'prescales' or 'lumi'");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::applyDataNormalisation(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
