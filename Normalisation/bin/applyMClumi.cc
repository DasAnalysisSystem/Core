#include <cstdlib>
#include <thread>
#include <iostream>
#include <filesystem>
#include <utility>
#include <optional>

#include <TChain.h>
#include <TFile.h>
#include <TROOT.h>
#include <TH1D.h>
#include <TRegexp.h>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Jet.h"

#include "Core/CommonTools/interface/ControlPlots.h"
#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Retrieve the sum of the weights obtained with `getSumWeights` and combines
/// it with the input cross section to calculate the normalisation factor.
float GetNormFactor (const int steering,
                     const vector<fs::path>& sumWgts, //!< file containing the hist
                     const float xsec) //!< cross section value
{
    auto h = DT::Flow(steering, sumWgts).GetInputHist<TH2>("hSumWgt");
    auto sumw = h->Integral(0, -1, // duplicates in overflow cancel overweighted entries
                            1, 2); // both positive and negative weights
    if (sumw <= 0)
        BOOST_THROW_EXCEPTION( DE::BadInput("Negative sum of weights.", h) );
    if (xsec <= 0)
        BOOST_THROW_EXCEPTION( invalid_argument("Negative cross section.") );
    auto factor = xsec/sumw;
    if (steering & DT::verbose)
        cout << xsec << '/' << sumw << '=' << factor << endl;
    return factor;
}

////////////////////////////////////////////////////////////////////////////////
/// Normalise with sum of weights (to be computed) and cross section (given)
///
/// \note A cut-off for events with hard scale above 5 TeV is applied,
///       since these events are not realistic
void applyMClumi
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const vector<fs::path>& sumWgts, //!< input ROOT files (histogram)
         const fs::path& output, //!< output ROOT file (n-tuple)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    // normalisation factor
    auto xsection = config.get<float>("corrections.MCnormalisation.xsection");
    float factor = GetNormFactor(steering, sumWgts, xsection);
    metainfo.Set<float>("corrections", "MCnormalisation", "xsection", xsection);

    // declaring branches
    auto genEvt = flow.GetBranchReadWrite<GenEvent>("genEvent");
    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto genJets = flow.GetBranchReadOnly<vector<GenJet>>("genJets", DT::facultative);
    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets", DT::facultative);

    // control plot
    ControlPlots::isMC = true;
    ControlPlots plots("controlplots");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        // sanity check
        if (genEvt->weights.size() != 1 || recEvt->weights.size() != 1)
            BOOST_THROW_EXCEPTION( DE::AnomalousEvent("Unexpected event weights", tIn) );
        if (recJets != nullptr && recJets->size() > 0 && recJets->front().scales.size() > 1)
            BOOST_THROW_EXCEPTION( DE::AnomalousEvent("Unexpected jet energy scale variations", tIn) );

        // renormalisation
        genEvt->weights.front() *= factor;
        if (steering & DT::fill) tOut->Fill();

        // control plot
        if (genJets != nullptr)
            plots(*genJets, genEvt->weights.front()                          );
        if (recJets != nullptr)
            plots(*recJets, genEvt->weights.front() * recEvt->weights.front());
    }

    metainfo.Set<bool>("git", "complete", true);

    plots.Write(fOut);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs, sumWgts;
        fs::path output;

        auto options = DAS::Options(
                            "Normalise the MC samples to the cross section.",
                            DT::config | DT::split | DT::Friend);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory (n-tuples)")
               .inputs("sumWgts", &sumWgts, "input ROOT file(s) or directory (output of `getSumWeights`")
               .output("output", &output, "output ROOT file")
               .arg<float>("xsection", "corrections.MCnormalisation.xsection", "cross section value");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::applyMClumi(inputs, sumWgts, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
