#include <bitset>
#include <cmath>
#include <cstdlib>
#include <iostream>

#include <TChain.h>
#include <TFile.h>
#include <TH1.h>

#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/Objects/interface/Event.h"

#include <darwin.h>

using namespace std;

namespace fs = filesystem;

namespace pt = boost::property_tree;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

enum {
    WEIGHT = 0b001, //!< use the weights
    SIGN   = 0b010, //!< use the sign
    CUTOFF = 0b100  //!< apply a selection on the weight
};

////////////////////////////////////////////////////////////////////////////////
/// Sum the weights from ROOT n-tuple. Mostly useful for MC to estimate the
/// effective number of events, which is necessary to normalise.
///
/// The histogram is filled in a way so that it can be used for two purposes:
/// - provide a distribution of the weights and choose a max weight,
/// - extract the normalisation factor by takaing its integral.
void getSumWeights
        (const vector<fs::path>& inputs, //!< input ROOT files (n-tuple)
         const fs::path& output, //!< output ROOT file (histograms)
         const pt::ptree& config, //!< config handled with `Darwin::Tools::Options`
         const int steering, //!< parameters obtained from explicit options
         const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto tOut = flow.GetOutputTree(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC) BOOST_THROW_EXCEPTION( DE::BadInput("Only MC may be used as input.",
                                      make_unique<TFile>(inputs.front().c_str() )) );

    const auto maxAbsValue = config.get<float>("corrections.weights.maxAbsValue");
    const auto mode = config.get<string>("corrections.weights.mode");

    cout << "maxAbsValue = " << maxAbsValue << endl;

    auto evnt = flow.GetBranchReadOnly<GenEvent>("genEvent");

    TString title = ";log_{10}(w);sign;#sum_{i=1}^{events} w_{i}";
    if (maxAbsValue > 0) title = Form("w < %f", maxAbsValue);
    auto h = make_unique<TH2D>("hSumWgt", title, 100, 3, 40,
                                                 2, -1, 1);
    const double overflow = h->GetXaxis()->GetXmax()+1.;

    uint32_t wm = 0;
    if      (mode == "weight"  ) wm = WEIGHT | SIGN;
    else if (mode == "signOnly") wm = SIGN;
    else if (mode != "count"   )
         BOOST_THROW_EXCEPTION( invalid_argument(mode + " is unknown") );

    if (maxAbsValue > 0) wm |= CUTOFF;

    cout << "wm = " << bitset<8>(wm) << endl;

    if (steering & DT::verbose)
        cout << setw(20) << "w"
             << setw(20) << "absw"
             << setw(20) << "x [log10(abs(w))]"
             << setw(20) << "y [sign]"
             << setw(20) << "z [eff. weight]" << endl;

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused ]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        const double w = evnt->weights.front();
        if (w == 0.) continue;

        if (!isfinite(w))
            BOOST_THROW_EXCEPTION( runtime_error("Infinite or NaN weight:"s + w) );

        // the weight is used twice:
        // - once for the bin to fill, which should always be [log(abs(w)),+/-1]
        // - once for the actual weight in the histogram, which depends on the options

        const double absw = std::abs(w),
                     x = log10(absw),
                     y = copysign(0.5,w); // bin center
        double z = wm & WEIGHT ? absw : 1;
        if (wm & SIGN) z = copysign(z,w);

        cout << setw(20) << w
             << setw(20) << absw
             << setw(20) << x
             << setw(20) << (2*y) // we want the sign only (bin center is irrelevant)
             << setw(20) << z << endl;

        h->Fill(x, y, z);

        if (x >= overflow)
            cerr << orange << "Warning: a good weight is being filled in the overflow.\n" << def;

        /// \note The values beyond `maxAbsValue` are filled a 2nd time in the overflow
        ///       with the opposite weight. The truncated sum of the weights is then
        ///       obtained from the integral by including the overflow.
        if ((wm & CUTOFF) && absw >= maxAbsValue)
            h->Fill(overflow, y, -z);
    }

    metainfo.Set<bool>("git", "complete", true);

    h->Write();

    cout << __func__ << ' ' << slice << " end" << endl;
}

} // end of namespace DAS::Normalisation

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options("Get the sum of the weights.", DT::split | DT::config);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file")
               .arg<string>("mode", "corrections.weights.mode",
                            "apply generator weight (`weight`), apply sign only (`signOnly`), "
                            "just count the number of events (`count`)")
               .arg<float>("maxAbsValue", "corrections.weights.maxAbsValue",
                           "Maximum absolute weight to consider (0 to run without cut)");

        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Normalisation::getSumWeights(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
