#!/bin/zsh
set -e

cmd=$(basename $0)

host=`hostname`
if [[ "${host[1,6]}" != "lxplus" ]]
then
    echo "WARNING: this command might only work on lxplus...."
fi

if [[ $# -lt 2 ]]
then
    echo $cmd HLTPATH year 
    echo "where\tHLTPATH = HLT_PFJet or HLT_AK8PFJet"
    echo "     \gyear = (20)1{6,7,8}"
    echo "Notes:"
    echo " - This script expects the different \`processedLumi.json\` files (obtained from CRAB jobs) renamed into \`{A,B,C,...}.json\` in the CWD."
    echo " - By default, these JSON files are stored in \`\$DAS_WORKAREA/tables/luminosities/20xx/\` (but this is not mandatory: it will work as long as it can find the JSON files in the CWD)."
    exit 1
else
    HLTPATH=$1
    year=$2
fi

# YEAR & TRIGGERS

if (( year > 2000 ))
then
    (( year -= 2000 ))
fi

echo year: $year

if (( year == 16 ))
then
    triggers=(40 60 80 140 200 260 320 400 450)
    eras=(B C D E F G H)
elif (( year == 17))
then
    triggers=(40 60 80 140 200 260 320 400 450 500)
    eras=(B C D E F)
elif (( year == 18 ))
then
    triggers=(40 60 80 140 200 260 320 400 450 500)
    eras=(A B C D)
else
    echo "$year is not recognised. You should edit the present script to make sure that it can be handled (see \$CMSSW_BASE/src/Core/Normalisation/scripts/)."
    exit
fi

if [[ "${HLTPATH}" != "HLT_PFJet" && "${HLTPATH}" != "HLT_AK8PFJet" ]]
then
    echo "$HLTPATH is not recognised. You should edit the present script to make sure that it can be handled (see \$CMSSW_BASE/src/Core/Normalisation/scripts/)."
    exit 1
fi

mkdir -p $HLTPATH

typeset -A xsect
xsect[nominal]=69200
unc="0.046"
xsect[upper]=`echo "${xsect[nominal]}*(1-$unc)" | bc`
xsect[lower]=`echo "${xsect[nominal]}*(1+$unc)" | bc`

# RUNNING BRILCALC
export PATH=$HOME/.local/bin:/cvmfs/cms-bril.cern.ch/brilconda3/bin:$PATH

for var in nominal upper lower
do
    for era in "${eras[@]}"
    do
        infile=${era}.json
        for t in "${triggers[@]}"
        do
            mkdir -p ${HLTPATH}/${t}/${era}
            outfile=${HLTPATH}/${t}/${era}/${var}.txt

            echo $era $t $var
            brilcalc lumi --byls --minBiasXsec ${xsect[$var]} -u /pb \
                --normtag /cvmfs/cms-bril.cern.ch/cms-lumi-pog/Normtags/normtag_PHYSICS.json \
                -i $infile --hltpath "${HLTPATH}${t}_v*" | grep '\.' | \
                awk '{print $13 " " $15}'       > $outfile &
                awk '{print $13 " " $15}' | grep -v '|'      > $outfile &
            # grep: trick to ignore line w/o relevant information
            # awk: 13 -> lumi, 15 -> pileup
        done
        wait
    done
done

# GETTING LUMIS

typeset -A effLumis
for t in "${triggers[@]}"
do
    effLumi=0
    for era in "${eras[@]}"
    do
        infile=${HLTPATH}/${t}/${era}/nominal.txt
        lumi=`awk '{s+=$1}END{print s}' $infile`
        echo $era $t $lumi
        effLumi=$((effLumi + lumi))
    done
    effLumis[$t]=$effLumi
done

outfile=$HLTPATH/Run20${year}.lumi
rm -f $outfile
touch $outfile
for t in "${triggers[@]}"
do
    echo "$t $effLumis[$t]" >> $outfile
done

echo Done
