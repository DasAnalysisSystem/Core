#include <cassert>
#include <cstdlib>
#include <fstream>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/toolbox.h"
#include "Core/Normalisation/interface/TriggerLumi.h"

#define DUMMY -999

using namespace std;

namespace fs = filesystem;

namespace DAS {

TriggerLumi::TriggerLumi (int trigger) :
    turnon(DUMMY), weight(DUMMY),
    h(new TH1D(Form("%d",trigger), "", nPtBins, pt_edges.data())) 
{
    cout << __func__ << endl;
}

[[ deprecated ]]
void TriggerLumi::Fill (const vector<RecJet>& jets)
{
    assert(h);
    for (const RecJet& jet: jets)
        h->Fill(jet.CorrPt(), weight);
}

////////////////////////////////////////////////////////////////////////////////
/// GetLumiFromFiles returns a map containing key value pairs corresponding to
/// the trigger threshold at HLT (e.g. 40, 60, etc.) and the object containing 
/// the turn-on point and its effective luminosity.
map<int, TriggerLumi> GetLumiFromFiles (const fs::path& lumi_file, const fs::path& turnon_file)
{
    map<int, TriggerLumi> trigger_luminosities;

    ifstream infile; // will be used twice consecutively

    auto checkFile = [&infile]() {
        if (!infile.fail()) return;
        cerr << "Failure at opening the file\n";
        exit(EXIT_FAILURE);
    };

    // 1) get effective luminosity
    assert(fs::exists(lumi_file));
    infile.open(lumi_file);
    cout << "Opening lumi file: " << lumi_file << endl;
    checkFile();
    while (infile.good()) { 
        int trigger;
        float lumi;
        infile >> trigger >> lumi;
        if (infile.eof()) break;
        TriggerLumi trigger_lumi(trigger);
        trigger_lumi.weight = 1./lumi;
        cout << trigger << ' ' << lumi << ' ' << trigger_lumi.weight << '\n';
        trigger_luminosities.insert({trigger, trigger_lumi});
    } 
    infile.close(); 

    // 2) get turn-on points
    cout << "Opening turn-on file: " << turnon_file << endl;
    assert(fs::exists(turnon_file));
    infile.open(turnon_file);
    checkFile();
    while (infile.good()) { 
        int trigger;
        double turnon;
        infile >> trigger >> turnon;
        if (infile.eof()) break;
        cout << trigger << ' ' << turnon << '\n';
        TriggerLumi& trigger_lumi = trigger_luminosities.at(trigger);
        trigger_lumi.turnon = ceil(turnon); //goes to int 
    } 
    infile.close(); 

    // then remove pairs with dummy entry
    for (auto trigger_lumi = trigger_luminosities.begin();
              trigger_lumi != trigger_luminosities.end(); /* nothing */) {

        // remove or increment
        if (   trigger_lumi->second.turnon == DUMMY
            || trigger_lumi->second.weight == DUMMY) 
            trigger_lumi = trigger_luminosities.erase(trigger_lumi);
        else ++trigger_lumi;

    }

    // display
    for (auto trigger_lumi: trigger_luminosities) 
        cout << trigger_lumi.first << '\t'
             << trigger_lumi.second.turnon << '\t'
             << trigger_lumi.second.weight << '\n';

    cout << flush;

    return trigger_luminosities;
}

}
