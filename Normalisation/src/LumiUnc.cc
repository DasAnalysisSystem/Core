#include "Core/Normalisation/interface/LumiUnc.h"
#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Format.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include <TString.h>

#include <colours.h>

using namespace DAS::Normalisation;
using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

LumiUnc::LumiUnc (const std::filesystem::path& fname, int year)
{
    if (fname == "/dev/null") {
        cerr << orange << "No lumi uncertainties will be applied\n" << def;
        return;
    }

    if (!fs::exists(fname))
        BOOST_THROW_EXCEPTION( fs::filesystem_error("File could not be found", fname,
                                make_error_code(errc::no_such_file_or_directory)) );
    pt::ptree config;
    pt::read_info(fname.string(), config);

    // first pushing the uncorrelated uncertainties in such a way that the ntuples
    // of different years may be added safely
    //
    /// \todo use a bit once technically available
    const pt::ptree& uncorrelated = config.get_child("uncorrelated");
    for (auto& entry: uncorrelated) {

        string source = "lumi" + entry.first;
        sources.push_back(source + SysUp);
        sources.push_back(source + SysDown);

        float factor = year == stoi(entry.first) ? entry.second.get_value<float>() : 0.;
        factors.push_back(factor);
    }

    // then pushing the correlated uncertainties, but only for the input year
    const pt::ptree& correlated = config.get_child("correlated");
    for (auto& child: correlated) {

        string source = child.first;
        sources.push_back(source + SysUp);
        sources.push_back(source + SysDown);

        float factor = child.second.get<float>(Form("%d", year));
        factors.push_back(factor);
    }

    for (auto& factor: factors) factor /= 100;
}

void LumiUnc::operator() (RecEvent * ev) const
{
    auto nom = ev->weights.front();
    for (auto factor: factors) {
        ev->weights.push_back(Weight{nom * (1+factor)});
        ev->weights.push_back(Weight{nom * (1-factor)});
    }
}
