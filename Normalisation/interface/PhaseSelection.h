#ifndef DAS_NORM_PHASESPACESEL
#define DAS_NORM_PHASESPACESEL
#include "Core/Objects/interface/Jet.h"
#include <vector>
#include <cmath>

namespace DAS::Normalisation {

std::vector<DAS::RecJet>::iterator phaseSel (std::vector<DAS::RecJet>& recjets) {
    auto leadingInTk = recjets.begin();
    while (leadingInTk != recjets.end()
            && std::abs(leadingInTk->Rapidity()) >= 3.0)
        ++leadingInTk;

    return leadingInTk;
}

} // end of DAS::Normalisation namespace

#endif
