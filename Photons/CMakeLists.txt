# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

core_add_executable(applyPhotonConversionVeto)
core_add_executable(applyPhotonID)
core_add_executable(getPhotonPerformance NO_EXAMPLE)
