#include <cassert>
#include <cstdlib>

#include <filesystem>
#include <iostream>
#include <vector>

#include "Core/Objects/interface/Event.h"
#include "Core/Objects/interface/Lepton.h"
#include "Core/Objects/interface/Photon.h"

#include "Core/CommonTools/interface/DASOptions.h"
#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/toolbox.h"

#include <TEfficiency.h>
#include <TFile.h>
#include <TString.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Photon {

struct PerformanceHist {
    TString name; //!< TDirectory's name (e.g. barrel_rec)

    TH1F pt, e, eta, phi;

    PerformanceHist (TString name)
        : name(name)
          , pt (name+"pt", "", 500, 0, 1000)
          , e  (name+"e", "", 500, 0, 1000)
          , eta(name+"eta", "", 100, -3, 3)
          , phi(name+"phi", "", 100, -3.15, 3.15)
    {
        cout << "New set of histograms for " << name << endl;
    }

    void Fill (const auto& Photon, float w)
    {
        pt.Fill(Photon.CorrP4().Pt(), w);
        e.Fill(Photon.CorrP4().E(), w);
        eta.Fill(Photon.CorrP4().Eta(), w);
        phi.Fill(Photon.CorrP4().Phi(), w);
    }

    void Write (TDirectory * dir)
    {
        dir->cd();
        auto subdir = dir->mkdir(name);
        subdir->cd();
        for (TH1 * h: {&pt, &e, &eta, &phi}) {
            h->SetDirectory(subdir);
            TString hname = h->GetName();
            hname.ReplaceAll(name, "");
            h->Write(hname);
        }
    }
};

void Fill (const GenPhoton& genPhoton,
           const vector<RecPhoton>& recPhotons,
           const float minPt,
           const float minAbsEta,
           const float maxAbsEta,
           const double w_gen_ev,
           const double w_rec_ev,
           map<TString, PerformanceHist>& histograms,
           TString region,
           TEfficiency& eff_pt
           )
{
    double w_gen_ph = genPhoton.weights.front();
    // Fill histograms for generated photon
    histograms.at(region + "_gen").Fill(genPhoton, w_gen_ev * w_gen_ph);

    const float maxDr = 0.1;
    bool matched = false;
    // Loop over the reco photons
    for (const auto& recPhoton: recPhotons) {
        if (recPhoton.CorrP4().Pt() < minPt) continue;
        if (abs(recPhoton.CorrP4().Eta()) > maxAbsEta) continue;
        if (abs(recPhoton.CorrP4().Eta()) < minAbsEta) continue;

        // Calculate weight for the reconstructed photon
        double w_rec_ph = recPhoton.weights.front();

        // Fill histograms for reconstructed photon
        histograms.at(region + "_rec").Fill(recPhoton, w_gen_ev*w_rec_ev*w_rec_ph);

        // Matching criterion (dR < maxDr)
        if (DeltaR(recPhoton.CorrP4(), genPhoton.CorrP4()) > maxDr) continue;

        // Fill histograms for matched photon
        histograms.at("matched_" + region + "_rec").Fill(recPhoton, w_gen_ev * w_rec_ev* w_rec_ph);
        histograms.at("matched_" + region + "_gen").Fill(genPhoton, w_gen_ev * w_gen_ph); // \todo Use rec-level weight? (depends what we want to do with this histogram)

        matched = true;
        break;
    }
    
    eff_pt.FillWeighted(matched, w_gen_ph, genPhoton.CorrPt());
}

////////////////////////////////////////////////////////////////////////////////
/// Obtain photon plots
void getPhotonSpectrum(
    const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
    const fs::path& output,         //!< output ROOT file (n-tuple)
    const int steering,             //!< parameters obtained from explicit options
    const DT::Slice slice = {1, 0}  //!< number and index of slice
) {
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut,tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    auto isMC = metainfo.Get<bool>("flags", "isMC");
    if (!isMC)
        BOOST_THROW_EXCEPTION( invalid_argument("Only for MC samples") );

    auto recEvt = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto genEvt = flow.GetBranchReadOnly<GenEvent>("genEvent");

    auto recPhotons = flow.GetBranchReadOnly<vector<RecPhoton>>("recPhotons");
    auto genPhotons = flow.GetBranchReadOnly<vector<GenPhoton>>("genPhotons");

    map<TString, PerformanceHist> histograms;
    for (TString level: {"rec", "gen"})
    for (TString region: {"barrel", "endcaps"}) {
        TString name = region + '_' + level;
        histograms.insert({name, PerformanceHist(name)});
        name = "matched_" + name;
        histograms.insert({name, PerformanceHist(name)});
    }

    vector<double> eff_binning {0, 10, 20, 35, 50, 65, 80, 100, 120, 150, 200, 500};
    TEfficiency barrel_eff_pt("barrel_eff_pt", "", eff_binning.size()-1, eff_binning.data()),
        endcaps_eff_pt("endcaps_eff_pt", "", eff_binning.size()-1, eff_binning.data());

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[maybe_unused]] 
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        // Loop over the gen photons first!
        for (const auto& genPhoton : *genPhotons) {
            const float maxBarrelAbsEta = 1.48,
                        maxEndcapAbsEta = 3.0;
            const float minPt = 20;

            if (genPhoton.CorrPt() < minPt) continue;

            // Calculate weight for the generated photon
            double w_gen_ev = genEvt->weights.front(),
                   w_rec_ev = recEvt->weights.front();

            // Calculation for the barrel
            if (abs(genPhoton.CorrP4().Eta()) < maxBarrelAbsEta)
                Fill(genPhoton, *recPhotons, minPt, 0., maxBarrelAbsEta,
                     w_gen_ev, w_rec_ev, histograms, "barrel", barrel_eff_pt);
            // Calculation for the endcaps
            else if (abs(genPhoton.CorrP4().Eta()) < maxEndcapAbsEta)
                Fill(genPhoton, *recPhotons, minPt, maxBarrelAbsEta, maxEndcapAbsEta,
                     w_gen_ev, w_rec_ev, histograms, "endcaps", endcaps_eff_pt);
        }
    }

    for (auto& h: histograms)
        h.second.Write(fOut);
    fOut->cd();
    for (auto eff: {&barrel_eff_pt, &endcaps_eff_pt}) {
        eff->SetDirectory(fOut);
        eff->Write();
    }

    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of namespace DAS::Photon

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main(int argc, char* argv[]) {
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options("Obtain gen/reco plots for photons in the barrel/endcaps.", DT::split);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
        options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::Photon::getPhotonSpectrum(inputs, output, steering, slice);
    } 
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
