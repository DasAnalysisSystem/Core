#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>
#include <map>

#include <TString.h>
#include <TFile.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/DASOptions.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::BTagging {

////////////////////////////////////////////////////////////////////////////////
/// Extract the B-tagged fraction from the output of `getBTagDiscriminant`.
/// Works for both data and MC.
void getBTagFraction
           (const vector<fs::path>& inputs, //!< input ROOT files (histograms)
            const fs::path& output, //!< output ROOT file (histograms)
            const int steering //!< parameters obtained from explicit options
            )
{
    [[ maybe_unused]]
    static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

    cout << __func__ << " start" << endl;

    auto hIn = DT::Flow(steering, inputs).GetInputHist<TH3>("discriminant");
    if (hIn->GetNbinsZ() != 4) {
        const char * what = Form("Found %d working points (expecting 4)", hIn->GetNbinsZ());
        BOOST_THROW_EXCEPTION( DE::BadInput(what, hIn) );
    }

    auto fOut = DT::GetOutputFile(output);

    auto project = [&hIn](const char * name, int bin) {
        return unique_ptr<TH1>( hIn->ProjectionX(name, 0, -1, bin, 4) );
    };

    auto hIncl = project("inclusive", 1);
    int im = hIncl->FindBin(22), // because the btagging calib starts with pt > 20 GeV
        iM = hIncl->GetNbinsX();
    auto integrate = [im,iM](const unique_ptr<TH1>& h) {
        return h->Integral(im, iM);
    };
    double sum = integrate(hIncl);

    map<TString, int> WPs { {"loose", 2}, {"medium", 3}, {"tight", 4} };
    for (const auto& WP: WPs) {
        auto h = project(WP.first, WP.second);
        h->SetTitle("B-tagged jet fraction for " + WP.first + " working point");
        cout << h->GetTitle() << ": " << integrate(h)/sum << endl;
        h->Divide(h.get(), hIncl.get(), 1, 1, "b");
        h->Write();
    }

    cout << __func__ << " stop" << endl;
}

} // end of DAS::BTagging namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Extract fraction of b-tagged jets "
                            "from output of `getBTagDiscriminant`.");
        options.inputs("inputs", &inputs, "input ROOT files")
               .output("output", &output, "output ROOT file")
               (argc, argv);
        const int steering = options.steering();

        DAS::BTagging::getBTagFraction(inputs, output, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
