#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include "Core/Objects/interface/Jet.h"
#include "Core/Objects/interface/Event.h"

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"

#include <TFile.h>
#include <TH3.h>

#include "Math/VectorUtil.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::BTagging {

////////////////////////////////////////////////////////////////////////////////
/// Working points provided by BTV for Full-Run-2 Ultra-Legacy samples.
///
/// Reference: [Wiki](https://btv-wiki.docs.cern.ch/ScaleFactors/)
vector<double> GetDeepJetEdges (int year, bool HIPM = false)
{
    switch (year) {
        case 2016:
            if (HIPM) return { 0., 0.0508, 0.2598, 0.6502, 1. };
            else      return { 0., 0.0480, 0.2489, 0.6377, 1. };
        case 2017:
            return {0., 0.0532, 0.3040, 0.7476, 1.};
        case 2018:
            return {0., 0.0490, 0.2783, 0.7100, 1.};
        default:
            const char * what = Form("The year %d is currently not supported", year);
            BOOST_THROW_EXCEPTION( invalid_argument(what) );
    }
}

////////////////////////////////////////////////////////////////////////////////
/// Get B-tagged binned discriminant in bins of pt, true flavour, and
/// discriminant value. Works for both data and MC (although in the case of
/// data, the true flavour is always trivial).
void getBTagBinnedDiscriminant
       (const vector<fs::path>& inputs, //!< input ROOT files (n-tuples)
        const fs::path& output, //!< output ROOT file (n-tuple)
        const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
        const int steering, //!< parameters obtained from explicit options
        const DT::Slice slice = {1,0} //!< number and index of slice
        )
{
    cout << __func__ << ' ' << slice << " start" << endl;

    DT::Flow flow(steering, inputs);
    auto tIn = flow.GetInputTree(slice);
    auto [fOut, tOut] = flow.GetOutput(output);

    DT::MetaInfo metainfo(tOut);
    metainfo.Check(config);
    //auto isMC = metainfo.Get<bool>("flags", "isMC");
    auto year = metainfo.Get<int>("flags", "year");
    bool HIPM = metainfo.Find("flags", "labels");
    if (HIPM && year != 2016)
        cerr << orange << "The HIPM label may only be used for 2016 samples. It will be just ignored for other years.\n" << def;

    const vector<double> flavour_edges = {0.5, 1.5, 2.5, 3.5, 4.5, 5.5}, /// \todo GSP?
                         disc_edges = GetDeepJetEdges(year, HIPM);
    const int nFlBins = flavour_edges.size()-1,
              nDiscBins = disc_edges.size()-1;
    auto discriminant = make_unique<TH3D>("discriminant", "B-tag discriminant",
                                            nPtBins, pt_edges.data(),
                                            nFlBins, flavour_edges.data(),
                                            nDiscBins, disc_edges.data());

    if (steering & DT::syst)
        BOOST_THROW_EXCEPTION( logic_error("B-tagging variations not yet implemented\n") );

    //GenEvent * genEvent = nullptr; /// \todo genEvent??
    auto recEvent = flow.GetBranchReadOnly<RecEvent>("recEvent");
    auto recJets = flow.GetBranchReadOnly<vector<RecJet>>("recJets");

    for (DT::Looper looper(tIn); looper(); ++looper) {
        [[ maybe_unused]]
        static auto& cout = steering & DT::verbose ? ::cout : DT::dev_null;

        /// \todo loop over variations

        auto evWgt = recEvent->weights.front();
        /// \todo gen event weight
        //if (isMC) evWgt *= genEvt->weights.front();

        for (const RecJet& jet: *recJets) {
            auto jWgt = jet.weights.front();
            discriminant->Fill(jet.CorrPt(), jet.hadronFlavour(), jet.DeepJet.B(), jWgt*evWgt);
        }
    }

    fOut->cd();
    discriminant->SetDirectory(fOut);
    discriminant->Write();
    metainfo.Set<bool>("git", "complete", true);

    cout << __func__ << ' ' << slice << " stop" << endl;
}

} // end of DAS::BTagging


#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        vector<fs::path> inputs;
        fs::path output;

        auto options = DAS::Options(
                            "Fill distributions of the discriminants in bins of "
                            "jet kinematic variables and for the fixed working points.",
                            DT::config | DT::split | DT::syst);
        options.inputs("inputs", &inputs, "input ROOT file(s) or directory")
               .output("output", &output, "output ROOT file");
               /// \todo provide WPs as input?
        const auto& config = options(argc, argv);
        const auto& slice = options.slice();
        const int steering = options.steering();

        DAS::BTagging::getBTagBinnedDiscriminant(inputs, output, config, steering, slice);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
