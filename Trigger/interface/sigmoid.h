#include <cassert>
#include <cstdlib>

#include <iostream>
#include <fstream>

#include <TF1.h>
#include <TH1.h>

using namespace std;

namespace DAS::Normalisation {

const float threshold = 0.995;
////////////////////////////////////////////////////////////////////////////////
/// Replaces TF1::GetX() which does not return the same value when run after
/// compilation or in interactive mode...
///
/// Principle: just scan the pt axis with step = 1 until eff > 0.995
float GetX (TF1 * f)
{
    double m = 0, M = 7000;
    f->GetRange(m,M);

    for (float pt = m; pt < M; ++pt)
        if (f->Eval(pt) > threshold)
            return pt;

    cerr << "No threshold was found from " << m << " to " << M << ".\nAborting.\n";
    exit(EXIT_FAILURE);
}

////////////////////////////////////////////////////////////////////////////////
/// Fit the trigger efficiency with sigmoid function
/// in [trigger, 3* trigger], where trigger is the HLT pt threshold.
///
/// The sigmoid function is defined as follows:
/// ```latex
/// \epsilon (p_T) = a + 0.5 \times (1-a) \times \left( 1 + \erf \left( \frac{p_T - \mu}{\sigma} \right) \right)
/// ```
/// where a, $\mu$ and $\sigma$ are the three parameters.
///
/// NOTE:
/// - the output is very sensitive to the fit range
/// - unclear if/how JES uncertainties should be taken into account
template<typename STREAM>
[[ deprecated ]]
TF1 * FitTriggerEfficiency
            (int trigger,                   //!< trigger HLT threshold
             TH1 * h,                       //!< efficiency histogram
             STREAM& Stream,                //!< `cout` or file
             int from,                      //!< last turnon, to know from where the ref is efficient
             const char * options = "NQERS")//!< fit options for TF1 function
{
    assert(from > 0);
    int mbin = h->GetXaxis()->FindBin(  from),
        Mbin = h->GetXaxis()->FindBin(3*from);
    float m = h->GetBinLowEdge(mbin+1), M = h->GetBinLowEdge(Mbin);
    for (int bin = 0; bin <= mbin; ++bin) {
        h->SetBinContent(bin, 0);
        h->SetBinError  (bin, 0);
    }
    for (int bin = Mbin+1; bin <= h->GetNbinsX()+1; ++bin) {
        h->SetBinContent(bin, 0);
        h->SetBinError  (bin, 0);
    }

    cout << "Fitting trigger " << trigger << " from " << m << " to " << M << endl;

    // function
    TF1 * f = new TF1(Form("sigmoid%d", trigger),
            "[0]+0.5*(1-[0])*(1+erf((x-[1])/[2]))",
            m, M);

    f->SetParNames("a", "#mu", "#sigma");

    //f->FixParameter(0,0);
    f->SetParameter(0,0.1);
    f->SetParLimits(0,0,0.9);

    f->SetParameter(1,1.1*trigger);
    f->SetParLimits(1,m,M);

    f->SetParameter(2,10);
    f->SetParLimits(2,1,150);

    // fit
    h->Fit(f, options, "", m, M);

    float turnon = GetX(f);

    Stream << trigger << '\t' << turnon << '\n';

    return f;
}

////////////////////////////////////////////////////////////////////////////////
/// DON'T fit the trigger efficiency
/// but just find the first bin above threshold
template<typename STREAM> float GetTriggerEfficiency
            (int trigger,    //!< trigger HLT threshold
             TH1 * h,        //!< efficiency histogram
             STREAM& Stream, //!< `cout` or file
             float from,     //!< last turnon, to know from where the ref is efficient
             float minEff)   //!< min efficiency (e.g. different for barrel and forward region)
{
    int mbin = h->GetXaxis()->FindBin(  from),
        Mbin = h->GetXaxis()->FindBin(6*from); /// \todo Where does the factor 6 come from?
    for (int bin = 0; bin < mbin; ++bin) {
        h->SetBinContent(bin, 0);
        h->SetBinError  (bin, 0);
    }
    for (int bin = Mbin+1; bin <= h->GetNbinsX()+1; ++bin) {
        h->SetBinContent(bin, 0);
        h->SetBinError  (bin, 0);
    }
    for (int bin = mbin; bin <= Mbin; ++bin) {
        float content = h->GetBinContent(bin);
        if (content < minEff) continue;
        float turnon = h->GetBinLowEdge(bin);
        Stream << trigger << '\t' << turnon << '\n';
        return turnon;
    }
    return -1;
}

} // end of DAS::Trigger namespace
