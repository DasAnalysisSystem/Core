#include <cassert>
#include <cstdlib>
#include <cmath>

#include <filesystem>
#include <fstream>
#include <iostream>

#include <TH2.h>
#include <TKey.h>
#include <TString.h>

#include "Core/CommonTools/interface/binnings.h"
#include "Core/CommonTools/interface/DASOptions.h"

#include "Core/Trigger/interface/sigmoid.h"
#include "Core/Trigger/interface/match.h"

#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/info_parser.hpp>

#include "common.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS::Normalisation {

////////////////////////////////////////////////////////////////////////////////
/// Get the trigger turn-on points from the trigger curves
void getTriggerTurnons
                  (const fs::path& input, //!< input ROOT file (n-tuple)
                   const fs::path& outputRoot, //!< output ROOT file with efficiency
                   const fs::path& outputTxt, //!< output text file with turn-ons
                   const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
                   const int steering //!< parameters obtained from explicit options
                   )
{
    cout << __func__ << " start" << endl;

    auto fOut = DT::GetOutputFile(outputRoot);

    /// \todo Use metainfo
    //DT::MetaInfo metainfo(tOut);
    //metainfo.Check(config);
    int year = config.get<int>("flags.year"); /// \todo Get year from metainfo?

    cout << setw(10) << "trigger";
    for (auto yBin: yBins) cout << setw(18) << yBin;
    cout << endl;

    pt::ptree triggers_lumi;
    auto lumi_file = config.get<fs::path>("corrections.normalisation.luminosities"); /// \todo Get lumis from metainfo?
    pt::read_info(lumi_file.c_str(), triggers_lumi);
    vector<int> triggerThresholds;
    for (auto trigger_lumi: triggers_lumi)
        triggerThresholds.push_back(stoi(trigger_lumi.first));

    auto fIn = make_unique<TFile>(input.c_str(), "READ");
    map<int, int> turnonsFinal;
    TIter Next(fIn->GetListOfKeys()) ;
    TKey* key = nullptr;
    TString defMethod = "TnP"; // only for the first trigger(s)
    while ( (key = dynamic_cast<TKey*>(Next())) ) { // looping over methods
        if (TString(key->ReadObj()->ClassName()) != TString("TDirectoryFile"))
            continue;

        fIn->cd();
        auto dIn = dynamic_cast<TDirectory*>(key->ReadObj());

        int lastturnon = triggerThresholds.front();
        cout << dIn << endl;

        TString method = dIn->GetName();
        cout << method << endl;

        fOut->cd();
        auto dOut = fOut->mkdir(method);

        TIter Next2(dIn->GetListOfKeys()) ;
        while ( (key = dynamic_cast<TKey*>(Next2())) ) { // looping of triggers
            if (TString(key->ReadObj()->ClassName()) != TString("TDirectoryFile"))
                continue;

            dIn->cd();
            auto ddIn = dynamic_cast<TDirectory*>(key->ReadObj());

            TString trigName = ddIn->GetName();
            assert(trigName.Contains("HLT"));

            dOut->cd();
            auto ddOut = dOut->mkdir(trigName);

            trigName.ReplaceAll("HLT","");
            int trigger = trigName.Atoi();
            if (method == "TnP"            && trigger < triggerThresholds.at(0)) continue;
            if (method == "emulation"      && trigger < triggerThresholds.at(1)) continue;
            if (method == "emulationShift" && trigger < triggerThresholds.at(2)) continue;
            cout << setw(10) << trigger;

            auto h2  = unique_ptr<TH2>(ddIn->Get<TH2>("test"));
            {
                auto den = unique_ptr<TH2>(ddIn->Get<TH2>("ref"));
                h2->Divide(h2.get(), den.get(), 1, 1, "B");
            }

            ddOut->cd();

            h2->SetDirectory(ddOut);
            h2->SetName("efficiency");

            vector<int> turnonsNow;
            for (int y = 1; y <= h2->GetNbinsY(); ++y) {

                // efficiency curve
                auto h1 = unique_ptr<TH1>(h2->ProjectionX(Form("efficiency_ybin%d", y), y, y));
                //cout << y << '\t';
                static ostream bitBucket(0); // printing to nowhere
                double threshold = 0.995;
                switch (year) {
                    case 2017:
                        if (y > 5) threshold = 0.99;
                        break;
                    case 2018:
                        if (y > 3) threshold = 0.99;
                        break;
                    case 2016:
                    default:
                        if (y > 5) threshold = 0.99; //2016 Pre
                        //if (y > 5) threshold = 0.99; 2016 Post
                }
                float from = h1->GetBinLowEdge(h1->FindBin(lastturnon)+1);
                int turnon = Normalisation::GetTriggerEfficiency(trigger, h1.get(), bitBucket, from, threshold);
                h1->SetTitle(Form("%d (%.3f)", turnon, threshold));
                h1->SetDirectory(dOut);
                h1->Write();
                turnonsNow.push_back(turnon);
            }

            /// \todo implement a more general logic to use only rapidity <3 turnons to
            /// determine the overall turnon
            auto maxturnon = max_element(turnonsNow.begin(), turnonsNow.begin()+6);
            h2->SetTitle(Form("%d", *maxturnon));
            h2->Write();
            for (auto turnon: turnonsNow) {
                if (turnon == *maxturnon) cout << "\x1B[32m\e[1m";
                if (turnon < 0) cout << "\x1B[31m\e[1m";
                cout << setw(18) << turnon;
                cout << "\x1B[30m\e[0m";
            }
            cout << endl;

            // final choice
            if (method == defMethod)
                turnonsFinal[trigger] = *maxturnon;

            defMethod = "emulation";
            lastturnon = *maxturnon;

            if (lastturnon == -1) {
                    cerr << "Didn't find any turnon for trigger " << trigger << '\n';
                    lastturnon = trigger;
            }
        } // end of loop over triggers
    } // end of loop over method

    pt::ptree turnon_file;
    for (auto& turnon: turnonsFinal)
        turnon_file.put<int>(to_string(turnon.first), turnon.second);
    pt::write_info(outputTxt.string(), turnon_file);

    cout << __func__ << " end" << endl;
}

} // end of DAS::Normalisation namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, outputRoot, outputTxt;

        auto options = DAS::Options(
                            "Determine the trigger thresholds and efficiencies.\n"
                            "Strategy:\n"
                            " - For the first trigger (presumably HLT40), only TnP is available\n"
                            " - For all other triggers, emulation is used by default\n"
                            " - The other methods are used as cross-checks",
                            DT::config);

        options.input ("input", &input, "input ROOT file")
               .output("outputRoot", &outputRoot, "output ROOT file with trigger efficiency curves")
               .output("outputTxt" , &outputTxt , "output 2-column text file with turn-on points"  )
               .arg<int>("year", "flags.year", "4-digit year") /// \todo get year from metainfo
               .arg<fs::path>("luminosities" , "corrections.normalisation.luminosities" , "2-column text file with luminosity per trigger");  /// \todo get lumis from metainfo

        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::Normalisation::getTriggerTurnons(input, outputRoot, outputTxt, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
