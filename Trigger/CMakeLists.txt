# SPDX-License-Identifier: GPLv3-or-later
#
# SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

core_add_executable(getTriggerCurves)
core_add_executable(getTriggerTurnons)

