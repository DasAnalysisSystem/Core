#include <cstdlib>
#include <cassert>
#include <iostream>
#include <vector>
#include <filesystem>

#include <TString.h>
#include <TFile.h>
#include <TH1.h>

#include "Math/VectorUtil.h"

#include "Core/CommonTools/interface/DASOptions.h"

#include <darwin.h>

using namespace std;

namespace pt = boost::property_tree;
namespace fs = filesystem;

namespace DE = Darwin::Exceptions;
namespace DT = Darwin::Tools;

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Template for function (TODO)
void SimpleExec 
           (const fs::path& input, //!< input ROOT files (histograms)
            const fs::path& output, //!< output ROOT file (histograms)
            const pt::ptree& config, //!< config handled with `Darwin::Tools::options`
            const int steering //!< parameters obtained from explicit options 
            )
{
    cout << __func__ << " start" << endl;

    // TODO

    cout << __func__ << " stop" << endl;
}

} // end of DAS namespace

#ifndef DOXYGEN_SHOULD_SKIP_THIS
int main (int argc, char * argv[])
{
    try {
        DT::StandardInit();

        fs::path input, output;

        auto options = DAS::Options("Template for simple executable." /* TODO */,
                            DT::config /* TODO */);
        options.input ("input" , &input , "input ROOT file")
               .output("output", &output, "output ROOT file")
               /* TODO: add here any needed argument */
               (argc, argv);
        const auto& config = options(argc, argv);
        const int steering = options.steering();

        DAS::SimpleExec(input, output, config, steering);
    }
    catch (boost::exception& e) {
        DE::Diagnostic(e);
        return EXIT_FAILURE;
    }

    return EXIT_SUCCESS;
}
#endif
