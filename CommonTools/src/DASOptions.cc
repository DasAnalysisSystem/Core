// SPDX-License-Identifier: GPLv3-or-later
//
// SPDX-FileCopyrightText: Louis Moureaux <louis.moureaux@cern.ch>

#include "DASOptions.h"

// We cannot generate version.h when compiled in CMSSW.
#ifndef DAS_IN_CMSSW
#   include "version.h"
#endif

namespace DT = Darwin::Tools;

DT::Options DAS::Options(const char *description, int options)
{
#ifdef DAS_IN_CMSSW
    // Getting the commit hash is not supported by the scram build system. Pass
    // a null hash, which means using the Darwin commit hash. This is the best
    // we can do.
    return DT::Options(description, options, nullptr);
#else
    return DT::Options(description, options, DAS_COMMIT_HASH);
#endif
}
