#pragma once

#include "Core/Objects/interface/Format.h"
#include "Core/Objects/interface/Weight.h"

#include <darwin.h>

#include <TFile.h>
#include <TH1.h>

#include <filesystem>
#include <map>
#include <utility>
#include <vector>

namespace DAS {

/**
 * \brief Gets an histogram from a TFile
 * \throws Darwin::Exceptions::BadInput if the histogram doesn't exist
 */
inline std::unique_ptr<TH1> getHistSafe (std::unique_ptr<TFile>& f,
                                         const std::string& name)
{
    /// \todo Move to Darwin?
    auto h = std::unique_ptr<TH1>(f->Get<TH1>(name.c_str()));
    if (!h) {
        std::cerr << name << std::endl;
        BOOST_THROW_EXCEPTION( Darwin::Exceptions::BadInput("Can't find required histogram", f) );
    }
    h->SetDirectory(nullptr);
    return h;
}

/**
 * \brief Customization point for how weights are retrieved from an object.
 *
 * The default implementation tries to use the `weights` field.
 */
template<class Object>
Weights &weightsRef (Object& obj)
{
    return obj.weights;
}

/**
 * \brief A generic base class to simplify applying scale factors
 * \tparam Object  The object type to apply scale factors for
 * \tparam Context A set of object types that are needed to derive cuts and
 *                 scale factors, but to which no corrections is applied.
 *
 * GenericSFApplier can be used in situations where one wants to apply a cut to
 * a collection of objects (e.g. photons) and correct the efficiency of this
 * cut in MC with scale factors. This class must be subclassed before it can be
 * used, and two methods need to be implemented in the subclass:
 *
 * - \ref passes() to check whether an object should pass the cut
 * - \ref binIndex() to retrieve the bin index in the histogram with the scale
 *   factors
 *
 * In addition, the constructor should load scale factors using
 * \ref loadNominal() and systematic uncertainties using \ref loadBinWiseUnc()
 * and \ref loadGlobalUnc() (which differ in how the uncertainty will be
 * correlated between bins of the scale factors.
 *
 * Instances of classes derived from this one is used by calling the
 * call operator on individual objects or whole object collections.
 * Objects that do not pass the selection will be given a weight of zero.
 * Objects passing the selection will be reweighted by the scale factor (if the
 * correction is enabled) and new weights will be appended for systematics
 * (again only if enabled).
 *
 * Sometimes, additional inputs are needed to derive the cut and/or the scale
 * factor. We call this *contextual* information. An arbitrary number of extra
 * arguments can be passed to the call operator for context. The applier passes
 * them directly to passes() and binIndex(), where they can be used to
 * implement the selection or the correction. The types of context objects must
 * be provided as class template arguments. The functions take constant
 * references to these types.
 *
 * Each uncertainty variation must be given a unique name. The names of all
 * weights added by this class can be retrieved using the \ref weightNames()
 * method. This is useful to store them in the metainfo.
 */
template<class Object, class... Context>
class GenericSFApplier
{
    std::unique_ptr<TFile> m_file; ///< SFs are loaded from this file
    bool m_correction,    ///< Apply the scale factors?
         m_uncertainties; ///< Calculate uncertainties?

    /// Histogram with the nominal SFs
    std::unique_ptr<TH1> m_nominal;
    /// Histograms with uncertainties correlated bin-by-bin
    std::map<std::string, std::unique_ptr<TH1>> m_bin_wise_uncs;
    /// Histograms with uncertainties correlated
    std::map<std::string, std::unique_ptr<TH1>> m_global_uncs;

public:
    GenericSFApplier (const std::filesystem::path& filePath,
                      bool correction,
                      bool uncertainties);
    virtual ~GenericSFApplier () noexcept = default;

    void operator() (Object& object, const Context&... ctx) const;
    void operator() (std::vector<Object>& objects, const Context&... ctx) const;
    std::vector<std::string> weightNames () const;

protected:
    /// How loaded histograms should be interpreted
    enum Interpretation {
        /// The histogram contains the uncertainty in the scale factors
        UseBinContent,
        /// The histogram errors contain the uncertainty in the scale factors
        UseBinError,
    };

    void loadNominal (const std::string& histPath);
    void loadBinWiseUnc (const std::string& name,
                         const std::string& histPath,
                         Interpretation intp = UseBinContent);
    void loadGlobalUnc (const std::string& name,
                        const std::string& histPath,
                        Interpretation intp = UseBinContent);

    /**
     * \brief Called to check whether an object passes the selection
     *
     * \note As an optimization, this is only called when the object weight is
     *       not already zero.
     */
    virtual bool passes (const Object& obj, const Context&... ctx) const = 0;

    /**
     * \brief Called to retrieve the bin to use in the scale factor histograms
     *
     * The histogram passed as second parameter is always the nominal one, and
     * it is assumed that the bin index is the same for uncertainty variations
     * (that is, we assume that the binnings match).
     */
    virtual int binIndex (const Object& obj,
                          const Context&... ctx,
                          const std::unique_ptr<TH1>& hist) const = 0;
};

/**
 * \brief Constructor
 * \param filePath      The path of a ROOT file to load scale factors from
 * \param correction    Whether to apply scale factors
 * \param uncertainties Whether to store uncertainties
 */
template<class Object, class... Context>
GenericSFApplier<Object, Context...>::GenericSFApplier (
        const std::filesystem::path& filePath,
        bool correction,
        bool uncertainties)
    : m_correction(correction)
    , m_uncertainties(uncertainties)
{
    namespace fs = std::filesystem;

    if (!correction)
        return;

    // Check that the file exists
    if (!fs::exists(filePath))
        BOOST_THROW_EXCEPTION(
            fs::filesystem_error("Not found",
                                 filePath,
                                 make_error_code(std::errc::no_such_file_or_directory)));

    m_file = std::make_unique<TFile>(filePath.c_str(), "READ");
}


/**
 * \brief Applies the selection and scale factors to a single object
 */
template<class Object, class... Context>
void GenericSFApplier<Object, Context...>::operator() (
        Object& obj, const Context&... ctx) const
{
    auto& weights = weightsRef(obj);

    // For safety
    if (weights.empty()) weights.emplace_back(DAS::Weight{1, 0});

    const float old = weights.front().v;
    if (old == 0) return; // Don't bother

    // Apply the cut
    if (!passes(obj, ctx...)) {
        weights = {{0, 0}};
        return; // Skip applying weights
    }

    // Nominal correction
    if (!m_correction) return;

    const int bin = binIndex(obj, ctx..., m_nominal);
    const float sf = m_nominal->GetBinContent(bin);
    weights *= sf;

    // Systematics
    if (!m_uncertainties) return;

    // The order of the next two blocks must match weightNames()
    for (const auto& [_, hist] : m_bin_wise_uncs) {
        const float variation = hist->GetBinContent(bin);
        weights.push_back({old * (sf + variation), bin});
        weights.push_back({old * (sf - variation), bin});
    }

    for (const auto& [_, hist] : m_global_uncs) {
        const float variation = hist->GetBinContent(bin);
        weights.push_back({old * (sf + variation), 0});
        weights.push_back({old * (sf - variation), 0});
    }
}

/**
 * \brief Applies the selection and scale factors to objects in a vector
 */
template<class Object, class... Context>
void GenericSFApplier<Object, Context...>::operator() (
        std::vector<Object>& objects, const Context&... ctx) const
{
    for (auto& obj: objects) {
        (*this)(obj, ctx...);
    }
}

/**
 * \brief Retrieves the name of weights added by this applier.
 * Returns an empty vector when uncertainties are disabled.
 */
template<class Object, class... Context>
std::vector<std::string>
GenericSFApplier<Object, Context...>::weightNames () const
{
    std::vector<std::string> names;
    // The order of the next two blocks must match the call operator
    for (const auto& [name, _] : m_bin_wise_uncs) {
        names.push_back(name + SysUp);
        names.push_back(name + SysDown);
    }
    for (const auto& [name, _] : m_global_uncs) {
        names.push_back(name + SysUp);
        names.push_back(name + SysDown);
    }
    return names;
}

/**
 * \brief Loads the histogram with the nominal scale factor.
 * Has no effect when not applying the correction.
 */
template<class Object, class... Context>
void GenericSFApplier<Object, Context...>::loadNominal (
        const std::string& histPath)
{
    if (!m_correction) return;
    m_nominal = getHistSafe(m_file, histPath);
}

/**
 * \brief Loads a systematic with bin-by-bin correlations.
 * \see \ref loadGlobalUnc()
 *
 * The histogram is expected to contain the (symmetric) uncertainty in the
 * scale factor. This function has no effect when not applying the correction
 * or when uncertainties are disabled.
 */
template<class Object, class... Context>
void GenericSFApplier<Object, Context...>::loadBinWiseUnc (
        const std::string& name, const std::string& histPath, Interpretation intp)
{
    if (!m_correction || !m_uncertainties) return;
    auto hist = getHistSafe(m_file, histPath);
    if (intp == UseBinError) // We always store variations in the bin contents
        for (int i = 0; i <= hist->GetNcells(); ++i)
            hist->SetBinContent(i, hist->GetBinError(i));
    m_bin_wise_uncs[name] = std::move(hist);
}

/**
 * \brief Loads a fully correlated systematic.
 * \see \ref loadGlobalUnc()
 *
 * The histogram is expected to contain the (symmetric) uncertainty in the
 * scale factor. This function has no effect when not applying the correction
 * or when uncertainties are disabled.
 */
template<class Object, class... Context>
void GenericSFApplier<Object, Context...>::loadGlobalUnc (
        const std::string& name, const std::string& histPath, Interpretation intp)
{
    if (!m_correction || !m_uncertainties) return;
    auto hist = getHistSafe(m_file, histPath);
    if (intp == UseBinError) // We always store variations in the bin contents
        for (int i = 0; i <= hist->GetNcells(); ++i)
            hist->SetBinContent(i, hist->GetBinError(i));
    m_global_uncs[name] = std::move(hist);
}

} // namespace DAS
