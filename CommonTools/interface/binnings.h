#pragma once

#include <cassert>
#include <functional>
#include <vector>

#include <TString.h>

namespace DAS {

////////////////////////////////////////////////////////////////////////////////
/// Make a vector of properly formated titles from bin edges.
inline std::vector<TString> MakeTitle (const std::vector<double>& edges,
                                       const char * v,
                                       bool lowEdge,
                                       bool highEdge,
                                       std::function<const char*(double)> format)
{
    assert(edges.size() > 1);
    std::vector<TString> titles;
    for (size_t i = 1; i < edges.size(); ++i) {
        TString title;
        if (i >              1 ||  lowEdge) title += Form("%s < ", format(edges.at(i-1)));
        title += v;
        if (i < edges.size()-1 || highEdge) title += Form(" < %s", format(edges.at(i  )));
        titles.push_back(title);
    }
    return titles;
}

static const std::vector<double> pthat_edges {30,34,39,44,50,56,63,71,80,89,98,108,120,131,143,156,170,196,226,260,300,336,375,420,470,500,531,564,600,645,693,744,800,846,894,946,1000,1088,1183,1287,1400,1491,1587,1690,1800,1934,2078,2233,2400,2579,2771,2978,3200,3578,4000,4472,5000};

static const std::vector<double> pt_edges {9, 12, 15,18,21,24,28,32,37,43,49,56,64,74,84,97,114,133,153,174,196,220,245,272,300,330,362,395,430,468,507,548,592,638,686,737,790,846,905,967,1032,1101,1172,1248,1327,1410,1497,1588,1684,1784,1890,2000,2116,2238,2366,2500,2640,2787,2941,3103,3273,3450,3637,3832,4037/*,4252,4477,4713,4961,5220,5492,5777,6076,6389,6717,7000*/},
                            Mjj_edges {160,  200,  249,  306,  372,  449,  539, 641,  756,  887, 1029, 1187, 1361, 1556, 1769, 2008, 2273, 2572, 2915, 3306, 3754, 4244, 4805, 5374, 6094, 6908, 7861, 8929, 10050 },
pt_av_edges {147, 175, 207, 243, 284, 329, 380, 437, 499, 569, 646, 732, 827, 931, 1046, 1171, 1307, 1458, 1621, 1806, 2003, 2217, 2453, 2702},
                             y_edges {0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0},
                             n_edges {0.5, 1.5, 2.5, 3.5, 4.5, 5.5, 6.5, 7.5};
static const int nYbins = y_edges.size()-1,
                 nPtBins = pt_edges.size()-1,
                 nMjjBins = Mjj_edges.size()-1,
                 nPtDijetBins = pt_av_edges.size()-1,
                 maxMult = n_edges.size()-1;

static const std::vector<TString> yBins = MakeTitle(y_edges, "|y|", false, true, [](double v) { return Form("%.1f", v);} );

static const double ptmin = pt_edges.front(),
                    ptmax = pt_edges.back(),
                    ymin = y_edges.front(),
                    ymax = y_edges.back();

static const int nPUbins = 100;

} // end of DAS namespace
