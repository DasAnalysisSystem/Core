import json, os

import FWCore.ParameterSet.Config as cms
import FWCore.ParameterSet.VarParsing as VarParsing

# memory check (deactivated)
SimpleMemoryCheck = cms.Service("SimpleMemoryCheck",
        ignoreTotal = cms.untracked.int32(1))

# command line parser
args = VarParsing.VarParsing ('analysis')
args.register('configFile',
     'config.json',
     VarParsing.VarParsing.multiplicity.singleton,
     VarParsing.VarParsing.varType.string,
     "JSON config file")
args.parseArguments()

from os.path import exists
params = {}
if exists(args.configFile):
    with open(args.configFile, "r") as f:
        params = json.load(f)

if not 'flags' in params:
    params['flags'] = {'options': ["jets"]}
flags = params['flags']

if not 'labels' in flags:
    flags["labels"] = []
labels = flags["labels"]

if not 'R' in flags:
    flags['R'] = 4
radius = flags['R']/10

if not 'options' in flags:
    raise ValueError("No options in input config")
options = flags['options']

jets = 'jets' in options
flavour = 'flavour' in options
muons = 'muons' in options
photons = 'photons' in options
triggers = 'triggers' in options
dump = 'dump' in options

import sys

if flavour and radius != 0.4:
    raise ValueError("Flavour tagging is only supported for ak4")

if len(args.inputFiles) > 0:
    inputFiles = args.inputFiles
else:
    inputFiles = ['root://cms-xrd-global.cern.ch//store/mc/RunIISummer20UL16MiniAODv2/QCD_Pt-15to7000_TuneCP5_Flat2018_13TeV_pythia8/MINIAODSIM/106X_mcRun2_asymptotic_v17-v1/270000/030D9093-0513-0340-A5A4-15DE53778266.root']
    params['datasets'] = {'/QCD_Pt-15to7000_TuneCP5_Flat2018_13TeV_pythia8/RunIISummer20UL16MiniAODv2-106X_mcRun2_asymptotic_v17-v1/MINIAODSIM' : 'Automatic'}

isMC = 'MINIAODSIM' in inputFiles[0]
flags["isMC"] = isMC

if any(x in inputFiles[0] for x in ['2016UL','UL16','UL2016']):
    year = 2016
elif any(x in inputFiles[0] for x in ['2017UL','UL17','UL2017']):
    year = 2017
elif any(x in inputFiles[0] for x in ['2018UL','UL18','UL2018']):
    year = 2018
    # \todo 2022 and 2023
elif any(x in inputFiles[0] for x in ['Run2024', 'mcRun3_2024']):
    year = 2024
else:
    raise ValueError("Year is not recognised")
flags["year"] = year

run = 2 if year < 2019 else 3

# definition of the process (note: the name is used when reclustering)
process = cms.Process('Darwin')

# definition of the output file
process.TFileService=cms.Service("TFileService",fileName=cms.string(args.outputFile))

# definition of the max number of events
process.maxEvents = cms.untracked.PSet(input = cms.untracked.int32(args.maxEvents))
if args.maxEvents > 0:
    print("MaxEvents="+str(args.maxEvents))

# definition of the source file(s)
process.source = cms.Source("PoolSource",
  fileNames = cms.untracked.vstring(inputFiles)
)

# message logger (not much used in the current setup)
process.load('FWCore.MessageService.MessageLogger_cfi')
process.MessageLogger = cms.Service("MessageLogger",
    categories = cms.untracked.vstring('FwkReport', 'JetPtMismatch', 'MissingJetConstituent'),
    destinations = cms.untracked.vstring('cerr'),
    cerr = cms.untracked.PSet(
            threshold = cms.untracked.string('WARNING'),
            JetPtMismatch = cms.untracked.PSet(limit = cms.untracked.int32(0)),
            MissingJetConstituent = cms.untracked.PSet(limit = cms.untracked.int32(0)),
            FwkReport = cms.untracked.PSet(reportEvery = cms.untracked.int32(1000)),
            )
    )

### GT ###

## \todo clarify how much it matters... (in the n-tuples, we take the
#  uncorrected, but what about DeepJet for instance??)
from Configuration.AlCa.GlobalTag import GlobalTag
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')

# See summary table from PdmV:
# https://twiki.cern.ch/twiki/bin/viewauth/CMS/PdmVAnalysisSummaryTable
# https://twiki.cern.ch/twiki/bin/view/CMS/PdmVRun2LegacyAnalysisSummaryTable
if isMC:
    if year == 2016:
        if 'HIPM' in inputFiles[0] or 'VFP' in inputFiles[0]:
            GT = '106X_mcRun2_asymptotic_preVFP_v11'
            if not 'HIPM' in labels:
                labels += ['HIPM']
        else:
            GT = '106X_mcRun2_asymptotic_v17'
    elif year == 2017:
        GT = '106X_mc2017_realistic_v10'
    elif year == 2018:
        GT = '106X_upgrade2018_realistic_v16_L1v1'
    # \todo 2022 and 2023
    elif year == 2024:
        GT = '133X_mcRun3_2024_realistic_v10' # \todo TBC
    else:
        raise RuntimeError("No GT could be determined")
else:
    if year > 2015 and year < 2019:
        GT = '106X_dataRun2_v37'
    # \todo 2022 and 2023
    elif year == 2024:
        GT = '141X_dataRun3_Prompt_v4' # \todo TBC
    else:
        raise RuntimeError("No GT could be determined")

print(GT)

process.GlobalTag = GlobalTag(process.GlobalTag, GT, '')

### MET ###

from pathlib import Path
METfilters = Path()
metNames = []
if "corrections" in params and "METfilters" in params["corrections"]:
    METfilters = Path(os.path.expandvars(params["corrections"]["METfilters"]))
    # expecting a 2-column file (see $DARWIN_TABLES/MET)
    with open(METfilters, 'r') as data:
        for line in data:
            p = line.split()
            metNames.append(str(p[0]))
            ## \note The MET filters are not applied but only saved to the n-tuples
    print("MET filters: " + " ".join(metNames))
else:
    print("\x1B[33mNo MET filters\x1B[0m")

### JETS ###

from RecoJets.JetProducers.GenJetParameters_cfi import *

# gen particles are used both for muons and for flavours
# (so not relevant for analyses working with all-flavour inclusive jets)
genParticleCollection = 'prunedGenParticles'
genJetCollection = 'slimmedGenJets'

# determine the PU method
hasPUPPI = 'PUPPI' in labels
hasCHS = 'CHS' in labels

if hasPUPPI and hasCHS:
    raise ValueError("PUPPI xor CHS")

if not hasPUPPI and not hasCHS:
    if year < 2019:
        hasCHS = True
        labels += ['CHS']
    else:
        hasPUPPI = True
        labels += ['PUPPI']

if hasPUPPI:
    PUmethod = 'PUPPI'
    JetCollection = 'slimmedJetsPuppi'
elif hasCHS:
    PUmethod = 'CHS'
    JetCollection = 'slimmedJets'

## \todo add forward triggers for 2017 and 2018
triggerNames = []
if jets:

    if radius == 0.8:
        ## reclustering:
        JETCorrPayload = 'AK8PF' + PUmethod.lower()
        from JMEAnalysis.JetToolbox.jetToolbox_cff import jetToolbox
        jetToolbox(process, 'ak8', 'jetSequence', 'noOutput', # change 'noOutput' to 'out' to get an intermediate ROOT file
                PUMethod=PUmethod, JETCorrPayload=JETCorrPayload, JETCorrLevels=[ '' ],
                runOnMC=isMC, Cut='pt > 10.0 && abs(rapidity()) < 5.0',
                bTagDiscriminators='')
        """\todo use AK label?"""
        genJetCollection = 'selectedPatJetsAK8PF' + PUmethod
        JetCollection = 'selectedPatJetsAK8PF' + PUmethod

        triggerNames = ['HLT_AK8PFJet40_v', 'HLT_AK8PFJet60_v', 'HLT_AK8PFJet80_v',
                'HLT_AK8PFJet140_v', 'HLT_AK8PFJet200_v', 'HLT_AK8PFJet260_v',
                'HLT_AK8PFJet320_v', 'HLT_AK8PFJet400_v', 'HLT_AK8PFJet450_v',
                'HLT_AK8PFJet500_v']
        if year > 2016:
            triggerNames += ['HLT_AK8PFJet550_v']

        ## default:
        #genJetCollection = 'slimmedGenJetsAK8'
        #JetCollection = 'slimmedJetsAK8'
        # does not work: "This PAT jet was not made from a JPTJet nor from PFJet."
    elif radius == 0.4:
        if 'FSQJet' in inputFiles[0] and year == 2017:
            triggerNames += ['HLT_HIAK4PFJet15_v','HLT_HIPFJet25_v','HLT_HIAK4PFJet40_v','HLT_HIAK4PFJet60_v','HLT_HIAK4PFJet80_v','HLT_HIPFJet140_v']
        else:
            triggerNames += ['HLT_PFJet40_v' , 'HLT_PFJet60_v' , 'HLT_PFJet80_v' , 'HLT_PFJet140_v',
                             'HLT_PFJet200_v', 'HLT_PFJet260_v', 'HLT_PFJet320_v', 'HLT_PFJet400_v',
                             'HLT_PFJet450_v', 'HLT_PFJet500_v']
            if year > 2016:
                triggerNames += ['HLT_PFJet550_v']

    else:
        raise ValueError("Only ak4 and ak8 are supported")

if muons and triggers:
    triggerNames += [ "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass8_v",
                      "HLT_Mu17_TrkIsoVVL_Mu8_TrkIsoVVL_DZ_Mass3p8_v"]


if (not isMC) and ('ZeroBias' in inputFiles[0]):
    triggerNames = ['HLT_ZeroBias_v']

### FLAVOURS ###

if jets and flavour:

    if isMC:
        from PhysicsTools.JetMCAlgos.HadronAndPartonSelector_cfi import selectedHadronsAndPartons
        from PhysicsTools.JetMCAlgos.AK4PFJetsMCFlavourInfos_cfi import ak4JetFlavourInfos
        process.selectedHadronsAndPartons = selectedHadronsAndPartons.clone(
            particles = genParticleCollection
        )
        process.ak4genJetFlavourInfos = ak4JetFlavourInfos.clone(
            jets = genJetCollection,
            hadronFlavourHasPriority = cms.bool(True) # only affect the parton flavour, actually
        )

    process.load('Configuration.Geometry.GeometryRecoDB_cff')
    process.load('Configuration.StandardSequences.MagneticField_cff')

    from PhysicsTools.PatAlgos.tools.jetTools import updateJetCollection

    # https://github.com/cms-sw/cmssw/blob/master/PhysicsTools/PatAlgos/test/patTuple_updateJets_fromMiniAOD_cfg.py
    updateJetCollection(
            process,
            jetSource = cms.InputTag('slimmedJets'),
            jetCorrections = ('AK4PF'+PUmethod.lower(), cms.vstring([]), 'None'),
            # NOTE: if you want to know the list of all possible btag
            # discriminators, just enter a random/dummy/wrong one and check the
            # error message while trying to run the N-tupliser...
            btagDiscriminators = [
                'pfDeepFlavourJetTags:probb',
                'pfDeepFlavourJetTags:probbb',
                'pfDeepFlavourJetTags:problepb',
                'pfDeepFlavourJetTags:probc',
                'pfDeepFlavourJetTags:probuds',
                'pfDeepFlavourJetTags:probg',
                ],
            postfix='Retrained'
            )
    JetCollection = 'selectedUpdatedPatJetsRetrained'

### MISC ###

if not 'preseed' in params:
    import random
    params['preseed'] = random.randrange(0,2e8)

with open(args.configFile, 'w') as f:
    json.dump(params, f, indent = 4)

getPUjetID = jets and 'CHS' in labels and radius == 0.4

process.ntupliser = cms.EDAnalyzer('Ntupliser',
    isMC            = cms.bool(isMC),
    year            = cms.int32(year),
    sandbox         = cms.bool('crab' in params and 'sandbox' in params['crab'] and params['crab']['sandbox']),
    config          = cms.string(args.configFile),
# event
    vertices        = cms.InputTag('offlineSlimmedPrimaryVertices'),
    rho             = cms.InputTag('fixedGridRhoFastjetAll'),
    pileupInfo      = cms.untracked.InputTag('slimmedAddPileupInfo'),
# jets
    PUjetID         = cms.bool(getPUjetID),
    lhe             = cms.InputTag('externalLHEProducer'),
    genjets         = cms.InputTag(genJetCollection, 'genJets' if radius != 0.4 else ''),
    recjets         = cms.InputTag(JetCollection, '', '' if radius == 0.4 else 'Darwin'),
# flavour stuff
    jetFlavourInfos = cms.InputTag('ak4genJetFlavourInfos'),
    SV_infos        = cms.InputTag('slimmedSecondaryVertices', '', 'PAT'), ## \todo remove?
    genparticles    = cms.InputTag(genParticleCollection),
# muons
    genLeptons      = cms.InputTag("particleLevel:leptons"),  # Rivet-based definitions
    recmuons        = cms.InputTag('slimmedMuons'),
# photons
    recphotons      = cms.InputTag('slimmedPhotons'),
# trigger
    triggerNames    = cms.vstring(triggerNames),
    triggerPrescales      = cms.InputTag('patTrigger', ''     , 'PAT' if run == 2 else 'RECO'),
    triggerPrescalesl1min = cms.InputTag('patTrigger', 'l1min', 'PAT' if run == 2 else 'RECO'),
    triggerPrescalesl1max = cms.InputTag('patTrigger', 'l1max', 'PAT' if run == 2 else 'RECO'),
    triggerResults   = cms.InputTag('TriggerResults','','HLT'),
    triggerObjects  = cms.InputTag('slimmedPatTrigger', '', 'PAT' if run == 2 else 'RECO'),
# MET
    met              = cms.InputTag('slimmedMETs'),
    metNames         = cms.vstring(metNames),
    metResults       = cms.InputTag('TriggerResults','', 'PAT' if run == 2 else 'RECO'),
)

if dump:
   print(process.dumpPython())
   sys.exit()

paths = process.ntupliser

# pile-up jet ID (https://twiki.cern.ch/twiki/bin/view/CMS/PileupJetIDUL)
if getPUjetID:
    # https://github.com/cms-sw/cmssw/blob/master/RecoJets/JetProducers/python/PileupJetID_cfi.py
    from RecoJets.JetProducers.PileupJetID_cfi import _chsalgos_106X_UL16APV, _chsalgos_106X_UL16, _chsalgos_106X_UL17, _chsalgos_106X_UL18, pileupJetId
    if year == 2016:
        if 'HIPM' in labels:
            _stdalgos = _chsalgos_106X_UL16APV
        else:
            _stdalgos = _chsalgos_106X_UL16
    elif year == 2017:
        _stdalgos = _chsalgos_106X_UL17
    elif year == 2018:
        _stdalgos = _chsalgos_106X_UL18
    else:
        raise ValueError("No PU jet ID could be determined")

    process.pileupJetId = pileupJetId.clone(
        jets=cms.InputTag(JetCollection),
        inputIsCorrected=False,
        applyJec=True, ## \todo check that the right JECs are applied
        vertexes=cms.InputTag("offlineSlimmedPrimaryVertices"),
        algos = cms.VPSet(_stdalgos),
    )
    paths = ( process.pileupJetId * paths )

if flavour:
    paths = ( process.patJetCorrFactorsRetrained *
              process.updatedPatJetsRetrained *
              process.pfImpactParameterTagInfosRetrained *
              process.pfInclusiveSecondaryVertexFinderTagInfosRetrained *
              process.pfDeepCSVTagInfosRetrained *
              process.pfDeepFlavourTagInfosRetrained *
              process.pfDeepFlavourJetTagsRetrained *
              process.patJetCorrFactorsTransientCorrectedRetrained *
              process.updatedPatJetsTransientCorrectedRetrained *
              process.selectedUpdatedPatJetsRetrained *
              paths )

    if isMC:
        paths = ( process.selectedHadronsAndPartons *
                  process.ak4genJetFlavourInfos *
                  paths )

if muons and isMC:
    # Pull dressed lepton definitions used in NanoAOD
    # This comes with a basic selection
    from PhysicsTools.NanoAOD.particlelevel_cff import mergedGenParticles, genParticles2HepMC, particleLevel
    from SimGeneral.HepPDTESSource.pythiapdt_cfi import HepPDTESSource
    process.HepPDTESSource = HepPDTESSource
    process.mergedGenParticles = mergedGenParticles  # Merges prunedGenParticles and slimmedGenParticles
    process.genParticles2HepMC = genParticles2HepMC  # Converts to HepMC for Rivet
    process.particleLevel = particleLevel            # Retrieves particle definitions from Rivet
    paths = (process.mergedGenParticles * process.genParticles2HepMC * process.particleLevel * paths)

if isMC:
    from GeneratorInterface.Core.genXSecAnalyzer_cfi import *
    process.GenXSecAnalyzer = cms.EDAnalyzer("GenXSecAnalyzer")
    paths = process.GenXSecAnalyzer * paths

process.options.numberOfConcurrentLuminosityBlocks = 1
process.p = cms.Path(paths)
