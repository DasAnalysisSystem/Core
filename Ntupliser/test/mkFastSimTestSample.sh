#!/bin/sh

set -e

if [[ $# -ne 1 ]]; then

    echo "$0 test folder" >&2
    exit 2
fi

folder="$1"

mkdir -p "$folder"
cd "$folder"

nevents=100
fragment=DYToLL_M-50_13TeV_pythia8_cff

# Number of threads to use. Let the user override (issue #79)
if [ -z $THREADS ]; then
    # Use up to 10 threads (works on lxplus)
    THREADS=$(( $(nproc) < 10 ? $(nproc) : 10 ))
fi

# MC production needs to run in a specific CMSSW release
release=/cvmfs/cms.cern.ch/slc7_amd64_gcc700/cms/cmssw/CMSSW_10_6_30

# File names
# Note that the CMSSW config performs string comparisons on these
aodsim_file=AODSIM-$fragment-UL18.root
miniaodsim_file=MINIAODSIMv2-$fragment-UL18.root
miniaod_file=MINIAODv2-${fragment}-UL18.root

# Only produce it once
if [ -f $miniaod_file ] ; then
    echo "$PWD/$miniaod_file already exists"
    exit
fi

# Source CMSSW
echo "Running in $release"
source /cvmfs/cms.cern.ch/cmsset_default.sh
cd $release; eval $(scram runtime -sh); cd -

# Step 1: AODSIM
# Settings from campaign RunIISpring22UL18FSwmLHEGSPremix in McM
# Apart from this being a GEN-SIM, the main differences is that pileup is
# generated inline instead of using a premix library.
cmsDriver.py $fragment \
    --fileout file:$aodsim_file \
    -n $nevents \
    --nThreads $THREADS \
    --fast \
    --conditions 106X_upgrade2018_realistic_v16 \
    --era Run2_2018_FastSim \
    --beamspot Realistic25ns13TeVEarly2018Collision \
    --step GEN,SIM,RECOBEFMIX,DIGI:pdigi_valid,L1,DIGI2RAW,L1Reco,RECO \
    --eventcontent AODSIM \
    --datatier AODSIM

# Step 2: MC MiniAODv2
# Settings from campaign RunIISummer20UL18MiniAODv2 in McM, adding
# --fast from the flow connecting it to RunIISpring22UL18FSwmLHEGSPremix.
cmsDriver.py MiniAODv2-$fragment \
    --filein file:$aodsim_file \
    --fileout file:$miniaodsim_file \
    --nThreads $THREADS \
    --fast \
    -n -1 \
    --conditions 106X_upgrade2018_realistic_v16_L1v1 \
    --era Run2_2018 \
    --step PAT \
    --procModifiers run2_miniAOD_UL \
    --geometry DB:Extended \
    --runUnscheduled \
    --eventcontent MINIAODSIM \
    --datatier MINIAODSIM

# Step 2bis: Data MiniAODv2
# Same settings as above but no SIM
cmsDriver.py MiniAODv2-$fragment-data \
    --filein file:$aodsim_file \
    --fileout file:$miniaod_file \
    --nThreads $THREADS \
    --fast \
    -n -1 \
    --conditions 106X_upgrade2018_realistic_v16_L1v1 \
    --era Run2_2018 \
    --step PAT \
    --procModifiers run2_miniAOD_UL \
    --geometry DB:Extended \
    --runUnscheduled \
    --eventcontent MINIAOD \
    --datatier MINIAOD
