#!/bin/sh
set -e
set -x

folder="$1"
input_file="$(readlink -f "$2")"
isMC="$3"

if [ ! -f $input_file ]; then
    echo $input_file cannot be found
    exit 1
fi

mkdir -p "$folder"
cd "$folder"

# diff $CMSSW_BASE/python/Core/Ntupliser/Ntupliser_cfg.py $CMSSW_BASE/src/Core/Ntupliser/python/Ntupliser_cfg.py (TODO)
if [ "$isMC" = "yes" ]; then
    cp $CMSSW_BASE/src/Core/Ntupliser/test/CI.json $CMSSW_BASE/test/CI.json
else
    grep -v flavour $CMSSW_BASE/src/Core/Ntupliser/test/CI.json > $CMSSW_BASE/test/CI.json
fi
cmsRun $CMSSW_BASE/src/Core/Ntupliser/python/Ntupliser_cfg.py inputFiles=file:"$input_file" outputFile=ntuple.root configFile=$CMSSW_BASE/test/CI.json
